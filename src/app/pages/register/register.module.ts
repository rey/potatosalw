import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NbButtonModule, NbCardModule, NbLayoutModule, NbProgressBarModule, NbActionsModule, NbInputModule, NbIconModule, NbSearchModule, NbToggleModule, NbSelectModule, NbDatepickerModule, NbTooltipModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
	declarations: [RegisterComponent],
	imports: [
		CommonModule,
		RegisterRoutingModule,
		SharedModule,
		NgxCaptchaModule,
		NbButtonModule,
		NbCardModule,
		NbLayoutModule,
		NbProgressBarModule,
		NbActionsModule,
		NbInputModule,
		NbIconModule,
		NbSearchModule,
		NbToggleModule,
		NbSelectModule,
		NbDatepickerModule,
		NbTooltipModule,
	]

})
export class RegisterModule { }
