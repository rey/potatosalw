import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SocialService {

    constructor(private httpClient: HttpClient) { }

    getGoogleInfo(token: string) {
        const personFields = 'genders,birthdays,emailAddresses';
        return this.httpClient.post(`user/validation-user-google`, { personFields, token });
    }
}
