import {
	CAPTCHAAPIKEY,
	DEFAULTMOBILEPREFIX,
} from "../../core/models/masters/masters.enum";
import { Component, OnDestroy, OnInit, ViewEncapsulation } from "@angular/core";
import { Router } from "@angular/router";
import {
	UntypedFormBuilder,
	UntypedFormControl,
	UntypedFormGroup,
	Validators,
} from "@angular/forms";

// Utils
import { LocalStorage } from "src/app/core/utils";

// Services
import { TranslateService } from "@ngx-translate/core";
import { TitleService } from "src/app/core/services/shared";
import { LoginService } from "src/app/core/services/login";

// Models
import { DateAdapter } from "@angular/material/core";
import { RegisterService } from "src/app/core/services/register/register.service";
import { RegisterModel } from "src/app/core/models/users/register.model";
import { map, switchMap } from "rxjs/operators";
import { ROUTES_NAME } from "src/app/core/utils/routes-name";
import { MastersService } from "src/app/core/services/masters";
import { GeneroModel } from "src/app/core/models/masters/genero.model";
import { Observable, of, Subscription } from "rxjs";
import { formsValidations } from "src/app/core/utils/forms-validations";
// import {
// 	AuthService,
// 	GoogleLoginProvider,
// 	SocialUser,
// } from "angularx-social-login";
import { SocialService } from "./social.service";
import { DialogService } from "src/app/core/services/shared/dialog.service";
import { CenterService } from "src/app/core/services/center/center.service";
import { PrefixCountry } from "src/app/core/models/shared/prefixCountry.model";
import { LoginModel } from "src/app/core/models/users/login.model";
import { LOCALSTORAGESTRINGS } from "src/app/core/models/masters/localstorage.enum";
import { MatomoAnalyticsUtils } from "src/app/core/utils/matomo-analytics.utils";
import { ToasterService } from "src/app/core/services/shared/toaster.service";

declare function init_plugins();

enum STEPS {
	STEP1 = "1",
	STEP2 = "2",
	STEP3 = "3",
}

@Component({
	selector: "app-register",
	styleUrls: ["./register.component.scss"],
	templateUrl: "./register.component.html",
	encapsulation: ViewEncapsulation.None,
})
export class RegisterComponent implements OnInit, OnDestroy {
	step2: UntypedFormGroup;
	step1: UntypedFormGroup;
	step3: UntypedFormGroup;
	hidePassword: boolean = true;
	hidePasswordR: boolean = true;
	loginImage: string;
	lang: string;
	siteKey: string = CAPTCHAAPIKEY;
	currentStep: string = STEPS.STEP1;
	validationMessages = {
		firstName: [],
		surname: [],
		email: [],
		mobile: [],
		birthdate: [],
		gender: [],
		keyword: [],
		terms: [],
		recaptcha: [],
		prefix: [],
		code: [],
	};
	loading: boolean = false;
	maxDate: Date;
	minDate: Date;
	genders$: Observable<GeneroModel[]>;
	loginRoute = ROUTES_NAME.LOGIN;
	$phonePrefixes: Observable<PrefixCountry[]>;
	mobileOk: boolean = true;
	mailOk: boolean = false;
	inputSMS: boolean = true;
	clickNewCode: boolean = false;
	clickNewEmail: boolean = false;
	today: Date;
	registerAux: boolean = false;

	private minAge: number = formsValidations.USER_MIN_AGE;
	private nameMaxLength: number = formsValidations.NAME_MAX_LENGTH;
	private surnameMaxLength: number = formsValidations.SURNAME_MAX_LENGTH;
	private emailMaxLength: number = formsValidations.EMAIL_MAX_LENGTH;
	private emailPattern: string = formsValidations.EMAIL_PATTERN;
	private mobileMinLength: number = formsValidations.MOBILE_MIN_LENGTH;
	private mobileMaxLength: number = formsValidations.MOBILE_MAX_LENGTH;
	private mobilePattern: string = formsValidations.MOBILE_PATTERN;
	private subs = new Subscription();
	private courseInitial: string;
	year: any;
	showProfiles: boolean = true;

	constructor(
		private router: Router,
		private formBuild: UntypedFormBuilder,
		public translateService: TranslateService,
		private titleService: TitleService,
		private localStorage: LocalStorage,
		private loginService: LoginService,
		private registerService: RegisterService,
		private dateAdapter: DateAdapter<Date>,
		private mastersService: MastersService,
		private socialService: SocialService,
		private dialogService: DialogService,
		private centerService: CenterService,
		private ma: MatomoAnalyticsUtils,
		private toaster: ToasterService
	) {
		const currentYear = new Date().getFullYear();
		const currentMount = new Date().getMonth();
		const currentDay = new Date().getDate();
		this.today = new Date(currentYear, currentMount, currentDay);
	}

	ngOnInit(): void {
		this.load();
		this.step1.get("profileParent").disable();
		this.step1.get("profileTeacher").disable();
	}

	private load() {
		this.genders$ = this.mastersService.getAllGenders();
		this.subs = this.centerService.getImageCenter().subscribe((img) => {
			this.loginImage = img;
		});
		this.$phonePrefixes = this.loginService.getPhonePrefixV2();
		this.crearInitialForm();
		this.confirmCodeForm();
		this.getCenterInfo();

		// this.authService.authState
		// 	.pipe(
		// 		switchMap((user: SocialUser) => {
		// 			if (user) {
		// 				return this.socialService.getGoogleInfo(user.authToken).pipe(
		// 					map((info: any) => JSON.parse(info.data)),
		// 					map((info: any) => ({ user, info }))
		// 				);
		// 			} else {
		// 				return of({ user: undefined, info: undefined });
		// 			}
		// 		})
		// 	)
		// 	.subscribe(
		// 		({ user, info }) => {
		// 			if (user) {
		// 				this.emailControl.setValue(user.email);
		// 				this.step1.get("firstName").setValue(user.firstName);
		// 				this.step1.get("surname").setValue(user.lastName);
		// 				this.step1.markAllAsTouched();
		// 				this.authService.signOut();
		// 			}

		// 			if (info) {
		// 				if (info.birthdays && info.birthdays[0]) {
		// 					const birthDate: Date = new Date(
		// 						Date.UTC(
		// 							info.birthdays[0].date.year,
		// 							info.birthdays[0].date.month - 1,
		// 							info.birthdays[0].date.day,
		// 							0,
		// 							0,
		// 							0
		// 						)
		// 					);
		// 					this.birthdateControl.setValue(birthDate);
		// 				}

		// 				if (info.phoneNumbers && info.phoneNumbers[0]) {
		// 					this.step1.get("mobile").setValue(info.phoneNumbers[0].value);
		// 				}

		// 				if (info.genders && info.genders[0]) {
		// 					this.step1
		// 						.get("genre")
		// 						.setValue(
		// 							info.genders[0].value === "male"
		// 								? 1
		// 								: info.genders[0].value === "female"
		// 								? 2
		// 								: 3
		// 						);
		// 				}
		// 			}
		// 		},
		// 		(err) => {
		// 			console.error(err);
		// 			this.authService.signOut();
		// 		}
		// 	);

		let today = new Date();
		this.minDate = new Date(today.setFullYear(today.getFullYear() - 110));
		today = new Date();
		this.maxDate = new Date(
			today.setFullYear(today.getFullYear() - this.minAge)
		);

		init_plugins();

		this.traducirOpciones();

		// Y hago un logout
		this.loginService.logout(false);
	}

	crearInitialForm() {
		this.step1 = this.formBuild.group({
			// asyncValidators: [this.registerValidationService.emailValidator()],
			// updateOn: 'blur'
			firstName: [
				"",
				{
					validators: [
						Validators.required,
						Validators.maxLength(this.nameMaxLength),
					],
				},
			],
			surname: [
				"",
				{
					validators: [
						Validators.required,
						Validators.maxLength(this.surnameMaxLength),
					],
				},
			],
			birthdate: ["", Validators.required],

			mobile: new UntypedFormControl("", [
				Validators.minLength(this.mobileMinLength),
				Validators.maxLength(this.mobileMaxLength),
				Validators.pattern(this.mobilePattern),
			]),
			genre: ["", Validators.required],
			email: [
				"",
				{
					validators: [
						Validators.required,
						Validators.maxLength(this.emailMaxLength),
						Validators.pattern(this.emailPattern),
					],
				},
			],
			prefix: [DEFAULTMOBILEPREFIX, Validators.required],
			profileEditor: [true, Validators.required],
			profileStudent: [false, Validators.required],
			profileTeacher: [false, Validators.required],
			profileParent: [false, Validators.required],
		});
	}

	confirmCodeForm() {
		this.step3 = this.formBuild.group({
			code: ["", { validators: [Validators.required] }],
		});
	}

	ngOnDestroy(): void {
		this.subs.unsubscribe();
	}

	nextStep() {
		if (this.step1.invalid) return false;
		if (this.mailOk && this.mobileOk) {
			this.currentStep = STEPS.STEP2;
			this.crearFormulario();
		} else {
			this.toaster.error(
				this.translateService.instant("REGISTRO.MAILORPHONEEXIST")
			);
			return false;
		}
	}

	crearFormulario() {
		this.step2 = this.formBuild.group({
			keyWord: [
				"",
				{
					validators: Validators.compose([
						Validators.required,
						Validators.pattern(formsValidations.PASSWORDPATTERN),
					]),
				},
			],
			terms: [false, Validators.requiredTrue],
			recaptcha: ["", Validators.required],
		});
	}

	validateYears() {
		const currentYear = new Date().getFullYear()
		const date = this.step1.get("birthdate").value;
		this.year = +date.substr(-20, 4);
		console.log(currentYear,this.year);
		const isAdult = currentYear - this.year
		if(isAdult<=17){
			this.showProfiles = false;
			this.step1.get("profileParent").setValue(false);
			this.step1.get("profileTeacher").setValue(false);
		}else{
			this.showProfiles = true;
			this.step1.get("profileParent").enable();
			this.step1.get("profileTeacher").enable();
		}
	}

	get emailControl(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("email") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get nickControl(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("nick") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get birthdateControl(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("birthdate") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get keywordControl(): UntypedFormControl {
		if (this.step2) {
			return this.step2.get("keyWord") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get genderControl(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("genre") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get profileEditor(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("profileEditor") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get profileStudent(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("profileStudent") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get profileTeacher(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("profileTeacher") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get profileParent(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("profileParent") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get termsControl(): UntypedFormControl {
		if (this.step2) {
			return this.step2.get("terms") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get recaptchaControl(): UntypedFormControl {
		if (this.step2) {
			return this.step2.get("recaptcha") as UntypedFormControl;
		} else {
			return null;
		}
	}

	traducirOpciones() {
		// Recupero el lenguaje

		this.lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
		this.translateService.use(this.lang);
		this.dateAdapter.setLocale(this.lang);

		// Titulo pagina
		this.translateService
			.get("REGISTRO.REGISTRARNUEVOUSUARIO")
			.subscribe((res: string) => {
				this.titleService.setHTMLTitle(res);
			});

		this.translateService
			.get("VALIDACIONES.ELNOMBREESOBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.firstName.push({
					type: "required",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.LOSAPELLIDOSSONOBLIGATORIOS")
			.subscribe((res: string) => {
				this.validationMessages.surname.push({
					type: "required",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.EMAILEXISTE")
			.subscribe((res: string) => {
				this.validationMessages.email.push({
					type: "emailExists",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.ELEMAILESOBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.email.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.ELEMAILNOESVALIDO")
			.subscribe((res: string) => {
				this.validationMessages.email.push({ type: "pattern", message: res });
			});

		this.translateService
			.get("VALIDACIONES.ELTELEFONOMOVILESOBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.mobile.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.MOBILEMIN")
			.subscribe((res: string) => {
				this.validationMessages.mobile.push({
					type: "minlength",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.MOBILEMAX")
			.subscribe((res: string) => {
				this.validationMessages.mobile.push({
					type: "maxlength",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.MOBILENUMEROS")
			.subscribe((res: string) => {
				this.validationMessages.mobile.push({ type: "pattern", message: res });
			});

		this.translateService
			.get("VALIDACIONES.BIRTHDATEOBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.birthdate.push({
					type: "required",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.BIRTHDATEMAX")
			.subscribe((res: string) => {
				this.validationMessages.birthdate.push({
					type: "matDatepickerMax",
					message: res.replace("{1}", `${this.minAge}`),
				});
			});

		this.translateService
			.get("VALIDACIONES.GENDEROBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.gender.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.LACONTRASENAESOBLIGATORIA")
			.subscribe((res: string) => {
				this.validationMessages.keyword.push({
					type: "required",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.MUSTACCEPTTERMS")
			.subscribe((res: string) => {
				this.validationMessages.terms.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.MAXLENGTH")
			.subscribe((res: string) => {
				this.validationMessages.firstName.push({
					type: "maxlength",
					message: res.replace("{1}", `${this.nameMaxLength}`),
				});
				this.validationMessages.surname.push({
					type: "maxlength",
					message: res.replace("{1}", `${this.surnameMaxLength}`),
				});
				this.validationMessages.email.push({
					type: "maxlength",
					message: res.replace("{1}", `${this.emailMaxLength}`),
				});
			});

		this.translateService
			.get("VALIDACIONES.RECAPTCHAREQUIRED")
			.subscribe((res: string) => {
				this.validationMessages.recaptcha.push({
					type: "required",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.PASSWORDPATTERN")
			.subscribe((res: string) => {
				this.validationMessages.keyword.push({ type: "pattern", message: res });
			});

		this.translateService
			.get("VALIDACIONES.PREFIXREQUIRED")
			.subscribe((res: string) => {
				this.validationMessages.prefix.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.CODEREQUIRED")
			.subscribe((res: string) => {
				this.validationMessages.code.push({ type: "required", message: res });
			});
	}

	sendSms(): void | boolean {
		if (this.step2.invalid) return false;

		const fv1 = this.step1.value; //Formulario con los datos del usuario

		//condicion para que si esta el movil vacio, pero el mail relleno, enviar por mail
		if (fv1.mobile === "") {
			this.inputSMS = false;
			this.currentStep = STEPS.STEP3;
			//llamar al metodo de enviar codigo por mail
			return this.sendMail();
		} else {
			let prefix: string = fv1.prefix.replace("+", "");
			this.registerService.requestSmsCode(prefix, fv1.mobile).subscribe(
				(result) => {
					//Si está correcto, pasamos al último step
					if (result.status === "ok") {
						this.currentStep = STEPS.STEP3;
					} else {
						//Si hay algún error, debemos mostrarlo aqui
						this.toaster.error(result.error_msg);
					}
				},
				(err) => {
					//Mostramos un error si ha salido mal el proceso (error genérico)
					this.toaster.error(
						this.translateService.instant("ERROR.HAOCURRIDOUNERROR")
					);
				}
			);
		}
	}

	registerUser() {
		const fv1 = this.step1.value; //Formulario con los datos del usuario
		const fv2 = this.step2.value; //Formulario con la contraseña
		const fv3 = this.step3.value; // Formulario con el código sms

		const userToRegister = new RegisterModel(
			fv2.keyWord,
			fv1.firstName,
			fv1.surname,
			fv1.birthdate,
			fv1.email,
			fv1.mobile,
			this.lang,
			fv1.genre,
			(fv1.prefix as string).replace("+", ""),
			fv1.profileEditor,
			fv1.profileStudent,
			fv1.profileTeacher,
			fv1.profileParent
		);

		this.registerService.mobileVerified(fv3.code, userToRegister).subscribe(
			(result) => {
				if (result.error.code === 0) {
					this.ma.event("submit", "sign_up", "Enviar");

					this.toaster.success(
						this.translateService.instant("REGISTRO.OKREGISTER")
					);
					//Si todo sale correctamente, logueamos al usuario en la aplicación
					const login: LoginModel = new LoginModel(
						(fv1.mobile as string).trim(),
						(fv2.keyWord as string).trim(),
						(fv1.prefix as string).replace("+", "")
					);

					this.loginService.login(login).subscribe(
						(res) => {
							if (res.data.ok) {
								// Guardo el usuario en el localstorage
								this.loginService.setUserLocalStorage(res.data.userDTO);
								this.loginService.setToken(
									res.data.tokenType,
									res.data.tokenAcces
								);
								this.ma.setUserId(res.data.userDTO.idUser);
								// Y le redirecciono al grafo de entrada
								this.router.navigateByUrl(this.courseInitial); // Go to initial course

								this.ma.event("submit", "login", "Enviar"); //Matomo event
							} else {
								this.toaster.error(
									this.translateService.instant("LOGIN.ERRORALVALIDARSE")
								);
							}
						},
						(err) => {
							this.toaster.error(
								this.translateService.instant("LOGIN.ERRORALVALIDARSE")
							);
						}
					);
				} else {
					//Error al registar al usuario
					this.toaster.error(result.error.msg);
				}
			},
			(err) => {
				this.toaster.error(
					this.translateService.instant("ERROR.HAOCURRIDOUNERROR")
				);
			}
		);
	}

	registerUserByMail() {
		const fv1 = this.step1.value; //Formulario con los datos del usuario
		const email = fv1.email;
		const fv2 = this.step2.value; //Formulario con la contraseña
		const fv3 = this.step3.value; // Formulario con el código sms

		const userToRegister = new RegisterModel(
			fv2.keyWord,
			fv1.firstName,
			fv1.surname,
			fv1.birthdate,
			fv1.email,
			fv1.mobile,
			this.lang,
			fv1.genre,
			(fv1.prefix as string).replace("+", ""),
			fv1.profileEditor,
			fv1.profileStudent,
			fv1.profileTeacher,
			fv1.profileParent
		);

		this.registerService
			.mobileVerifiedByMail(fv3.code, userToRegister)
			.subscribe(
				(result) => {
					if (result.error.code === 0) {
						this.ma.event("submit", "sign_up", "Enviar");

						this.toaster.success(
							this.translateService.instant("REGISTRO.OKREGISTER")
						);
						//Si todo sale correctamente, logueamos al usuario en la aplicación
						const login: LoginModel = new LoginModel(
							(fv1.mobile as string).trim(),
							(fv2.keyWord as string).trim(),
							(fv1.prefix as string).replace("+", "")
						);
						login.phone = email;

						this.loginService.login(login).subscribe(
							(res) => {
								if (res.data.ok) {
									// Guardo el usuario en el localstorage
									this.loginService.setUserLocalStorage(res.data.userDTO);
									this.loginService.setToken(
										res.data.tokenType,
										res.data.tokenAcces
									);
									this.ma.setUserId(res.data.userDTO.idUser);
									// Y le redirecciono al grafo de entrada
									this.router.navigateByUrl(this.courseInitial); // Go to initial course

									this.ma.event("submit", "login", "Enviar"); //Matomo event
								} else {
									this.toaster.error(
										this.translateService.instant("LOGIN.ERRORALVALIDARSE")
									);
								}
							},
							(err) => {
								this.toaster.error(
									this.translateService.instant("LOGIN.ERRORALVALIDARSE")
								);
							}
						);
					} else {
						//Error al registar al usuario
						this.toaster.error(result.error.msg);
					}
				},
				(err) => {
					this.toaster.error(
						this.translateService.instant("ERROR.HAOCURRIDOUNERROR")
					);
				}
			);
	}

	googleSignin() {
		//this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
	}

	showPrivacyDialog(event: MouseEvent) {
		event.preventDefault();
		event.stopPropagation();
		this.dialogService.openPrivacyDialog();
	}

	showConditionsDialog(event: MouseEvent) {
		event.preventDefault();
		this.dialogService.openConditionsDialog();
	}
	closeModal() {
		this.router.navigateByUrl(this.loginRoute);
	}

	onKeyUpCode(ev): void {
		//Cuando sean 6 dígitos, tenemos que enviar el código al servidor para poder registar y validar al usuario
		if (ev.target.value.length === 6) {
			//this.step3.get("code").disable();
			this.registerUser();
		}
	}

	onKeyUpCodeMail(ev): void {
		//Cuando sean 6 dígitos, tenemos que enviar el código al servidor para poder registar y validar al usuario
		if (ev.target.value.length === 6) {
			//this.step3.get("code").disable();
			this.registerUserByMail();
		}
	}

	requestNewCode(event: any): void {
		this.clickNewCode = true;
		this.sendSms();
	}

	sendMail(): void | boolean {
		if (this.step2.invalid) return false;

		const fv1 = this.step1.value; //Formulario con los datos del usuario
		const prefix: string = fv1.prefix.replace("+", "");

		this.clickNewEmail = true;

		this.registerService
			.requestMailCode(prefix, fv1.mobile, fv1.email)
			.subscribe(
				(result) => {
					//Si está correcto, pasamos al último step
					if (result.data !== null) {
						this.inputSMS = false;
						this.toaster.success(
							this.translateService.instant("WHATSAPP.CODESENDEDMAIL")
						);
					} else {
						//Si hay algún error, debemos mostrarlo aqui
						this.toaster.error(
							this.translateService.instant("WHATSAPP.CODENOTSENDEDMAIL")
						);
					}
				},
				(err) => {
					//Mostramos un error si ha salido mal el proceso (error genérico)
					this.toaster.error(
						this.translateService.instant("ERROR.HAOCURRIDOUNERROR")
					);
				}
			);
	}

	private getCenterInfo(): void {
		if (this.centerService.currentConfig) {
			this.courseInitial = `course/${this.centerService.currentConfig.idCourse}/graph/${this.centerService.currentConfig.idTarget}`;
		} else {
			this.subs = this.centerService.centerConfig.subscribe((data) => {
				this.courseInitial = `course/${data.idCourse}/graph/${data.idTarget}`;
			});
		}
	}

	verificarMovil() {
		const fv1 = this.step1.value;
		if (fv1.mobile != "") {
			this.registerService
				.mobileValidation(fv1.mobile, (fv1.prefix as string).replace("+", ""))
				.subscribe((res) => {
					this.mobileOk = res.data;
					if (res.error.msg === "MOBILE.EXIST") {
						this.toaster.error(
							this.translateService.instant(res.error.msg)
						);
					}
				});
		} else {
			this.mobileOk = true;
		}
	}

	verificarMail() {
		const fv1 = this.step1.value;
		this.registerService.mailValidation(fv1.email).subscribe((res) => {
			this.mailOk = res.data;
			if (res.error.msg === "MAIL.EXIST") {
				this.toaster.error(
					this.translateService.instant(res.error.msg)
				);
			}
		});
	}

	reset() {
		this.step1.reset();
		this.step2.reset();
		this.step3.reset();
		this.load();
		this.clickNewCode = false;
		this.clickNewEmail = false;
		this.currentStep = STEPS.STEP1;
	}

	registerNick() {
		this.registerAux = true;
		//const modalRef = this.modalService.open(RegisterNickComponent,{scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W60} h-100` });
		//this.closeModal();
	}
}
