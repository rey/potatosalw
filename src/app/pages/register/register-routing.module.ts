import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoValidationGuard } from 'src/app/core/guards/demo-validation.guard';
import { RegisterComponent } from './register.component';

const routes: Routes = [
    {
        path: '',
        component: RegisterComponent,
        canActivate: [DemoValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RegisterRoutingModule { }
