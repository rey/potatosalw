import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoValidationGuard } from 'src/app/core/guards/demo-validation.guard';
import { ValidateProfileComponent } from './validate-profile.component';

const routes: Routes = [
    {
        path: ':id',
        component: ValidateProfileComponent,
        canActivate: [DemoValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ValidateProfileRoutingModule { }
