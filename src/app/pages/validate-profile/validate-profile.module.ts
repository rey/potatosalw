import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ValidateProfileRoutingModule } from './validate-profile-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ValidateProfileComponent } from './validate-profile.component';

@NgModule({
    declarations: [ValidateProfileComponent],
    imports: [
        CommonModule,
        ValidateProfileRoutingModule,
        SharedModule
    ]
})
export class ValidateProfileModule { }
