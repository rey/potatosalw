import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { environment } from 'src/environments/environment';

// Utils
import { LocalStorage } from 'src/app/core/utils';

// Services
import { TranslateService } from '@ngx-translate/core';
import { AlertService, TitleService } from 'src/app/core/services/shared';
import { UsersService } from 'src/app/core/services/users';
import { LoginService } from 'src/app/core/services/login';

// Models
import { UserProfileValidationModel } from 'src/app/core/models/users';

// Mappers
import { UsersProfileValidationMapperService } from 'src/app/core/services/mapper';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';

declare function init_plugins();

@Component({
    selector: 'app-validate',
    styleUrls: ['../login/login.component.scss'],
    templateUrl: './validate-profile.component.html'
})
export class ValidateProfileComponent implements OnInit {

    public formularioClave: UntypedFormGroup;

    private uuid: string;
    public mensajeValidacion: string;

    private userProfileValidation: UserProfileValidationModel;

    // tslint:disable-next-line: member-ordering
    validationMessages = {
        clave: []
    };

    public loading = false;
    public ocultarAcciones = false;

    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private formBuild: UntypedFormBuilder,
        private titleService: TitleService,
        public translateService: TranslateService,
        private alertService: AlertService,
        private localStorage: LocalStorage,
        private usersService: UsersService,
        private loginService: LoginService,
        private usersProfileValidationMapperService: UsersProfileValidationMapperService) {

        this.activatedRoute.params.subscribe(params => {
            this.uuid = params['id'];
        });
    }

    ngOnInit(): void {

        init_plugins();

        this.crearFormulario();

        this.traducirOpciones();
    }

    crearFormulario() {

        this.formularioClave = this.formBuild.group({
            clave: ['', Validators.required]
        });
    }

    traducirOpciones() {

        // Recupero el lenguaje
        const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
        this.translateService.use(lang);

        // Titulo pagina
        this.translateService.get('VALIDAR.VALIDARDATOS').subscribe((res: string) => {
            this.titleService.setHTMLTitle(res);
        });

        this.translateService.get('VALIDACIONES.LACLAVEESOBLIGATORIA').subscribe((res: string) => {
            this.validationMessages.clave.push({ type: 'required', message: res });
        });
    }

    validarClave() {

        if (this.formularioClave.invalid) {
            return;
        }

        const campos = this.formularioClave.value;

        this.loading = true;
        this.loading = false;
    }
}
