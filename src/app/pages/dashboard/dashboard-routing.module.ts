import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileValidationGuard } from 'src/app/core/guards';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
    // {
    //     path: '',
    //     component: DashboardComponent
    // },
    {
        path: ':id',
        component: DashboardComponent,
        canActivate: [ProfileValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
