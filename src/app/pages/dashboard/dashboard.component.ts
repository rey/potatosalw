import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

// Services
import { TranslateService } from '@ngx-translate/core';
import { TitleService } from 'src/app/core/services/shared';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              public translateService: TranslateService,
              private titleService: TitleService,
              ) {

                this.activatedRoute.params.subscribe(params => {
                  // De esta forma cada vez que cambia el parametro lo actualizo
                  this.router.routeReuseStrategy.shouldReuseRoute = () => false;
                });
  }

  ngOnInit() {

    this.translateService.get('DASHBOARD.SALWARE').subscribe((res: string)  => {
      this.titleService.setHTMLTitle(res);
    });
  }

}
