import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MobileFormComponent } from './components/mobile-form/mobile-form.component';
import { SelectUserComponent } from './components/select-user/select-user.component';
import { SelectEmailComponent } from './components/select-email/select-email.component';
import { NbButtonModule, NbCardModule, NbLayoutModule, NbProgressBarModule, NbActionsModule, NbInputModule, NbIconModule, NbSearchModule, NbToggleModule, NbSelectModule, NbDatepickerModule, NbTooltipModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
    declarations: [ForgotPasswordComponent, MobileFormComponent, SelectUserComponent, SelectEmailComponent],
    imports: [
        CommonModule,
        ForgotPasswordRoutingModule,
        SharedModule,
				NbButtonModule,
				NbCardModule,
				NbLayoutModule,
				NbProgressBarModule,
				NbActionsModule,
				NbInputModule,
				NbIconModule,
				NbSearchModule,
				NbToggleModule,
				NbSelectModule,
				NbDatepickerModule,
				NbTooltipModule
    ]
})
export class ForgotPasswordModule { }
