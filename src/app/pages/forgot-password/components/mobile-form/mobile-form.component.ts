import { DEFAULTMOBILEPREFIX } from "../../../../core/models/masters/masters.enum";
import { LoginService } from "src/app/core/services/login";
import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewEncapsulation,
} from "@angular/core";
import {
	UntypedFormBuilder,
	UntypedFormControl,
	UntypedFormGroup,
	Validators,
} from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Observable, Subscription } from "rxjs";
import { PrefixCountry } from "src/app/core/models/shared/prefixCountry.model";
import { TitleService } from "src/app/core/services/shared";
import { LocalStorage } from "src/app/core/utils";
import { ROUTES_NAME } from "src/app/core/utils/routes-name";
import { LOCALSTORAGESTRINGS } from "src/app/core/models/masters/localstorage.enum";
import { PhoneEmitterModel } from "src/app/core/models/masters/config.model";
import { Router } from "@angular/router";
import { CenterService } from "src/app/core/services/center/center.service";
import { environment } from "src/environments/environment";
import { DomainTypes } from "src/app/core/models/masters/masters.enum";
import { RegisterNickComponent } from "src/app/pages/register/register-nick/register-nick.component";
import { MODAL_DIALOG_TYPES } from "src/app/core/utils/modal-dialog-types";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { RecuperarPassNickComponent } from "./recuperar-pass-nick/recuperar-pass-nick.component";

@Component({
	selector: "app-mobile-form",
	templateUrl: "./mobile-form.component.html",
	styleUrls: ["./mobile-form.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class MobileFormComponent implements OnInit {
	@Input() isLoading: boolean = false;
	@Output() phoneSent: EventEmitter<PhoneEmitterModel> =
		new EventEmitter<PhoneEmitterModel>();

	public formulario: UntypedFormGroup;
	public loading = false;
	public loginRoute: string = ROUTES_NAME.LOGIN;
	$phonePrefixes: Observable<PrefixCountry[]>;

	validationMessages = {
		mobile: [],
		prefix: [],
	};

	loginImage:any;
	courseInitial:any;
	idCenter: number = JSON.parse(
		this.localStorage.getItem(LOCALSTORAGESTRINGS.CONFIGSITE)
	).idCenter;
	isDemo: boolean = environment.domain === DomainTypes.DEMO ? true : false;
	isPro: boolean = environment.domain === DomainTypes.SALWARE ? true : false;
	private subs = new Subscription();

	registerAux: boolean = false;

	constructor(
		private router: Router,
		private formBuild: UntypedFormBuilder,
		private titleService: TitleService,
		public translateService: TranslateService,
		private localStorage: LocalStorage,
		private loginService: LoginService,
		private centerService: CenterService,
		private modalService: NgbModal,
	) {}

	ngOnInit(): void {
		this.crearFormulario();
		this.traducirOpciones();
		this.getCenterInfo();
		this.$phonePrefixes = this.loginService.getPhonePrefix();
	}

	private getCenterInfo(): void {
		if (this.centerService.currentConfig) {
			this.loginImage = this.centerService.currentConfig.imgLogin;
		} else {
			this.subs = this.centerService.centerConfig.subscribe((data) => {
				if (this.idCenter === 5) {
					this.loginImage = environment.logoHome;
				} else {
					this.loginImage = data.imgLogin;
				}
			});
		}
	}

	crearFormulario() {
		this.formulario = this.formBuild.group({
			prefix: [DEFAULTMOBILEPREFIX, Validators.required],
			mobile: ["", { validators: [Validators.required] }],
		});
	}

	get mobileControl(): UntypedFormControl {
		if (this.formulario) {
			return this.formulario.get("mobile") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get prefixControl() {
		if (this.formulario) {
			return this.formulario.get("prefix") as UntypedFormControl;
		} else {
			return null;
		}
	}

	closeModal() {
		this.router.navigateByUrl(this.loginRoute);
	}

	traducirOpciones() {
		const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
		this.translateService.use(lang);

		// Titulo pagina
		this.translateService
			.get("RECUPERARCONTRASENA.RECUPERARCONTRASENA")
			.subscribe((res: string) => {
				this.titleService.setHTMLTitle(res);
			});

		this.translateService
			.get("VALIDACIONES.ELTELEFONOMOVILESOBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.mobile.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.PREFIXREQUIRED")
			.subscribe((res: string) => {
				this.validationMessages.prefix.push({ type: "required", message: res });
			});
	}

	recuperarContrasena() {
		this.formulario.markAllAsTouched();

		let value: PhoneEmitterModel = {
			mobile: (this.mobileControl.value as string).trim(),
			prefix: (this.prefixControl.value as string).replace("+", "").trim(),
		};

		if (this.formulario.valid) {
			this.phoneSent.emit(value);
		}
	}

	recoveryPassNick() {
		this.registerAux = true;
		const modalRef = this.modalService.open(RecuperarPassNickComponent,{scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W60} h-100` });
		this.closeModal();
	}

}
