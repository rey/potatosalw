import { Component, OnDestroy, Output, EventEmitter, OnInit, ViewEncapsulation } from "@angular/core";
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from "@angular/forms";

// Services
import { TranslateService } from "@ngx-translate/core";
import { RegisterService } from "src/app/core/services/register/register.service";
import { ROUTES_NAME } from "src/app/core/utils/routes-name";
import { Subscription } from "rxjs";
import { formsValidations } from "src/app/core/utils/forms-validations";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { ToasterService } from "src/app/core/services/shared/toaster.service";

declare function init_plugins();

enum STEPS {
	STEP1 = "1",
	STEP2 = "2",
}

@Component({
	selector: "app-recuperar-pass-nick",
	styleUrls: ["./recuperar-pass-nick.component.scss"],
	templateUrl: "./recuperar-pass-nick.component.html",
	encapsulation: ViewEncapsulation.None,
})
export class RecuperarPassNickComponent implements OnInit, OnDestroy {
	@Output() closeAux: EventEmitter<boolean> = new EventEmitter<boolean>();
	step2: UntypedFormGroup;
	step1: UntypedFormGroup;
	hidePassword: boolean = true;
	hidePasswordR: boolean = true;
	bgImage: string;
	lang: string;
	currentStep: string = STEPS.STEP1;
	validationMessages = {
		nick: [],
		keyword: [],
		repeatKeyWord: [],
		terms: [],
		paper: [],
		idSecurityQuestions: [],
		securityAnswer: [],
		idSecurityQuestions2: [],
		securityAnswer2: []
	};
	loading: boolean = false;
	loginRoute = ROUTES_NAME.LOGIN;

	securityQuestions: { id: number; question: string }[] = [
    //{ id: 0, question: ""},
    { id: 1, question: this.translateService.instant("SECURITYQUESTIONS.QUESTION1")},
    { id: 2, question: this.translateService.instant("SECURITYQUESTIONS.QUESTION2")},
    { id: 3, question: this.translateService.instant("SECURITYQUESTIONS.QUESTION3")},
    { id: 4, question: this.translateService.instant("SECURITYQUESTIONS.QUESTION4")},
    { id: 5, question: this.translateService.instant("SECURITYQUESTIONS.QUESTION5")},
  ];

	roleAsignado: boolean = true;
	nickOk: boolean = false;
	private token: string;
	private idUser: number;

	private nickMaxLength: number = formsValidations.NICK_MAX_LENGTH;
	private nickMinLength: number = formsValidations.NICK_MIN_LENGTH;
	private passMaxLenght: number = formsValidations.PASS_MAX_LENGTH;
	private emailMaxLength: number = formsValidations.EMAIL_MAX_LENGTH;
	private emailPattern: string = formsValidations.EMAIL_PATTERN;
	private subs = new Subscription();
	private courseInitial: string;

	constructor(
		private formBuild: UntypedFormBuilder,
		public translateService: TranslateService,
		private registerService: RegisterService,
		private toaster: ToasterService,
		public activeModal: NgbActiveModal,
	) { }

	ngOnInit(): void {
		this.crearFormularioInicial();
	}

	ngOnDestroy(): void {
		this.subs.unsubscribe();
	}

	closeModal() {
		//this.router.navigateByUrl(this.loginRoute);
		this.activeModal.close();
	}

	crearFormularioInicial() {
		this.step1 = this.formBuild.group({
			nick: ["",
				{
					validators: [
						Validators.required,
						Validators.maxLength(this.nickMaxLength),
						Validators.minLength(this.nickMinLength),
						Validators.pattern(formsValidations.NICKPATTERN)
					],
				},
			],
			idSecurityQuestions: ["", [Validators.required, Validators.pattern(formsValidations.QUESTION_PATTERN)]],
			securityAnswer: ["", [Validators.required, Validators.minLength(formsValidations.ANSWER_MIN_LENGTH)]],
			idSecurityQuestions2: ["", [Validators.required, Validators.pattern(formsValidations.QUESTION_PATTERN)]],
			securityAnswer2: ["", [Validators.required, Validators.minLength(formsValidations.ANSWER_MIN_LENGTH)]]
		});
	}

	get nickControl(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("nick") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get question1Control(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("idSecurityQuestions") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get answer1Control(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("securityAnswer") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get question2Control(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("idSecurityQuestions2") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get answer2Control(): UntypedFormControl {
		if (this.step1) {
			return this.step1.get("securityAnswer2") as UntypedFormControl;
		} else {
			return null;
		}
	}

	validateUser(){
		const fv1 = this.step1.value;
		this.registerService.nickSecurityQuestionsAnswerValidation(fv1.nick, fv1.idSecurityQuestions, fv1.securityAnswer, fv1.idSecurityQuestions2, fv1.securityAnswer2).subscribe((res) => {
				if (res.error.code === 1) {
					this.toaster.error(this.translateService.instant(res.error.msg));
				} else{
					this.nextStep();
				}
			});
	}

	nextStep() {
		if(this.step1.invalid){
			false;
		} else {
			this.currentStep = STEPS.STEP2;
			this.crearFormulario();
		}
	}

	crearFormulario() {
		this.step2 = this.formBuild.group({
			keyWord: ["",	{validators: [Validators.required, Validators.maxLength(this.passMaxLenght), Validators.pattern(formsValidations.PASSWORDPATTERN)],},],
			repeatKeyWord: ["",	{validators: [Validators.required, Validators.maxLength(this.passMaxLenght), Validators.pattern(formsValidations.PASSWORDPATTERN)],},],
	});
	}

	get keywordControl(): UntypedFormControl {
		if (this.step2) {
			return this.step2.get("keyWord") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get repeatKeywordControl(): UntypedFormControl {
		if (this.step2) {
			return this.step2.get("repeatKeyWord") as UntypedFormControl;
		} else {
			return null;
		}
	}

	validateRepeatPassword() {
    this.step2.get('repeatKeyWord').updateValueAndValidity();
  }

	cambiarContrasena() {
		if(this.step2.invalid){
			this.toaster.error(this.translateService.instant("RECUPERARCONTRASENA.RECUPERARCONTRASENA"));
		} else{
			this.toaster.success(this.translateService.instant("RECUPERARCONTRASENA.RECUPERARCONTRASENA"));
		}
	}
}
