import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { User } from "src/app/core/models/users/user.models";
import { RegisterService } from "src/app/core/services/register/register.service";
import { CenterService } from 'src/app/core/services/center/center.service';
import { Subscription } from "rxjs";
import { LOCALSTORAGESTRINGS } from "src/app/core/models/masters/localstorage.enum";
import { LocalStorage } from "src/app/core/utils";
import { environment } from "src/environments/environment";
import { DomainTypes } from "src/app/core/models/masters/masters.enum";
import { ToasterService } from "src/app/core/services/shared/toaster.service";

@Component({
	selector: "app-select-user",
	templateUrl: "./select-user.component.html",
	styleUrls: ["./select-user.component.scss"],
})
export class SelectUserComponent implements OnInit {
	@Input() isLoading: boolean = false;
	@Input() userList: User[] = [];
	@Output() goBack: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output() userSelected: EventEmitter<User> = new EventEmitter<User>();
	validoPin = false;
	validarSms: UntypedFormGroup;
	@Input() prefix: any;
	@Input() mobile: any;
	loginImage:any;
	courseInitial:any;
	validationMessages = {
		code: [],
	};
	idCenter: number = JSON.parse(
		this.localStorage.getItem(LOCALSTORAGESTRINGS.CONFIGSITE)
	).idCenter;
	isDemo: boolean = environment.domain === DomainTypes.DEMO ? true : false;
	isPro: boolean = environment.domain === DomainTypes.SALWARE ? true : false;


	private subs = new Subscription();

	constructor(
		private fb: UntypedFormBuilder,
		private translateService: TranslateService,
		private registerService: RegisterService,
		private centerService: CenterService,
		private localStorage: LocalStorage,
		private toaster: ToasterService
	) {
		this.validarSms = this.fb.group({
			code: ["", { validators: [Validators.required] }],
		});
	}

	ngOnInit() {
		this.getCenterInfo()
	}

	onGoBack() {
		this.goBack.emit(true);
	}

	onUserSelected(user: User) {
		this.userSelected.emit(user);
	}

	traducirOpciones() {
		this.translateService
			.get("VALIDACIONES.CODEREQUIRED")
			.subscribe((res: string) => {
				this.validationMessages.code.push({ type: "required", message: res });
			});
	}

	onKeyUpCode(ev): void {
		//Cuando sean 6 dígitos, tenemos que enviar el código al servidor para poder registar y validar al usuario
		if (ev.target.value.length === 6) {
			this.validarCodigo();
		}
	}

	validarCodigo() {
		const fv3 = this.validarSms.value; // Formulario con el código sms

		this.registerService
			.smsPinVerified(fv3.code, this.prefix, this.mobile)
			.subscribe(
				(result) => {
					if (result.error.code === 0) {
						this.toaster.success(
							this.translateService.instant("SELECTUSER.VALIDCODE")
						);
						this.validoPin = true;
						this.validarSms.get("code").disable();
					} else {
						//Error al registar al usuario
						this.toaster.error(result.error.msg);
					}
				},
				(err) => {
					this.toaster.error(
						this.translateService.instant("ERROR.HAOCURRIDOUNERROR")
					);
				}
			);
	}

	requestNewCode(): void {
		this.registerService
			.mobileRequestSms(this.prefix, this.mobile)
			.subscribe((res) => {
				this.toaster.success(
					this.translateService.instant("SELECTUSER.CODESENDED")
				);
			});
	}



	private getCenterInfo(): void {
		if (this.centerService.currentConfig) {
			this.loginImage = this.centerService.currentConfig.imgLogin;
		} else {
			this.subs = this.centerService.centerConfig.subscribe((data) => {
				if (this.idCenter === 5) {
					this.loginImage = environment.logoHome;
				} else {
					this.loginImage = data.imgLogin;
				}
			});
		}
	}
}
