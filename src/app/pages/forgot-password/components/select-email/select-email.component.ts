import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/core/models/users/user.models';

@Component({
    selector: 'app-select-email',
    templateUrl: './select-email.component.html',
    styleUrls: ['./select-email.component.scss']
})
export class SelectEmailComponent implements OnInit {
    @Input() isLoading: boolean = false;
    @Input() emails: User[];
    @Output() goBack: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() emailSelected: EventEmitter<User> = new EventEmitter<User>();

    constructor() { }

    ngOnInit() {
    }

    onEmailSelected(user) {
        this.emailSelected.emit(user);
    }

    onGoBack() {
        this.goBack.emit(true);
    }
}
