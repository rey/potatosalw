import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Services
import { TranslateService } from '@ngx-translate/core';

// Models
import { ROUTES_NAME } from 'src/app/core/utils/routes-name';
import { ForgotPasswordService } from 'src/app/core/services/forgot-password/forgot-password.service';
import { finalize, take } from 'rxjs/operators';
import { User } from 'src/app/core/models/users/user.models';
import { DialogService } from 'src/app/core/services/shared/dialog.service';
import { PhoneEmitterModel } from 'src/app/core/models/masters/config.model';

declare function init_plugins();

enum FORGOT_PASSWORD_STEPS {
    PHONE,
    USER,
    EMAIL
}

@Component({
    selector: 'app-forgot-password',
    styleUrls: ['./forgot-password.component.scss'],
    templateUrl: './forgot-password.component.html',
})
export class ForgotPasswordComponent implements OnInit {
    public step: FORGOT_PASSWORD_STEPS = FORGOT_PASSWORD_STEPS.PHONE;
    public userList: User[] = [];
    public emailList: User[] = [];
    public isLoading: boolean = false;

    private userSelected: User;
    private emailSelected: User;
    mobile: string;
    prefix: string;

    constructor(private forgotPasswordService: ForgotPasswordService, private translateService: TranslateService, private router: Router, private dialogService: DialogService) {

    }

    ngOnInit(): void {
        init_plugins();
    }

    onPhoneSent(ev: PhoneEmitterModel) {
        this.isLoading = true;

        this.mobile = ev.mobile;
        this.prefix = ev.prefix;
        this.forgotPasswordService.getUsersByMobile(ev.mobile, ev.prefix).pipe(finalize(() => this.isLoading = false)).subscribe(
            res => {
                if (!res || res.length === 0 || res.filter(u => u.mail).length === 0) {
                    this.dialogService.openConfirmDialog(false, this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), this.translateService.instant('RECUPERARCONTRASENA.NOUSERERROR'), ['dialog-80']);
                } else {
                    this.userList = res;
                    this.step = FORGOT_PASSWORD_STEPS.USER;
                }

            },
            err => {
                this.dialogService.openConfirmDialog(false, this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), this.translateService.instant('RECUPERARCONTRASENA.ERROR'), ['dialog-80']);
            }
        );
    }

    onGoBack() {
        this.step--;
    }

    onUserSelected(user: User) {
        this.userSelected = user;
        this.emailList = this.userList.filter(u => u.mail);
        this.step = FORGOT_PASSWORD_STEPS.EMAIL;
    }

    onEmailSelected(email: User) {
        this.emailSelected = email;

        this.isLoading = true;
        this.forgotPasswordService.forgotPassword(this.userSelected.idUser, this.emailSelected.idUser).pipe(finalize(() => this.isLoading = false)).subscribe(
            res => {
                const dialog = this.dialogService.openConfirmDialog(true, this.translateService.instant('RECUPERARCONTRASENA.OKTITLE'), this.translateService.instant('RECUPERARCONTRASENA.EMAILENVIADO'), ['dialog-80']);

                dialog.afterClosed().pipe(take(1)).subscribe(res =>
                    this.router.navigate([ROUTES_NAME.LOGIN])
                );
            },
            err => {
                const dialog = this.dialogService.openConfirmDialog(false, this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), this.translateService.instant('RECUPERARCONTRASENA.ERROR'), ['dialog-80']);

                dialog.afterClosed().pipe(take(1)).subscribe(res =>
                    this.step = FORGOT_PASSWORD_STEPS.PHONE
                );
            }
        )
    }
}
