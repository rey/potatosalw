import { TestBed } from '@angular/core/testing';

import { ForgotPasswordValidationService } from './forgot-password-validation.service';

describe('ForgotPasswordValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ForgotPasswordValidationService = TestBed.get(ForgotPasswordValidationService);
    expect(service).toBeTruthy();
  });
});
