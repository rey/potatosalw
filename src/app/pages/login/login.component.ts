
import { User } from "src/app/core/models/users/user.models";
import { MatomoAnalyticsUtils } from "./../../core/utils/matomo-analytics.utils";
import {
	AppVersions,
	DEMOUSER,
} from "./../../core/models/masters/masters.enum";
import {
	DEFAULTMOBILEPREFIX,
	DomainTypes,
} from "../../core/models/masters/masters.enum";
import { CenterService } from "./../../core/services/center/center.service";
import { Observable, Subscription } from "rxjs";
import { Component, OnInit, OnDestroy, ViewEncapsulation } from "@angular/core";
import {
	UntypedFormBuilder,
	UntypedFormControl,
	UntypedFormGroup,
	Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { finalize, take } from "rxjs/operators";

// Utils
import { LocalStorage } from "src/app/core/utils";

// Services
import { TranslateService } from "@ngx-translate/core";
import { AlertService, TitleService } from "src/app/core/services/shared";
import { LoginService } from "src/app/core/services/login";
import { ROUTES_NAME } from "src/app/core/utils/routes-name";
import { LoginModel } from "src/app/core/models/users/login.model";
import { UtilsService } from "src/app/core/services/shared/utils.service";
import { MatLegacyDialog as MatDialog } from "@angular/material/legacy-dialog";
import { AcceptDialogComponent } from "src/app/shared/components/accept-dialog/accept-dialog.component";
import { environment } from "src/environments/environment";
import { PrefixCountry } from "src/app/core/models/shared/prefixCountry.model";
import { LOCALSTORAGESTRINGS } from "src/app/core/models/masters/localstorage.enum";
import { ModalSendCommentsComponent } from "src/app/shared/components/modal-send-comments/modal-send-comments.component";
import { NbDialogService } from "@nebular/theme";
import { MODAL_DIALOG_TYPES } from "src/app/core/utils/modal-dialog-types";
import { UsersService } from "src/app/core/services/users";
import { register } from "src/app/store/actions/user.action";
import { Store } from "@ngrx/store";
import { State } from "src/app/store/models/store.model";

declare function init_plugins();

@Component({
	selector: "app-login",
	styleUrls: ["./login.component.scss"],
	templateUrl: "./login.component.html",
	encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit, OnDestroy {
	public formulario: UntypedFormGroup;
	public existeTelefono = false;
	public loading = false;
	public hidePassword = true;
	public loginImage: string;
	private subs = new Subscription();
	private courseInitial: string;
	$phonePrefixes: Observable<PrefixCountry[]>;
	isDemo: boolean = environment.domain === DomainTypes.DEMO ? true : false;
	isPro: boolean = environment.domain === DomainTypes.SALWARE ? true : false;
	idCenter: number;
	forgotPasswordRoute = ROUTES_NAME.FORGOT_PASSWORD;
	signUpRoute = ROUTES_NAME.SIGN_UP;
	registerNewUserRoute = ROUTES_NAME.REGISTER_NEW_USER;
	loginRoute = ROUTES_NAME.LOGIN;
	version: string = AppVersions.PROD;
	versionAPI: string = AppVersions.API;

	validationMessages = {
		nick: [],
		password: [],
		prefix: [],
	};

	public constructor(
		private router: Router,
		private formBuild: UntypedFormBuilder,
		private titleService: TitleService,
		public translateService: TranslateService,
		private alertService: AlertService,
		private localStorage: LocalStorage,
		private loginService: LoginService,
		private utils: UtilsService,
		private dialog: MatDialog,
		private ma: MatomoAnalyticsUtils,
		private centerService: CenterService,
		private nebularDialogService: NbDialogService,
		public usersService: UsersService,
		private store: Store<State>
	) {
		const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
		this.translateService.use(lang);
		this._checkScreenMode();
	}

	public ngOnInit() {
		this.idCenter = JSON.parse(this.localStorage.getItem(LOCALSTORAGESTRINGS.CONFIGSITE))
			.idCenter;
		init_plugins();

		this.getCenterInfo();
		this.crearFormulario();

		this.traducirOpciones();
		this.$phonePrefixes = this.loginService.getPhonePrefix();

		if (this.isDemo) {
			this.login();
		}
	}

	crearFormulario() {
		// default data and form groups
		this.formulario = this.formBuild.group({
			nick: [this.isDemo ? DEMOUSER.USER : "", Validators.required],
			keyWord: [this.isDemo ? DEMOUSER.PASS : "", Validators.required],
			prefix: [DEFAULTMOBILEPREFIX, Validators.required],
		});
	}

	private validateUserControl() {
		// Compruebo si existe datos en localstorage del usuario y la sesion es valida
		if (this.loginService.validateUserLocalStorage()) {
			const currentUser: User = JSON.parse(
				this.localStorage.getItem(LOCALSTORAGESTRINGS.USER)
			) as User;
			this.ma.setUserId(currentUser.idUser);
			// En este caso le redirecciono a la pagina de entrada
			this.router.navigateByUrl(this.courseInitial);
		}
	}

	get nickControl() {
		if (this.formulario) {
			return this.formulario.get("nick") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get keyWordControl() {
		if (this.formulario) {
			return this.formulario.get("keyWord") as UntypedFormControl;
		} else {
			return null;
		}
	}

	get prefixControl() {
		if (this.formulario) {
			return this.formulario.get("prefix") as UntypedFormControl;
		} else {
			return null;
		}
	}

	traducirOpciones() {
		this.translateService.get("LOGIN.ACCEDER").subscribe((res: string) => {
			this.titleService.setHTMLTitle(res);
		});

		this.translateService
			.get("VALIDACIONES.MAILOTELEFONOBLIGATORIO")
			.subscribe((res: string) => {
				this.validationMessages.nick.push({ type: "required", message: res });
			});

		this.translateService
			.get("VALIDACIONES.LACONTRASENAESOBLIGATORIA")
			.subscribe((res: string) => {
				this.validationMessages.password.push({
					type: "required",
					message: res,
				});
			});

		this.translateService
			.get("VALIDACIONES.PREFIXREQUIRED")
			.subscribe((res: string) => {
				this.validationMessages.prefix.push({ type: "required", message: res });
			});
	}

	public resetForm() {
		this.formulario.value.nick = "";
		this.formulario.value.keyWord = "";
	}

	private getCenterInfo(): void {
		if (this.centerService.currentConfig) {
			this.loginImage = this.centerService.currentConfig.imgLogin;
			this.courseInitial = `course/${this.centerService.currentConfig.idCourse}/graph/${this.centerService.currentConfig.idTarget}`;
			this.validateUserControl();
		} else {
			this.subs = this.centerService.centerConfig.subscribe((data) => {
				if (this.idCenter === 5) {
					this.loginImage = environment.logoHome;
				} else {
					this.loginImage = data.imgLogin;
				}
				this.courseInitial = `course/${data.idCourse}/graph/${data.idTarget}`;
				this.validateUserControl();
			});
		}
	}

	public login() {
		this.alertService.clear();
		if (this.formulario.valid) {
			this.loading = true;
			const fv = this.formulario.value;
			const login: LoginModel = new LoginModel(
				(fv.nick as string).trim(),
				(fv.keyWord as string).trim(),
				(fv.prefix as string).replace("+", "")
			);

			this.loginService
				.login(login)
				.pipe(finalize(() => (this.loading = false)))
				.subscribe(
					(res) => {
						if (res.data != null) {
							// Guardo el usuario en el localstorage
							this.loginService.setUserLocalStorage(res.data.userDTO);
							let loggedInUser = {
								idUser: res.data.userDTO.idUser
							}
							this.store.dispatch(new register(res.data.userDTO));
							this.loginService.setToken(
								res.data.tokenType,
								res.data.tokenAcces
							);
							this.ma.setUserId(res.data.userDTO.idUser);
							localStorage.removeItem("selectedGroupInfo");

							this.usersService
								.getListUserProfile()
								.pipe(take(1))
								.subscribe((res: any) => {
									this.usersService._listUserProfile = res.data;
								});
							this.router.navigateByUrl(this.courseInitial); // Go to initial course
							this.ma.event("submit", "login", "Enviar"); //Matomo event
						} else {
							this.alertService.error(
								this.translateService.instant("LOGIN.ERRORALVALIDARSE"),
								AlertService.AlertServiceContextValues.Login
							);
						}
					},
					(err) => {
						this.alertService.error(
							this.translateService.instant("LOGIN.ERRORALVALIDARSE"),
							AlertService.AlertServiceContextValues.Login
						);
					}
				);
		}
	}

	private _checkScreenMode() {
		let data: string = this.utils.getWindowOrientation();
		let dialogRef;

		this.translateService
			.get("ACEPTARCANCELAR.MODELANDSCAPE")
			.subscribe((res: string) => {
				if (data === "portrait")
					dialogRef = this.dialog.open(AcceptDialogComponent, {
						width: "350px",
						data: { message: res },
					});
			});
	}

	showCommentsModal() {
		this.nebularDialogService.open(ModalSendCommentsComponent, {
			dialogClass: MODAL_DIALOG_TYPES.W60,
		});
	}

	ngOnDestroy(): void {
		this.subs.unsubscribe();
	}

	closeModal() {
		this.router.navigateByUrl(this.loginRoute);
	}
}
