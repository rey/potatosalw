import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";
import { SharedModule } from "src/app/shared/shared.module";
import { QuillModule } from "ngx-quill";
import {
	NbButtonModule,
	NbCardModule,
	NbLayoutModule,
	NbProgressBarModule,
	NbActionsModule,
	NbInputModule,
	NbIconModule,
	NbSearchModule,
	NbToggleModule,
	NbSelectModule,
	NbDatepickerModule,
	NbTooltipModule,
} from "@nebular/theme";
import { NbEvaIconsModule } from "@nebular/eva-icons";

@NgModule({
	declarations: [LoginComponent],
	imports: [
		CommonModule,
		LoginRoutingModule,
		SharedModule,
		QuillModule,
		NbButtonModule,
		NbCardModule,
		NbLayoutModule,
		NbProgressBarModule,
		NbActionsModule,
		NbInputModule,
		NbIconModule,
		NbSearchModule,
		NbToggleModule,
		NbSelectModule,
		NbDatepickerModule,
		NbTooltipModule,
	],
})
export class LoginModule {}
