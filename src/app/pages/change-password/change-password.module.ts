import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangePasswordRoutingModule } from './change-password-routing.module';
import { ChangePasswordComponent } from './change-password.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NbButtonModule, NbCardModule, NbLayoutModule, NbProgressBarModule, NbActionsModule, NbInputModule, NbIconModule, NbSearchModule, NbToggleModule, NbSelectModule, NbDatepickerModule, NbTooltipModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
@NgModule({
    declarations: [ChangePasswordComponent],
    imports: [
        CommonModule,
        ChangePasswordRoutingModule,
        SharedModule,
				NbButtonModule,
				NbCardModule,
				NbLayoutModule,
				NbProgressBarModule,
				NbActionsModule,
				NbInputModule,
				NbIconModule,
				NbSearchModule,
				NbToggleModule,
				NbSelectModule,
				NbDatepickerModule,
				NbTooltipModule
    ]
})
export class ChangePasswordModule { }
