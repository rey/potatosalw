import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoValidationGuard } from 'src/app/core/guards/demo-validation.guard';
import { ChangePasswordComponent } from './change-password.component';

const routes: Routes = [
    {
        path: '',
        component: ChangePasswordComponent,
        canActivate: [DemoValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ChangePasswordRoutingModule { }
