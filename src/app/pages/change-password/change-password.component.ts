import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { finalize, take } from 'rxjs/operators';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { ChangePasswordModel } from 'src/app/core/models/users/change-password.model';
import { ChangePasswordService } from 'src/app/core/services/change-password/change-password.service';
import { AlertService, TitleService } from 'src/app/core/services/shared';
import { DialogService } from 'src/app/core/services/shared/dialog.service';
import { LocalStorage } from 'src/app/core/utils';
import { formsValidations } from 'src/app/core/utils/forms-validations';
import { ROUTES_NAME } from 'src/app/core/utils/routes-name';

declare function init_plugins();

function passwordMatcher(c: AbstractControl): { [key: string]: boolean } | null {
    const passControl = c.get('keyWord');
    const confirmControl = c.get('keyWordConfirm');

    if (passControl.pristine || confirmControl.pristine) {
        return null;
    }

    if (passControl.value === confirmControl.value) {
        return null;
    }
    return { 'match': true };
}

export class ParentGroupValidationStateMatcher implements ErrorStateMatcher {
    isErrorState(control: UntypedFormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const invalidCtrl = !!(control && control.invalid && (control.dirty || control.touched));
        const invalidParent = !!(control && (control.dirty || control.touched) && control.parent && control.parent.invalid && control.parent.dirty);

        return (invalidCtrl || invalidParent);
    }
}

@Component({
    selector: 'app-restore-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ChangePasswordComponent implements OnInit {
    public isLoading: boolean = false;

    private token: string;
    private idUser: number;

    public formulario: UntypedFormGroup;
    public loginRoute: string = ROUTES_NAME.LOGIN;
    matcher = new ParentGroupValidationStateMatcher();

    validationMessages = { keyword: [], keywordConfirm: [] };

    constructor(
        private router: Router,
        private formBuild: UntypedFormBuilder,
        private titleService: TitleService,
        public translateService: TranslateService,
        private alertService: AlertService,
        private localStorage: LocalStorage,
        private changePasswordService: ChangePasswordService,
        private activatedRoute: ActivatedRoute,
        private dialogService: DialogService
    ) {

    }

    ngOnInit(): void {
        init_plugins();
        this.getQueryParams();
        this.crearFormulario();
        this.traducirOpciones();
    }

    getQueryParams() {
        const queryParams = this.activatedRoute.snapshot.queryParams;

        if (!queryParams || !queryParams.idUser || !queryParams.token) {
            this.dialogService.openConfirmDialog(false, this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), this.translateService.instant('CAMBIARPASS.NOK'));
            this.router.navigate([this.loginRoute]);
        }
        this.idUser = queryParams.idUser;
        this.token = queryParams.token;
    }

    crearFormulario() {
        // default data and form groups
        this.formulario = this.formBuild.group({
            keyWord: ['', { validators: Validators.compose([Validators.required, Validators.pattern(formsValidations.PASSWORDPATTERN)])}],
            keyWordConfirm: ['', Validators.required],
        }, { validator: passwordMatcher });
    }

    get keywordControl(): UntypedFormControl {
        if (this.formulario) {
            return (this.formulario.get('keyWord') as UntypedFormControl);
        } else {
            return null;
        }
    }

    get keywordConfirmControl(): UntypedFormControl {
        if (this.formulario) {
            return (this.formulario.get('keyWordConfirm') as UntypedFormControl);
        } else {
            return null;
        }
    }

    traducirOpciones() {

        const lang:string = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
        this.translateService.use(lang);

        // Titulo pagina
        this.translateService.get('CAMBIARPASS.CAMBIARPASS').subscribe((res: string) => {
            this.titleService.setHTMLTitle(res);
        });

        this.translateService.get('VALIDACIONES.LACONTRASENAESOBLIGATORIA').subscribe((res: string) => {
            this.validationMessages.keyword.push({ type: 'required', message: res });
        });

        this.translateService.get('VALIDACIONES.CONFIRMKEYWORD').subscribe((res: string) => {
            this.validationMessages.keywordConfirm.push({ type: 'required', message: res });
        });

        this.translateService.get('VALIDACIONES.KEYWORDNOCOINCIDE').subscribe((res: string) => {
            this.validationMessages.keywordConfirm.push({ type: 'match', message: res });
        });

        this.translateService.get('VALIDACIONES.PASSWORDPATTERN').subscribe((res: string) => {
            this.validationMessages.keyword.push({ type: 'pattern', message: res });
        });
    }

    cambiarContrasena() {
        this.formulario.markAllAsTouched();

        if (this.formulario.valid) {

            const fv:any = this.formulario.value;

            const changePassword: ChangePasswordModel = new ChangePasswordModel(this.idUser, this.token, fv.keyWord);

            this.isLoading = true;

            this.changePasswordService.changePassword(changePassword).pipe(finalize(() => this.isLoading = false)).subscribe(
                res => {
                    if (res.error && res.error.code !== 0) {
                        this.dialogService.openConfirmDialog(false, this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), this.translateService.instant('CAMBIARPASS.NOK'));
                    } else {
                        const dialog = this.dialogService.openConfirmDialog(true, this.translateService.instant('CAMBIARPASS.CAMBIARPASS'), this.translateService.instant('CAMBIARPASS.OK'));


                        dialog.afterClosed().pipe(take(1)).subscribe(res =>
                            this.router.navigate([ROUTES_NAME.LOGIN])
                        );
                    }
                },
                err => {
                    this.alertService.error(this.translateService.instant('CAMBIARPASS.NOK'), AlertService.AlertServiceContextValues.Login);
                }
            );
        }
    }
}
