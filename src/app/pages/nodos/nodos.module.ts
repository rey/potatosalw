import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NodosRoutingModule } from "./nodos-routing.module";
import { NodosComponent } from "./nodos.component";
import { SharedModule } from "src/app/shared/shared.module";
import { QuillModule } from "ngx-quill";

import { AngularDraggableModule } from "angular2-draggable";
import { MatExpansionModule } from "@angular/material/expansion";
import { NgxCopilotModule } from "ngx-copilot";
import { NgxDocViewerModule } from "ngx-doc-viewer";

@NgModule({
    declarations: [NodosComponent],
    imports: [
        CommonModule,
        NodosRoutingModule,
        SharedModule,
        QuillModule,
        AngularDraggableModule,
        MatExpansionModule,
        NgxCopilotModule,
        NgxDocViewerModule,
    ],
    exports: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NodosModule {}
