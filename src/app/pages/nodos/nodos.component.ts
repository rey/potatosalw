import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgxCopilotService } from 'ngx-copilot';
import { Subscription } from 'rxjs';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { CanvasData, TutotrialDataLocal } from 'src/app/core/models/tuturial/tutorial.model';
import { GraphService } from 'src/app/core/services/graph/graph.service';
import { LoginService } from 'src/app/core/services/login';
import { MastersService } from 'src/app/core/services/masters';

// Services
import { TitleService } from 'src/app/core/services/shared';
import { TargetsService } from 'src/app/core/services/targets';
import { LocalStorage } from 'src/app/core/utils';
import { Profiles } from 'src/app/core/utils/profiles.enum';
import { environment } from 'src/environments/environment';
declare var $: any;

@Component({
    selector: 'app-nodos',
    templateUrl: './nodos.component.html',
    styleUrls: ['./nodos.component.scss']
})
export class NodosComponent implements OnInit, OnDestroy {
    public courseId: number;
    public graphId: number;
    public isGuest: boolean = false;
    tutorialGrafo: CanvasData = {
        crearActividad: false,
        nodoNodo: false,
        quizQuiz: false,
    }
    canEdit: boolean = false;
    background: string;
    graphTitle:string;
    urlTutorial:string = environment.tutorial

    private authorId: number;

    private subscriptions: Subscription[] = [];
    changedToEditor: boolean;
		public defaultViewType: number;

    constructor(private router: Router,
        private  copilot:  NgxCopilotService,
        private activatedRoute: ActivatedRoute,
        private translateService: TranslateService,
        private titleService: TitleService,
        private targetsService: TargetsService,
        private loginService: LoginService, private localStorageService: LocalStorage,
				private graphServ: GraphService,
        private masterService: MastersService) {


        this.activatedRoute.params.subscribe(params => {
            this.courseId = parseInt(params['idCurso']);
            this.graphId = parseInt(params['id']);




            //Si es un usuario invitado

            // De esta forma cada vez que cambia el parametro lo actualizo
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;

            // Y obtengo el nombre del curso para ponerlo en la cabecera
            this.targetsService.getTargetById(this.graphId)
                .subscribe((resp: any) => {
                    if (resp.data) {
                        //se hace que si el titulo es LanguageRounded, se acorta y se ponen ... suspensivos
                        //this.graphTitle = resp.data.tittle
                        const title = resp.data.tittle;
                        if (title.length > 24)
                            this.graphTitle = title.substring(0,22) + '...';
                        else
                            this.graphTitle = title;

                        this.background = resp.data.targetImage;
                        this.authorId = resp.data.idAuthor;
                        this.canEdit = this.getCanEdit();
                        //Si no es editor del grafo siempre le pondremos el
                        //if(!this.canEdit && this.loginService.getProfile() !== Profiles.Teacher) this.masterService.cambiarPerfil(Profiles.Student, false);

                        this.tutorialDisponible();
                        this.changedToEditor = this.cambiarEditor();
                        this.translateService.get('NODOS.GRAFO').subscribe((res: string) => {
                            this.titleService.barTitle = `${res}: ${resp.data.tittle}`;
                            this.titleService.setHTMLTitle(resp.data.tittle);
                        });
                    }
                });
        });

        this.isGuest = localStorage.getItem('INVITADO') === 'true' ? true : false;

        this.subscriptions.push(this.loginService.currentProfile$.subscribe(profile =>
            this.canEdit = this.getCanEdit()
        ));
    }
    tutorialDisponible() {
        if(this.loginService.comprobarTutorial() && this.canEdit){
            let canvasGrafoTutorial = JSON.parse(this.localStorageService.getItem(LOCALSTORAGESTRINGS.TUTORIALDATA)) as TutotrialDataLocal;
            this.tutorialGrafo = canvasGrafoTutorial.canvasGrafo;
            this.abrirTutorial();
        }
        else {
            this.tutorialGrafo.crearActividad = true;
            this.tutorialGrafo.nodoNodo = true;
            this.tutorialGrafo.quizQuiz = true;
        }
    }
    abrirTutorial() {
    if (!this.tutorialGrafo.crearActividad) {

            let reload = localStorage.getItem('RELOAD');
            if(reload === undefined || reload === null){
                this.localStorageService.setItem('RELOAD', true );
                window.location.reload();
            }

            setTimeout(() => {
                this.initPosition(4);
            }, 1000);
        }
    }

    helpLienzoVacio(){
        let canvasGrafoTutorial = JSON.parse(this.localStorageService.getItem(LOCALSTORAGESTRINGS.TUTORIALDATA)) as TutotrialDataLocal;
        canvasGrafoTutorial.canvasGrafo.crearActividad = true;
        this.copilot.removeWrapper();
        this.loginService.updateDataLocalTutorial(canvasGrafoTutorial);

    }
    helpLienzoNodoNodo(){
        let canvasGrafoTutorial = JSON.parse(this.localStorageService.getItem(LOCALSTORAGESTRINGS.TUTORIALDATA)) as TutotrialDataLocal;
        canvasGrafoTutorial.canvasGrafo.nodoNodo = true;
        this.copilot.removeWrapper();
        this.loginService.updateDataLocalTutorial(canvasGrafoTutorial);
    }
    helpLienzoQuizQuiz(){
        let canvasGrafoTutorial = JSON.parse(this.localStorageService.getItem(LOCALSTORAGESTRINGS.TUTORIALDATA)) as TutotrialDataLocal;
        canvasGrafoTutorial.canvasGrafo.quizQuiz = true;
        this.copilot.removeWrapper();
        this.loginService.updateDataLocalTutorial(canvasGrafoTutorial);
    }
    ngOnInit() {
			this.graphServ.getGraphInfo(this.graphId).subscribe(res => {
				const viewType =  res.data.viewType
				this.graphServ.changedefaultTypeView(res.data.viewType);

			})
			this.graphServ.defaultTypeView.subscribe((data: number) => { this.defaultViewType = data; });
        // let style = $("ngx-wrapper-overview >  .copilot-view");
        // style = style.style.maxWidth = "600px";
        // console.log(style)
    }

    initPosition = (o: any) => {
        this.copilot.checkInit(o)
    };

    /*Next Step*/
    nextStep = (stepNumber:any) =>  this.copilot.next(stepNumber);

    done = () => {
        this.copilot.removeWrapper()
    }
    sigAyuda(idTemplate: string){

    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }

    private getCanEdit(): boolean {
        //return this.loginService.esAutor() && this.loginService.getUser().idUser === this.authorId;
        return this.loginService.esAutor();
    }

    private cambiarEditor(): boolean {
        return this.loginService.getUser().idUser === this.authorId;
    }
}
