import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NodosComponent } from './nodos.component';

const routes: Routes = [
    {
        path: ':idCurso/graph/:id',
        component: NodosComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NodosRoutingModule { }
