import { ValidateSmsCodeRoutingModule } from './validate-sms-code-routing.module';
import { ValidateSmsCodeComponent } from './validate-sms-code.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [ValidateSmsCodeComponent],
    imports: [
        CommonModule,
        ValidateSmsCodeRoutingModule,
        SharedModule
    ]
})
export class ValidateSmsCodeModule { }
