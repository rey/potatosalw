import { ValidateSmsCodeComponent } from './validate-sms-code.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoValidationGuard } from 'src/app/core/guards/demo-validation.guard';

const routes: Routes = [
    {
        path: '',
        component: ValidateSmsCodeComponent,
        canActivate: [DemoValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ValidateSmsCodeRoutingModule { }
