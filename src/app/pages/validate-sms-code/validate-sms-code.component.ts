import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

// Utils
import { LocalStorage } from 'src/app/core/utils';

// Services
import { TranslateService } from '@ngx-translate/core';
import { TitleService } from 'src/app/core/services/shared';
import { ROUTES_NAME } from 'src/app/core/utils/routes-name';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';

declare function init_plugins();

@Component({
    selector: 'app-validate-sms-code',
    styleUrls: ['./validate-sms-code.component.scss'],
    templateUrl: './validate-sms-code.component.html'
})
export class ValidateSmsCodeComponent implements OnInit {

    public form: UntypedFormGroup;
    public loginRoute: string = ROUTES_NAME.LOGIN;
    private prefix:string
    private phone:string

    // tslint:disable-next-line: member-ordering
    validationMessages = {
        code : []
    };



	constructor(private router: Router,
		private formBuild: UntypedFormBuilder,
		private titleService: TitleService,
		public translateService: TranslateService,
		private activatedRoute: ActivatedRoute,
		private localStorage: LocalStorage,
		private toaster: ToasterService
	) {

	}

    ngOnInit(): void {
        init_plugins()
        this.getQueryParams();
        this.crearFormulario();
        this.traducirOpciones();
    }

    getQueryParams() {
        const queryParams = this.activatedRoute.snapshot.queryParams;

        if (!queryParams || !queryParams.phone || !queryParams.prefix) {
            this.toaster.error(this.translateService.instant('VALIDAR.NOK'));
            this.router.navigate([this.loginRoute]);
        }

        this.prefix = queryParams.prefix;
        this.phone = queryParams.phone;
    }

	crearFormulario() {
		this.form = this.formBuild.group({
			code: ['', Validators.required]
		});
	}

    traducirOpciones() {

        // Recupero el lenguaje
        const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
        this.translateService.use(lang);

        // Titulo pagina
        this.translateService.get('VALIDATESMSCODE.TITLE').subscribe((res: string) => {
            this.titleService.setHTMLTitle(res);
        });



    }

    validarClave() {

        // if (this.formularioClave.invalid) {
        //     return;
        // }

        // const campos = this.formularioClave.value;

        // this.loading = true;
        // this.loading = false;
    }
}
