import { CenterService } from './../../core/services/center/center.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

// Utils
import { LocalStorage } from 'src/app/core/utils';

// Services
import { TranslateService } from '@ngx-translate/core';
import { TitleService } from 'src/app/core/services/shared';


// Validators
import { ROUTES_NAME } from 'src/app/core/utils/routes-name';
import { ValidateAccountService } from 'src/app/core/services/validate-account/validate-account.service';
import { finalize } from 'rxjs/operators';
import { LoginService } from 'src/app/core/services/login';
import { Subscription } from 'rxjs';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';

declare function init_plugins();

@Component({
    selector: 'app-validate-account',
    styleUrls: ['../login/login.component.scss'],
    templateUrl: './validate-account.component.html'
})
export class ValidateAccountComponent implements OnInit, OnDestroy {
    private uuid: string;
    private idUser: number;
    private courseInitial:string
    private subs = new Subscription

    public loginRoute: string = ROUTES_NAME.LOGIN;

    public loading: boolean = false;
    public errorCode: number;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private titleService: TitleService,
        public translateService: TranslateService,
        private localStorage: LocalStorage,
        private validateAccountService: ValidateAccountService,
        private loginService: LoginService,
        private centerService:CenterService,
				private toaster: ToasterService
    ) {
    }

    ngOnInit(): void {
        init_plugins();

        this.loginService.logout(false);
        this.centerService.getCenterConfigByDomain()

        this.traducirOpciones();
        this.getQueryParams();

        this.subs = this.centerService.centerConfig.subscribe(data => {
            this.courseInitial  = `course/${data.idCourse}/graph/${data.idTarget}`
            this.validateAccount();
        }, err => {
            console.error(err)
        })
    }

    ngOnDestroy(): void {
        this.subs.unsubscribe()
    }

    getQueryParams() {
        const queryParams = this.activatedRoute.snapshot.queryParams;

        if (!queryParams || !queryParams.idUser || !queryParams.uuid) {
            this.toaster.error(this.translateService.instant('VALIDAR.NOK'));
            this.router.navigate([this.loginRoute]);
        }

        this.idUser = queryParams.idUser;
        this.uuid = queryParams.uuid;
    }

    validateAccount() {
        this.loading = true;
        this.validateAccountService.validateAccount(this.idUser, this.uuid).pipe(
            finalize(() => {
                this.loading = false;
            }
            )).subscribe(
                res => {
                    if (res.error && res.error.code !== 0) {
                        this.errorCode = res.error.code;
                        this.toaster.error(this.translateService.instant('VALIDAR.NOK'));
                        setTimeout(() => {
                            this.router.navigate([this.loginRoute]);
                        }, 3000);
                    } else {
                        this.errorCode = 0;
                        this.toaster.success(this.translateService.instant('VALIDAR.OK'));
                        this.loginService.setUserLocalStorage(res.data);

                        setTimeout(() => {
                            this.router.navigateByUrl(this.courseInitial); // Navigate to DEFAULT GRAPH
                        }, 3000);
                    }
                },
                err => {
                    this.errorCode = err.error.code;
                    this.toaster.error(this.translateService.instant('VALIDAR.NOK'));
                    setTimeout(() => {
                        this.router.navigate([this.loginRoute]);
                    }, 3000);
                }
            );
    }

    traducirOpciones() {
        // Recupero el lenguaje
        const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
        this.translateService.use(lang);

        // Titulo pagina
        this.translateService.get('VALIDAR.VALIDARDATOS').subscribe((res: string) => {
            this.titleService.setHTMLTitle(res);
        });
    }
}
