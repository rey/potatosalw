import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoValidationGuard } from 'src/app/core/guards/demo-validation.guard';
import { ValidateAccountComponent } from './validate-account.component';

const routes: Routes = [
    {
        path: '',
        component: ValidateAccountComponent,
        canActivate: [DemoValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ValidateAccountRoutingModule { }
