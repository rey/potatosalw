import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ValidateAccountRoutingModule } from './validate-account-routing.module';
import { ValidateAccountComponent } from './validate-account.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [
        ValidateAccountComponent
    ],
    imports: [
        CommonModule,
        ValidateAccountRoutingModule,
        SharedModule
    ]
})
export class ValidateAccountModule { }
