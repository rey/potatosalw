import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import {
	NbButtonModule,
	NbCardModule,
	NbLayoutModule,
	NbProgressBarModule,
	NbActionsModule,
	NbInputModule,
	NbIconModule,
	NbSearchModule,
	NbToggleModule,
	NbSelectModule,
	NbDatepickerModule,
	NbTooltipModule,
} from "@nebular/theme";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
	declarations: [HomeComponent],
	imports: [
		CommonModule,
		HomeRoutingModule,
		SharedModule,
		NbButtonModule,
		NbCardModule,
		NbLayoutModule,
		NbProgressBarModule,
		NbActionsModule,
		NbInputModule,
		NbIconModule,
		NbSearchModule,
		NbToggleModule,
		NbSelectModule,
		NbDatepickerModule,
		NbTooltipModule,
	],
})
export class HomeModule {}
