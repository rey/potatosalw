import { environment } from './../../../environments/environment';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
// Utils
import { LocalStorage } from 'src/app/core/utils';

// Services
import { TranslateService } from '@ngx-translate/core';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { DialogService } from 'src/app/core/services/shared/dialog.service';
import { Router } from '@angular/router';

declare function init_plugins();

const LOGO_URL = 'https://salware.com/images/logos/';

@Component({
    selector: 'app-home',
    styleUrls: ['./home.component.scss'],
    templateUrl: './home.component.html',
		animations: [
			trigger('EnterLeave', [
				state('flyIn', style({ transform: 'translateX(0)' })),
				transition(':enter', [
					style({ transform: 'translateX(-100%)' }),
					animate('0.5s 400ms ease-in')
				]),
				transition(':leave', [
					animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
				])
			]),
			trigger('fadeInOut', [
				state(
					'void',
					style({
						opacity: 0
					})
				),
				transition('void <=> *', animate(3000))
			])
		],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

	logoHome: string
	domain: string
	state = 'none';
    public constructor(
        public translateService: TranslateService,
        private localStorage: LocalStorage,
        private dialogService: DialogService,
				private router: Router
        )
        {
            const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
            this.translateService.use(lang);
        }

    ngOnInit() {
			this.state = ':enter';
			this.state = '*';
			this.logoHome = `${LOGO_URL}${environment.logoHome}`
			this.domain = `${environment.domain}`

			const archivoPDF = document.getElementById("logo").setAttribute("src", this.logoHome);
		}

		redirectRegister() {
			this.router.navigate(['auth/sign-up']);
		}

    showDialogPolicy(event: MouseEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.dialogService.openDialogPolicy();
    }

    showDialogWe(event: MouseEvent) {
        event.preventDefault();
        this.dialogService.openDialogWe();
    }

    showDialogCookie(event: MouseEvent) {
        event.preventDefault();
        this.dialogService.openDialogCookie();
    }

    showDialogConcatc(event: MouseEvent) {
        event.preventDefault();
        this.dialogService.openDialogContact();
    }
}
