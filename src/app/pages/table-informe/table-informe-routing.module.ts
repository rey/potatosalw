import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DemoValidationGuard } from "src/app/core/guards/demo-validation.guard";
import { TableInformeComponent } from "./table-informe.component";

const routes: Routes = [
	{
		path: "",
		component: TableInformeComponent,
		// canActivate: [DemoValidationGuard],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TableInformeRoutingModule {}
