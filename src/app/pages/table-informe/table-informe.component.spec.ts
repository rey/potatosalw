import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TableInformeComponent } from './table-informe.component';

describe('TableInformeComponent', () => {
  let component: TableInformeComponent;
  let fixture: ComponentFixture<TableInformeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TableInformeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableInformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
