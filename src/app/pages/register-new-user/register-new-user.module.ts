import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterNewUserComponent } from './register-new-user.component';
import { RegisterRoutingModule } from './register-new-user-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [RegisterNewUserComponent],
  imports: [
    CommonModule,
    SharedModule,
    RegisterRoutingModule
  ]
})
export class RegisterNewUserModule { }
