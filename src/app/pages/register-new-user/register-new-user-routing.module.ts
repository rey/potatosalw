import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoValidationGuard } from 'src/app/core/guards/demo-validation.guard';
import { RegisterNewUserComponent } from './register-new-user.component';

const routes: Routes = [
    {
        path: '',
        component: RegisterNewUserComponent,
        canActivate: [DemoValidationGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RegisterRoutingModule { }
