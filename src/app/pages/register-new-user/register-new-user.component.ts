import { Component, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
// import { AuthService, GoogleLoginProvider } from 'angularx-social-login';
import { finalize } from 'rxjs/operators';
import { RegisterNewUser } from 'src/app/core/models/users/register.model';
import { RegisterService } from 'src/app/core/services/register/register.service';
import { TitleService, AlertService } from 'src/app/core/services/shared';
import { DialogService } from 'src/app/core/services/shared/dialog.service';
import { formsValidations } from 'src/app/core/utils/forms-validations';
import { ROUTES_NAME } from 'src/app/core/utils/routes-name';
import { RegisterNewUserValidationService } from './register-new-user-validation.service';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';

declare function init_plugins();

function emailMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  const emailControl = c.get('email');
  const confirmControl = c.get('emailConfirm');

  if (emailControl.pristine) {
      return null;
  }
  return { 'match': true };
}

function passwordMatcher(c: AbstractControl): { [key: string]: boolean } | null {
  const passControl = c.get('keyWord');
  const confirmControl = c.get('keyWordConfirm');

  if (passControl.pristine) {
      return null;
  }

  return { 'match': true };
}

export class ParentGroupValidationStateMatcher implements ErrorStateMatcher {
  isErrorState(control: UntypedFormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const invalidCtrl = !!(control && control.invalid && (control.dirty || control.touched));
      const invalidParent = !!(control && (control.dirty || control.touched) && control.parent && control.parent.invalid && control.parent.dirty);

      return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-register-new-user',
  templateUrl: './register-new-user.component.html',
  styleUrls: ['./register-new-user.component.scss']
})
export class RegisterNewUserComponent implements OnInit {
  registerNewUserRoute = ROUTES_NAME.LOGIN;

    public formulario: UntypedFormGroup;
    public existeTelefono = false;
    public loading = false;
    public hidePassword = true;
    validationMessages = {
      firstName: [],
      surname: [],
      email: [],
      emailConfirm: [],
      mobile: [],
      birthdate: [],
      gender: [],
      keyword: [],
      keywordConfirm: [],
      terms: []
  };

  constructor(
    private router: Router,
    private formBuild: UntypedFormBuilder,
    private titleService: TitleService,
    public translateService: TranslateService,
    private alertService: AlertService,
    private registerService: RegisterService,
    private registerValidationService: RegisterNewUserValidationService,
    private dialogService: DialogService,
		private toaster: ToasterService
  ) { }

  ngOnInit() {
    this.crearFormulario();
    init_plugins();
  }


  private passMinLength: number = formsValidations.PASSWORD_MIN_LENGTH;
  private emailMaxLength: number = formsValidations.EMAIL_MAX_LENGTH;
  private passMaxLength: number = formsValidations.PASS_MAX_LENGTH;
  private emailPattern: string = formsValidations.EMAIL_PATTERN;


  crearFormulario() {
    this.formulario = this.formBuild.group({
        emailGroup: this.formBuild.group({
            email: ['', {
                validators: [Validators.required, Validators.maxLength(this.emailMaxLength), Validators.pattern(this.emailPattern)],
                asyncValidators: [this.registerValidationService.emailValidator()],
                updateOn: 'blur'
            }],
        }, { validator: emailMatcher }),
        passwordGroup: this.formBuild.group({
            keyWord: ['', { validators: [Validators.required, Validators.minLength(this.passMinLength), Validators.maxLength(this.passMaxLength)] }],
        }, { validator: passwordMatcher }),
        terms: [false, Validators.requiredTrue]


    });
}

get emailGroup(): UntypedFormGroup {
  if (this.formulario) {
      return (this.formulario.get('emailGroup') as UntypedFormGroup);
  } else {
      return null;
  }
}

get emailControl(): UntypedFormControl {
  if (this.emailGroup) {
      return (this.emailGroup.get('email') as UntypedFormControl);
  } else {
      return null;
  }
}


get passwordGroup(): UntypedFormGroup {
  if (this.formulario) {
      return (this.formulario.get('passwordGroup') as UntypedFormGroup);
  } else {
      return null;
  }
}

get keywordControl(): UntypedFormControl {
  if (this.passwordGroup) {
      return (this.passwordGroup.get('keyWord') as UntypedFormControl);
  } else {
      return null;
  }
}

get termsControl(): UntypedFormControl {
  if (this.formulario) {
      return (this.formulario.get('terms') as UntypedFormControl);
  } else {
      return null;
  }
}

  crearFormulario_() {
    // default data and form groups
    this.formulario = this.formBuild.group({
        nick: ['', Validators.required],
        keyWord: ['', Validators.required],
        prefix : ['']
    });
}
traducirOpciones() {


  this.translateService.get('LOGIN.ACCEDER').subscribe((res: string) => {
      this.titleService.setHTMLTitle(res);
  });

  this.translateService.get('VALIDACIONES.EMAILEXISTE').subscribe((res: string) => {
    this.validationMessages.email.push({ type: 'emailExists', message: res });
});

this.translateService.get('VALIDACIONES.ELEMAILESOBLIGATORIO').subscribe((res: string) => {
    this.validationMessages.email.push({ type: 'required', message: res });
});

this.translateService.get('VALIDACIONES.ELEMAILNOESVALIDO').subscribe((res: string) => {
    this.validationMessages.email.push({ type: 'pattern', message: res });
});
this.translateService.get('VALIDACIONES.ELEMAILNOESVALIDO').subscribe((res: string) => {
  this.validationMessages.emailConfirm.push({ type: 'pattern', message: res });
});

this.translateService.get('VALIDACIONES.LACONTRASENAESOBLIGATORIA').subscribe((res: string) => {
  this.validationMessages.keyword.push({ type: 'required', message: res });
});
this.translateService.get('VALIDACIONES.MUSTACCEPTTERMS').subscribe((res: string) => {
  this.validationMessages.terms.push({ type: 'required', message: res });
});


}

public resetForm() {
  this.formulario.value.nick = '';
  this.formulario.value.keyWord = '';
}

  register() {
      this.formulario.markAllAsTouched();
      if (!this.formulario.valid) {

          this.loading = true;
          const fv = this.formulario.value;

          const userToRegister = new RegisterNewUser(fv.emailGroup.email, fv.passwordGroup.keyWord);

          this.registerService.registerNewUser(userToRegister).pipe(finalize(() => this.loading = false)).subscribe(
              res => {
                  this.toaster.success(this.translateService.instant('REGISTRO.PENDINGVALIDATION'));
                  this.router.navigate([ROUTES_NAME.LOGIN]);
              },
              err => {
                  this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.RegistrarEmpleado);
                  this.toaster.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'));
              }
          );
      }
  }


  googleSignin() {
    //this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
}

showPrivacyDialog(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    this.dialogService.openPrivacyDialog();
}

showConditionsDialog(event: MouseEvent) {
    event.preventDefault();
    this.dialogService.openConditionsDialog();
}


}
