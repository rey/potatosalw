import { Injectable } from '@angular/core';
import { AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RegisterService } from 'src/app/core/services/register/register.service';

@Injectable({
    providedIn: 'root'
})
export class RegisterNewUserValidationService {

    constructor(private registerService: RegisterService) { }

    emailValidator(): AsyncValidatorFn {
        return (control: AbstractControl): Observable<ValidationErrors | null> => {
            return this.registerService.checkEmailExists(control.value).pipe(map(res => res ? { emailExists: true } : null));
        };
    }
}
