import { User } from '../../../core/models/users/user.models';
import { CenterModel } from '../../../core/models/masters/center.model';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';

// Utils
import { LocalStorage } from 'src/app/core/utils';

// Models
import { UserProfileValidationModel, UserCenterModel, UserSettingsProfileModel } from 'src/app/core/models/users';

// Services
import { TranslateService } from '@ngx-translate/core';
import { AlertService, TitleService } from 'src/app/core/services/shared';
import { LoginService } from 'src/app/core/services/login';
import { UsersService } from 'src/app/core/services/users';
import { MastersService } from 'src/app/core/services/masters';

// Mapper
import { UsersRegisterMapperService, UsersCentersMapperService, UsersProfileValidationMapperService,
         UsersSettingsProfileMapperService } from 'src/app/core/services/mapper';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GetDataService } from 'src/app/core/services/get-data/get-data.service';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data-centro.component.html',
  styleUrls: ['../user-data-general/user-data-general.component.scss']
})
export class UserDataCentroComponent implements OnInit {

  public formularioDatos: UntypedFormGroup;

  userData: User;

  usersCenterData: UserCenterModel;

  usersProfileValidationData: UserProfileValidationModel[];

  usersSettingsProfileData: UserSettingsProfileModel[];

  centers: CenterModel[] = [];

  public loading = false;

  constructor(private formBuild: UntypedFormBuilder,
              public translateService: TranslateService,
              private titleService: TitleService,
              private alertService: AlertService,
              private localStorage: LocalStorage,
              public loginService: LoginService,
              private usersService: UsersService,
              private mastersService: GetDataService,
              private usersRegisterMapperService: UsersRegisterMapperService,
              private usersCentersMapperService: UsersCentersMapperService,
              private usersSettingsProfileMapperService: UsersSettingsProfileMapperService,
              private usersProfileValidationMapperService: UsersProfileValidationMapperService,
              private activeModal: NgbActiveModal
              ) {

  }

  ngOnInit(): void {

    // Obtengo los maestros
    this.obtenerDatosMaestros();

    // Lanzo las traducciones
    this.traducirOpciones();

    // Cargo los formularios
    this.crearFormularios();

    // Y obtengo la informacion del usuario
    this.obtenerDatosUsuario();
  }

  private obtenerDatosMaestros() {
      this.centers = this.mastersService.appCenters
  }

  private traducirOpciones() {
    // Recupero el lenguaje
    const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
    this.translateService.use(lang);

    // Titulo pagina
    this.translateService.get('USERDATA.CONFIGURACIONCENTRO').subscribe((res: string)  => {
      this.titleService.setHTMLTitle(res);
      this.titleService.setBarTitle(res);
    });
  }

  // Mostrar Aviso
  public mostrarMensajeAviso() {

    // Para poder hacer las comparaciones
    //this.userData.usersProfileValidation = this.usersProfileValidationData;

    const resp = this.usersService.checkIfUserDataAreRegistered(this.userData, 'centro');

    if (resp.result !== true) {
        this.alertService.info(this.translateService.instant(resp.mensaje),
                               AlertService.AlertServiceContextValues.UserData);
    } else {
      this.alertService.clear();
    }
  }
  // Fin mostrar aviso

  private crearFormularios() {

    // default data and form groups
    this.formularioDatos = this.formBuild.group({
      idUser: [this.loginService.getUser().idUser],
      // centro
      idUserCenter: [0],
      idCenter: ['']
    });
  }

  private cargarDatosFormulario() {

    if (this.usersCenterData != null) {
      this.formularioDatos.controls.idUser.setValue(this.userData.idUser);
      this.formularioDatos.controls.idUserCenter.setValue(this.usersCenterData.idUserCenter);
      this.formularioDatos.controls.idCenter.setValue(this.usersCenterData.idCenter);
    }
  }

  private registrarUserSettingProfile() {

    // Y si era la primera vez que grababa algo siendo estudiante lo registro en users_settings_profile
    if (this.usersSettingsProfileData.length === 0 ||
        this.usersSettingsProfileData.filter(x => x.profile === 'centro').length === 0) {

      let userSettingProfile = new UserSettingsProfileModel();

      userSettingProfile.idUserSettingProfile = 0;
      userSettingProfile.idUser = this.userData.idUser;
      userSettingProfile.profile = 'centro';
    }
  }

  private obtenerDatosUsuario() {

    this.loading = true;

    this.usersService.getUserByIdUser(this.loginService.getUser().idUser)
                      .subscribe( resp => {
                        if (resp) {

                          // tslint:disable-next-line: max-line-length
                          //this.usersService.addUserLog(this.loginService.getUser().idUser, `Crear formulario datos generales - Datos de usuario centro`, 'OK');

                          // Mapeo los datos del usuario
                          // this.userData = this.usersRegisterMapperService.transform(resp.data.users.nodes[0]);

                          // // Mapeo los datos del usuario-centro
                          // if (resp.data.users.nodes[0].UsersCenters.length > 0) {
                          //   this.usersCenterData = this.usersCentersMapperService.transform(resp.data.users.nodes[0].UsersCenters[0]);
                          // }

                          // // Los de las validaciones del usuario (si las tiene)
                          // if (resp.data.users.nodes[0].usersProfileValidation.length > 0) {
                          //   // tslint:disable-next-line: max-line-length
                          //   this.usersProfileValidationData = this.usersProfileValidationMapperService.transform(resp.data.users.nodes[0].usersProfileValidation);
                          // }

                          // // Cargo los user settings profile para ver que menus van en gris
                          // this.usersSettingsProfileData = resp.data.users.nodes[0].usersSettingsProfiles;

                          if (this.usersSettingsProfileData === undefined) {
                            this.usersSettingsProfileData = [];
                          }

                          // Cargo los datos
                          this.cargarDatosFormulario();

                          // Y el mensaje de aviso en funcion de si ha sido validado o no.
                          this.mostrarMensajeAviso();

                          this.loading = false;
                        } else {
                          // tslint:disable-next-line: max-line-length
                          this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.UserData);

                          // tslint:disable-next-line: max-line-length
                          //this.usersService.addUserLog(this.loginService.getUser().idUser, `Crear formulario datos generales - Datos de usuario centro`, 'ERROR');
                        }
    }, (error) => {
      this.loading = false;

      // tslint:disable-next-line: max-line-length
      this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.UserData);

      // tslint:disable-next-line: max-line-length
      //this.usersService.addUserLog(this.loginService.getUser().idUser, `Crear formulario datos generales - Datos de usuario centro - ERROR: ${error}`, 'ERROR');
    });
  }

  public grabarDatos() {

    if (this.formularioDatos.invalid) {
      return;
    }

    // Recupero los datos del formulario
    this.loading = true;

    const campos = this.formularioDatos.value;

    // Y restauramos los datos del usuario en base al formulario
    // Los campos que no van a ser modificados iran a undefined y no se actualizaran
    this.userData = this.usersRegisterMapperService.transform(campos);

    // El telefono no se actualiza
    this.userData.mobile = null;

    this.usersService.updateUser(this.userData)
                      .subscribe((resp: any) => {

                        // tslint:disable-next-line: max-line-length
                        //this.usersService.addUserLog(this.loginService.getUser().idUser, `Grabar datos generales - Datos de usuario centro`, 'OK');

                        // Grabo los datos del centro al que corresponde
                        this.usersCenterData = this.usersCentersMapperService.transform(campos);
                        this.usersCenterData.idUser = this.userData.idUser;
                    }, (error) => {
                      console.log(error);

                      this.loading = false;

                      // tslint:disable-next-line: max-line-length
                      this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.UserData);

                      // tslint:disable-next-line: max-line-length
                      //this.usersService.addUserLog(this.loginService.getUser().idUser, `Grabar datos generales - Datos de usuario centro - ERROR: ${error}`, 'ERROR');
                    });
  }

  closeModal(sendData) {
    this.activeModal.close(sendData);
  }
}
