import { User } from '../../../core/models/users/user.models';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

// Utils
import { LocalStorage } from 'src/app/core/utils';

// Models
import { UserRegisterModel, UserProfileValidationModel, UserEmployeerModel, UserSettingsProfileModel } from 'src/app/core/models/users';

// Services
import { TranslateService } from '@ngx-translate/core';
import { AlertService, TitleService } from 'src/app/core/services/shared';
import { LoginService } from 'src/app/core/services/login';
import { UsersService } from 'src/app/core/services/users';

// Mapper
import { UsersAuthorsMapperService, UsersRegisterMapperService, UsersParentsMapperService, UsersCentersMapperService,
         UsersEmployeersMapperService, UsersStudentsParentsMapperService, UsersProfileValidationMapperService,
         CentersMapperService } from 'src/app/core/services/mapper';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data-empleador.component.html',
  styleUrls: ['..//user-data-general/user-data-general.component.scss']
})
export class UserDataEmpleadorComponent implements OnInit {

  public formularioDatos: UntypedFormGroup;

  userData: User;

  usersEmployeerData: UserEmployeerModel;

  usersProfileValidationData: UserProfileValidationModel[];

  usersSettingsProfileData: UserSettingsProfileModel[];

  enterprises: any[] = [];

  public loading = false;

  constructor(private formBuild: UntypedFormBuilder,
              public translateService: TranslateService,
              private titleService: TitleService,
              private alertService: AlertService,
              private localStorage: LocalStorage,
              public loginService: LoginService,
              private usersService: UsersService,
              private usersRegisterMapperService: UsersRegisterMapperService,
              private usersEmployeersMapperService: UsersEmployeersMapperService,
              private activeModal: NgbActiveModal
              ) {

  }

  ngOnInit(): void {

    // Obtengo los maestros
    this.obtenerDatosMaestros();

    // Lanzo las traducciones
    this.traducirOpciones();

    // Cargo los formularios
    this.crearFormularios();

    // Y obtengo la informacion del usuario
    this.obtenerDatosUsuario();
  }

  private obtenerDatosMaestros() {

    // this.mastersService.DEPRECATED_getAllEnterprises()
    //                     .subscribe(({ data }) => {
    //                       this.enterprises = data.enterprise.nodes;
    //                      });
  }

  private traducirOpciones() {
    // Recupero el lenguaje
    const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
    this.translateService.use(lang);

    // Titulo pagina
    this.translateService.get('USERDATA.CONFIGURACIONEMPLEADOR').subscribe((res: string)  => {
      this.titleService.setHTMLTitle(res);
      this.titleService.setBarTitle(res);
    });
  }

  // Mostrar Aviso
  public mostrarMensajeAviso() {

    // Para poder hacer las comparaciones
    //this.userData.usersProfileValidation = this.usersProfileValidationData;

    const resp = this.usersService.checkIfUserDataAreRegistered(this.userData, 'empleador');

    if (resp.result !== true) {
        this.alertService.info(this.translateService.instant(resp.mensaje),
                               AlertService.AlertServiceContextValues.UserData);
    } else {
      this.alertService.clear();
    }
  }
  // Fin mostrar aviso

  // Datos Generales
  private crearFormularios() {

    // default data and form groups
    this.formularioDatos = this.formBuild.group({
      idUser: [this.loginService.getUser().idUser],
      idUserEnterprise: [0],
      idEnterprise: [''],
      creationDate: [],
      creationDateString: []
    });
  }

  private cargarDatosFormulario() {

    if (this.usersEmployeerData != null) {
      this.formularioDatos.controls.idUser.setValue(this.userData.idUser);
      this.formularioDatos.controls.idUserEnterprise.setValue(this.usersEmployeerData.idUserEnterprise);
      this.formularioDatos.controls.idEnterprise.setValue(this.usersEmployeerData.idEnterprise);
    }
  }

  private registrarUserSettingProfile() {

    // Y si era la primera vez que grababa algo siendo estudiante lo registro en users_settings_profile
    if (this.usersSettingsProfileData.length === 0 ||
        this.usersSettingsProfileData.filter(x => x.profile === 'empleador').length === 0) {

      let userSettingProfile = new UserSettingsProfileModel();

      userSettingProfile.idUserSettingProfile = 0;
      userSettingProfile.idUser = this.userData.idUser;
      userSettingProfile.profile = 'empleador';
    }
  }

  private obtenerDatosUsuario() {

    this.loading = true;

    this.usersService.getUserByIdUser(this.loginService.getUser().idUser)
                      .subscribe( resp => {
                        if (resp) {

                          // tslint:disable-next-line: max-line-length
                          //this.usersService.addUserLog(this.loginService.getUser().idUser, `Crear formulario datos generales f- Datos de usuario empleador`, 'OK');

                          // Mapeo los datos del usuario
                          // this.userData = this.usersRegisterMapperService.transform(resp.data.users.nodes[0]);

                          // // Mapeo los datos del usuario-empleador
                          // if (resp.data.users.nodes[0].usersEnterprises.length > 0) {
                          //   // tslint:disable-next-line: max-line-length
                          //   this.usersEmployeerData = this.usersEmployeersMapperService.transform(resp.data.users.nodes[0].usersEnterprises[0]);
                          // }

                          // // Los de las validaciones del usuario (si las tiene)
                          // if (resp.data.users.nodes[0].usersProfileValidation.length > 0) {
                          //   // tslint:disable-next-line: max-line-length
                          //   this.usersProfileValidationData = this.usersProfileValidationMapperService.transform(resp.data.users.nodes[0].usersProfileValidation);
                          // }

                          // // Cargo los user settings profile para ver que menus van en gris
                          // this.usersSettingsProfileData = resp.data.users.nodes[0].usersSettingsProfiles;

                          if (this.usersSettingsProfileData === undefined) {
                            this.usersSettingsProfileData = [];
                          }

                          // Cargo los datos
                          this.cargarDatosFormulario();

                          // Y el mensaje de aviso en funcion de si ha sido validado o no.
                          this.mostrarMensajeAviso();

                          this.loading = false;
                        } else {
                          // tslint:disable-next-line: max-line-length
                          this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.UserData);

                          // tslint:disable-next-line: max-line-length
                          //this.usersService.addUserLog(this.loginService.getUser().idUser, `Crear formulario datos generales f- Datos de usuario empleador`, 'ERROR');
                        }
    }, (error) => {
      this.loading = false;

      // tslint:disable-next-line: max-line-length
      this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.UserData);

      // tslint:disable-next-line: max-line-length
      //this.usersService.addUserLog(this.loginService.getUser().idUser, `Crear formulario datos generales f- Datos de usuario empleador - ERROR: ${error}`, 'ERROR');
    });
  }

  public grabarDatos() {

    if (this.formularioDatos.invalid) {
      return;
    }

    // Recupero los datos del formulario
    this.loading = true;

    const campos = this.formularioDatos.value;

    // Y restauramos los datos del usuario en base al formulario
    // Los campos que no van a ser modificados iran a undefined y no se actualizaran
    this.userData = this.usersRegisterMapperService.transform(campos);

    // El telefono no se actualiza
    this.userData.mobile = null;

    this.usersService.updateUser(this.userData)
                      .subscribe((resp: any) => {

                        // tslint:disable-next-line: max-line-length
                        //this.usersService.addUserLog(this.loginService.getUser().idUser, `Grabar datos generales - Datos de usuario empleador`, 'OK');

                        // Grabo los datos de la empresa a la que corresponde
                        this.usersEmployeerData = this.usersEmployeersMapperService.transform(campos);
                        this.usersEmployeerData.idUser = this.userData.idUser;
                    }, (error) => {
                      console.log(error);

                      this.loading = false;

                      // tslint:disable-next-line: max-line-length
                      this.alertService.error(this.translateService.instant('ERROR.HAOCURRIDOUNERROR'), AlertService.AlertServiceContextValues.UserData);

                      // tslint:disable-next-line: max-line-length
                      //this.usersService.addUserLog(this.loginService.getUser().idUser, `Grabar datos generales f- Datos de usuario empleador - ERROR: ${error}`, 'ERROR');
                    });
  }

  closeModal(sendData) {
    this.activeModal.close(sendData);
  }
}
