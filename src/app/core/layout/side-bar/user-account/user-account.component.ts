import { Utils } from './../../../utils/utils';
import { Component, ElementRef, HostListener, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/core/models/users/user.models';
import { LoginService } from 'src/app/core/services/login';
import { UserDataGeneralComponent } from 'src/app/pages/user-data/user-data-general/user-data-general.component';
import { ImagenPipe } from 'src/app/shared/pipes/imagen.pipe';
import { MODAL_DIALOG_TYPES } from 'src/app/core/utils/modal-dialog-types';
import { UsersService } from 'src/app/core/services/users';
import { Profiles } from 'src/app/core/utils/profiles.enum';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ModalProfileActionsComponent } from 'src/app/shared/components/modal-profile-actions/modal-profile-actions.component';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/models/store.model';
import { availableProfiles } from 'src/app/store/models/profiles.model';

const NOIMAGE = "../../../../../assets/images/icons/account_circle.svg"
@Component({
	selector: 'app-user-account',
	templateUrl: './user-account.component.html',
	styleUrls: ['./user-account.component.scss'],
	providers: [ImagenPipe],
	encapsulation: ViewEncapsulation.None
})
export class UserAccountComponent implements OnInit {

	showUserAccount: boolean = false
	user: User
	profiles = Profiles;
	inside: boolean;
	actualProfile: string;
	private destroy$ = new Subject();
	profilesOfUser: availableProfiles;

	@HostListener('document:mousedown', ['$event'])
	onGlobalClick(event): void {
		if (!this.eRef.nativeElement.contains(event.target)) {
			this.showUserAccount = false;
			this.destroy$.next(true);
		}
	}

	constructor(private loginService: LoginService,
		private modalService: NgbModal,
		private imagePipe: ImagenPipe,
		private utils: Utils,
		public usersService: UsersService,
		private store: Store<State>,
		private eRef: ElementRef) {
		this.user = this.loginService.getUser()

	}

	ngOnInit() {
		//this.actualProfile = this.loginService.getProfile();
		this.store.select(store => store.profiles).subscribe((profiles) => {
			this.profilesOfUser = profiles
		})
		this.store.select(store => store.selectedProfile).subscribe((selectedProfile) => {
			this.actualProfile = selectedProfile['selectedProfile']
		})
	}

	ngOnDestroy() {
		this.destroy$.next(true);
	}

	showMenuPanel() {
		if (!this.showUserAccount) {
			this.showUserAccount = !this.showUserAccount;
		} else {
			this.showUserAccount = !this.showUserAccount;
			this.destroy$.next(true);
		}
	}

	setProfile(profile) {
		if(profile != this.profiles.Student){
			localStorage.removeItem('selectedGroupInfo');
		}
		this.loginService.setProfile(profile)
		this.showModalProfileActions();
	}

	showGeneralSettings(): void {
		this.showUserAccount = false
		let config = { scrollable: false, windowClass: `${MODAL_DIALOG_TYPES.W60}` }
		this.modalService.open(UserDataGeneralComponent, config)
	}

	getUserAvatar(): string {
		return this.utils.getUserAvatar(this.user.pictureUser)
	}

	logout(): void {
		localStorage.removeItem('selectedGroupInfo');
		this.loginService.logout(true);
	}

	showModalProfileActions(){
	  this.modalService.open(ModalProfileActionsComponent, {scrollable: false, windowClass: MODAL_DIALOG_TYPES.W80 } );
		this.showUserAccount = false;
		this.destroy$.next(true);
	}
}
