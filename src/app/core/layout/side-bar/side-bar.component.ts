import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { Component,OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NgxCopilotService } from 'ngx-copilot';
import { environment } from 'src/environments/environment';
import { TutotrialDataLocal } from '../../models/tuturial/tutorial.model';
import { User } from '../../models/users/user.models';
import { LoginService } from '../../services/login';
import { LocalStorage } from '../../utils/local-storage';
@Component({
    selector: 'app-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SideBarComponent implements OnInit, OnDestroy {

    public arrayValues: number[] = []
    public usuario: User;

    cpPosition: any;
    sidebarData: boolean;
    urlTutorial: string = environment.tutorial


    public constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private modalService: NgbModal,
        public translateService: TranslateService,
        public loginService: LoginService,
        private localStorage: LocalStorage,
        private copilot:  NgxCopilotService,
        private localStorageService: LocalStorage
    ) {
        this.activatedRoute.params.subscribe(params => {
            // De esta forma cada vez que cambia el parametro lo actualizo
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        });

    }
    public ngOnInit() {
        this._traducirOpciones();
        this.tutorialDisponible();
    }

    ngOnDestroy(): void {}

    tutorialDisponible() {
        if(this.loginService.comprobarTutorial()){
            let canvasGrafoTutorial = JSON.parse(this.localStorageService.getItem(LOCALSTORAGESTRINGS.TUTORIALDATA)) as TutotrialDataLocal;
            this.sidebarData = canvasGrafoTutorial.sideBarData;
                this.abrirTutorial();
        }
        else {
            this.sidebarData = true;
        }
    }
    abrirTutorial() {
        if (!this.sidebarData) {
            setTimeout(() => {
                this.initPosition(10);
            }, 500);
        }
    }
	/*Re initialize in specify step*/
    initPosition  = (stepNumber:any) => {
        this.copilot.checkInit(stepNumber)

    };
    nextStep = (stepNumber:any) =>  this.copilot.next(stepNumber);

    /*Finish*/
    done= () =>  {
        this.copilot.removeWrapper()
        let cursoTotorial = JSON.parse(this.localStorageService.getItem(LOCALSTORAGESTRINGS.TUTORIALDATA)) as TutotrialDataLocal;
        cursoTotorial.sideBarData = true;
        this.loginService.updateDataLocalTutorial(cursoTotorial);
    };


    private _traducirOpciones():void {
        const lang = this.localStorage.getItem(LOCALSTORAGESTRINGS.LANG);
        this.translateService.use(lang);
    }

		logout(): void { this.loginService.logout(true); }
}
