export class ColorUtils {

    public constructor() { }

    public  LEAD = 'rgb(165, 165, 165)';//plomo
    public  RED =  'rgb(255, 11, 18)';//rojo
    public  YELLOW = 'rgb(255, 252, 56)';//amarillo
    public  GREEN = 'rgb(138, 253, 50)';//verde
    public  GREEN_DARK = 'rgb(40, 248, 45)';//verde oscuro
}
