export const GRAPH_EDGE_TYPES = {
    LINE: 'line',
    CURVE: 'curve',
    ARROW: 'arrow',
    CURVED_ARROW: 'curvedArrow',
    BRANCH: 'branch'
}
