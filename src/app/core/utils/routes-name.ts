export const ROUTES_NAME = {
    LOGIN: '/auth/login',
    FORGOT_PASSWORD: '/auth/forgot-password',
    SIGN_UP: '/auth/sign-up',
    REGISTER_NEW_USER: '/auth/register-new-user',
    DASHBOARD: '/dashboard'
}
