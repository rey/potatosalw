import { NgxCopilotService } from "ngx-copilot";
export class NgxCopilotUtils {
    title: string;
    name: string;
    copilotOrder: number = 0;
    listNgxCopilot: [] = [];

    constructor(public copilot: NgxCopilotService) {
        
    }
 
    skip = (o: any = null) => this.copilot.next(o);

    initPosition = (o: any) => this.copilot.checkInit(o);
  
    done = () => this.copilot.removeWrapper()
  
    init = () => this.copilot.checkInit()

}