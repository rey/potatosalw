import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginService } from '../services/login';

// Servicios

@Injectable({
  providedIn: 'root'
})
export class VerificaTokenGuard implements CanActivate {

  constructor(public loginService: LoginService) { }

  canActivate(): boolean | Promise<boolean> {

    const token = this.loginService.getToken();
    const payload = JSON.parse(atob(token.split('.')[1]));

    const expirado = this.expiradoToken(payload.exp);

    if (expirado) {
      return false;
    }

    return this.verificaRenuevaToken(payload.exp);
  }

  verificaRenuevaToken(fechaExp: number): Promise<boolean> {
    return new Promise((resolve, reject) => {

      const tokenExp = new Date( fechaExp * 1000);
      const ahora = new Date();

      ahora.setTime(ahora.getTime() + (1 * 60 * 60 * 1000));

      if (tokenExp.getTime() > ahora.getTime() ) {
        resolve(true);
      } else {
        // PENDIENTE
        // this.loginService.renewToken()
        //         .subscribe( () => {
        //           resolve(true);
        //         }, () => {
        //           this.loginService.logout();
        //           reject(false);
        //         });
      }

      resolve(true);
    });
  }

  expiradoToken( fechaExp: number) {

    const ahora = new Date().getTime() / 1000;

    if (fechaExp < ahora) {
      return true;
    } else {
      return false;
    }
  }
}
