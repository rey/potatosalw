import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponse } from '../../models/shared/api-response.model';
import { User } from '../../models/users/user.models';

@Injectable({
    providedIn: 'root'
})
export class ForgotPasswordService {

    constructor(private http: HttpClient) { }

    forgotPassword(idUser: number, idUserEmail: number) {
        const url = `user/sendMailForgetUserByMobile/${idUser}/${idUserEmail}`;
        return this.http.get<any>(url);
    }

    getUsersByMobile(mobile: string, mobilePrefix:string): Observable<User[]> {
        return this.http.post<ApiResponse<User[]>>(`user/getListUserByMobile`, {mobile: mobile, extension: mobilePrefix}).pipe(map(res => res.data));
    }
}
