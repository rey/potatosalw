import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ValidateAccountService {

    constructor(private http: HttpClient) { }

    public validateAccount(idUser: number, uuid: string): Observable<any> {
        const url = `user/get-validation-user-mail?idUser=${idUser}&uuid=${uuid}`;
        return this.http.get<any>(url);
    }
}
