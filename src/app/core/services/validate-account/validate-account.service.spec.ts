import { TestBed } from '@angular/core/testing';

import { ValidateAccountService } from './validate-account.service';

describe('ValidateAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidateAccountService = TestBed.get(ValidateAccountService);
    expect(service).toBeTruthy();
  });
});
