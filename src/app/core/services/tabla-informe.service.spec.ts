import { TestBed } from '@angular/core/testing';

import { TablaInformeService } from './tabla-informe.service';

describe('TablaInformeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TablaInformeService = TestBed.get(TablaInformeService);
    expect(service).toBeTruthy();
  });
});
