import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SnackbarService {
    private SUCCESS: string = 'snackSuccess';
    private ERROR: string = 'snackError';
    private INFO: string = 'snackInfo';

    constructor(private snackBar: MatSnackBar) { }

    public success(message: string, duration?:number) {
        this.showCenter(message, this.SUCCESS, duration);
    }

		public successCenter(message: string, duration?:number) {
				this.showCenter(message, this.SUCCESS, 10000);
		}

    public error(message: string) {
        this.showCenter(message, this.ERROR);
    }
    public info(message: string) {
        this.showCenter(message, this.INFO);
    }
    private show(message: string, type: string, duration?:number) {
        this.snackBar.open(message, undefined, {
            duration: duration || 5000,
            horizontalPosition: 'left',
            verticalPosition: 'top',
            panelClass: type
        });
    }
		private showCenter(message: string, type: string, duration?: number) {
			const snackBarRef = this.snackBar.open(message, undefined, {
					duration: duration || 3000,
					horizontalPosition: 'center',
					verticalPosition: 'top',
					panelClass: type
			});

			snackBarRef.afterOpened().subscribe(() => {
					const snackBarElement = document.querySelector('.mat-snack-bar-container');
					if (snackBarElement) {
							snackBarElement.addEventListener('click', () => {
									snackBarRef.dismiss();
							});
					}
			});
	}
}
