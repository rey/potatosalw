import { TestBed } from '@angular/core/testing';

import { SigmaToolbarsService } from './sigma-toolbars.service';

describe('SigmaToolbarsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SigmaToolbarsService = TestBed.get(SigmaToolbarsService);
    expect(service).toBeTruthy();
  });
});
