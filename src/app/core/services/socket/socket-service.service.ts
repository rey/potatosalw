import { BehaviorSubject } from 'rxjs';
import { LoginService } from 'src/app/core/services/login';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ConfigChallengeModel } from '../../models/groups/groups-model';

@Injectable({
	providedIn: "root",
})
export class SocketService {
	public msgFromServer = new Subject<string>();
	public closeSocket = new Subject<CloseEvent>();
	public openSocket = new Subject<Event>();
	public answeredBySocket: BehaviorSubject<boolean> =
		new BehaviorSubject<boolean>(false);

	public studentsConnected: Subject<any[]> =
		new Subject<any[]>();

	private _socket: WebSocket;
	private _idSession: number;
	private _configChallenge: ConfigChallengeModel;
	public _syncChallenge: BehaviorSubject<boolean> =
		new BehaviorSubject<boolean>(false);

	public getansweredBySocket() {
		return this.answeredBySocket.asObservable();
	}

	public setansweredBySocket(value: boolean): void {
		this.answeredBySocket.next(value);
	}

	public getStudentsConnected() {
		return this.studentsConnected.asObservable();
	}

	public setStudentsConnected(value: any): void {
		this.studentsConnected.next(value);
	}

	public get configChallenge(): ConfigChallengeModel {
		return this._configChallenge;
	}
	public set configChallenge(value: ConfigChallengeModel) {
		this._configChallenge = value;
	}

	public get idSession(): number {
		return this._idSession;
	}
	public set idSession(value: number) {
		this._idSession = value;
	}

	public get socket() {
		return this._socket;
	}
	public set socket(value) {
		this._socket = value;
	}

	public getSyncChallenge() {
		return this._syncChallenge.asObservable();
	}

	public setSyncChallenge(value: boolean): void {
		this._syncChallenge.next(value);
	}

	constructor(private loginService: LoginService) {}

	createSocket(idGroup: number): void {
		const idUser = this.loginService.getUser().idUser;
		try {
			this._socket = new WebSocket(
				`${environment.ws}://${environment.socketIp}:${environment.socketPort}${environment.socketUrl}${idUser}/${idGroup}`
			);
			this._socket.onopen = (ev) => {
				// console.log("SE ABRE EL SOCKET: ", ev)
				this.openSocket.next(ev);
			};
			this._socket.onclose = (ev) => {
				// console.log("SE HA DESCONECTADO EL SOCKET: ", ev)
				this.closeSocket.next(ev);
			};

			this._socket.onmessage = (msg) => {
				// console.log("SE HA RECIBIDO UN MENSAJE DEL SOCKET: ", msg)
				this.msgFromServer.next(msg.data);
			};
			this._socket.onerror = (err) => {
				console.error("ERROR EN EL WS: ", err);
			};
		} catch (e) {
			console.error("ERROR AL ABRIR EL SOCKET: ", e);
		}
	}

	removeSocket(): void {
		if (this._socket) this._socket.close();
	}

	sendDataToServer(msg: string) {
		this._socket.send(msg);
	}
}
