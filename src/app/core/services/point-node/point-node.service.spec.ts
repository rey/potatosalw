import { TestBed } from '@angular/core/testing';

import { PointNodeService } from './point-node.service';

describe('PointNodeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PointNodeService = TestBed.get(PointNodeService);
    expect(service).toBeTruthy();
  });
});
