import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http:HttpClient) { }

  /**
   * @returns
   */
  getListCenterByIdUser():Observable<any>{
    return this.http.get<any>(`getcentersasignedteacher`)
  }

}
