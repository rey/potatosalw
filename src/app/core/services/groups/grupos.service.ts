import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GroupModel } from '../../models/groups/groups-model';

export const SOCKETMESSAGES = {
	"OPEN" : "open_quiz",
	"CLOSE" : "close_quiz",
	"FINISH" : "finish_quiz",
	"CLOSESOCKET" : "close_socket",
	"USERSCOUNTER" : "counter_users",
	"ORDERMODLIST" : "order_mod_list",
	"ORDERMODAUTO" : "order_mod_auto"
}

@Injectable({
  providedIn: 'root'
})
export class GruposService {

  constructor(private http: HttpClient) { }

  createGroup(group: GroupModel, image?: File | string){
      const url = `group/createupdate/group`;
      const formData: FormData = new FormData();
      const q = {
        idGroup: group.idGroup,
        idProfessor: group.idProfessor,
        title: group.title,
        description: group.description,
        imagen: '',
        idLanguage: group.idLanguage,
        idCenter: group.idCenter,
        share: group.share,
        creationDate: group.creationDate,
        editDate: group.editDate,
        identification: group.identification
    }

    formData.append('files', image);
    formData.append('group',  JSON.stringify(q));
    return this.http.post<any>(url, formData);
  }

  updateGroup(group: GroupModel, image: string | File) {
    // @PostMapping("/group/createupdate/group")
    const url = `group/createupdate/group`;
    const formData: FormData = new FormData();
    const q = {
      idGroup: group.idGroup,
      idProfessor: group.idProfessor,
      title: group.title,
      description: group.description,
      imagen: group.imagen,
      idLanguage: group.idLanguage,
      idCenter: group.idCenter,
      share: group.share,
      creationDate: group.creationDate,
      editDate: group.editDate,
      identification: group.identification
    }

    formData.append('files', image);
    formData.append('group',  JSON.stringify(q));
    return this.http.post<any>(url, formData);

  }

  /**
   *
   * @param group de tipo GroupModel
   * @returns
   */
  deleteGroup(group: GroupModel) {
    // @PostMapping("/group/createupdate/group") const q = {
        const url = `group/deletegroup`;
        const q = {
          idGroup: group.idGroup,
          nameImage: group.imagen,
      }
      return this.http.post<any>(url, q);
  }

  getGrupos() {
    const url = `group/getgroups`;
    return this.http.get<any>(url);
  }


  getListEstudiantes(idGrupo: number){
    const url = `group/studentsListByGroup/${idGrupo}`;
    return this.http.get<any>(url);
  }

  getListadoEstudiantesInscribir( idGrupo: number, filter: string) {
     const url = `group/StudentsNotAsigned`;
        const q = {
          idGroup: idGrupo,
          filter: filter,
      }
      return this.http.post<any>(url, q);
  }

  //  Endpoint para invitar al estudiante
  getInvitarEstudiante( idGrupo: number, idUser: number) {
    const url = `group/inviteStudentToGroup/${idGrupo}/${idUser}`;
     return this.http.get<any>(url);
 	}


  //  Endpoint para obtener el listado de peticiones de grupos como estudiante
  getPeticionesEstudiante(){
    const url = `group/groupInviteList`;
    return this.http.get<any>(url);
  }

	//Endpoint de listado de peticiones para acceder al grupo como profesor
  getPeticionesProfesor(idGroup: number){
    const url = `group/studentsInviteList/${idGroup}`;
    return this.http.get<any>(url);
  }

  filtrarEstudiantesBusqueda(idGroup: number, filter: any){
    const url = `group/studentsnotasignedidentification`;
    const q = {
      idGroup: idGroup,
      filter: filter.filtrado,
    }
    return this.http.post<any>(url, q);
  }

  filtrarCursosBusqueda(idGroup: number, filter: any, idTeacher: number){
    const url = `group/targetListNotAsignedToGroup`;
    const q = {
      idGroup: idGroup,
      filter: filter.filtrado,
      idTeacher: idTeacher,
    }
    return this.http.post<any>(url, q);
  }

   /////////////////////////
  //  Como profesor
    /////////////////////////
/**
 *
 * @param idGrupo
 * @param idUser
 * @returns
 */
  aceptarInvitacion(idGrupo: number, idUser: number){
    const url = `group/acceptInvitation/teacher`;
    const q = {
      idGroup: idGrupo,
      idStudent: idUser,
    }
    return this.http.post<any>(url, q);
  }

  /**
   *
   * @param idGrupo
   * @param idUser
   * @returns
   */
  rechazarInvitacion(idGrupo: number, idUser: number){
    const url = `group/deleteInvitation/teacher`;
    const q = {
      idGroup: idGrupo,
      idStudent: idUser,
    }
    return this.http.post<any>(url, q);
  }

	aceptarInvitacionEstudiante(idGrupo: number, idUser: number){
		const url = `group/acceptInvitation/student`;
		const q = {
			idGroup: idGrupo,
			idStudent: idUser,
		}
		return this.http.post<any>(url, q);
	}

	rechazarInvitacionEstudiante(idGrupo: number, idUser: number){
		const url = `group/deleteInvitation/student`;
		const q = {
			idGroup: idGrupo,
			idStudent: idUser,
		}
		return this.http.post<any>(url, q);
	}

  eliminarEstudiante(idGrupo: number, idUser: number){
    const url = `group/deleteStudent`;
    const q = {
      idGroup: idGrupo,
      idStudent: idUser,
    }
    return this.http.post<any>(url, q);
  }


  getGruposEstudiante() {
    const url = `group/groupsListByStudent`;
    return this.http.get<any>(url);
  }


    // ENDPOINT PARA OBTENER LISTA DE INVITACIONES A GRUPOS DE UN ESTUDIANTE
    // URL:
    // api-dev-pruebas/rest/group/groupInviteList/{idStudent}
    // PARAMETROS GET:
    // idStudent : String


  getGruposInvitaciones() {
    const url = `group/groupInviteList`;
    return this.http.get<any>(url);
  }



  // ENDPOINT PARA BUSCAR UN GRUPO POR NOMBRE
  // URL:
  // api-dev-pruebas/rest/group/groupListNotAsigned/{idStudent}
  // PARAMETROS – BODY POST:
  // {
  //     "filter":"ja"
  // }

  getGruposFiltradoEstudiante(filter: any) {

    const url = `group/groupListNotAsigned`;
    const q = {
      filter: filter.filtrado,
    }
    return this.http.post<any>(url, q);

  }


  // ENDPOINT PARA SOLICITAR EL REGISTRO DE UN ESTUDIANTE A UN GRUPO
  // URL:
  // api-dev-pruebas/rest/group/requestStudentToGroup/{idGroup}/{idStudent}
  solicitarUnirGrupo(idGroup: number) {

    const url = `group/requestStudentToGroup/${idGroup}`;

    return this.http.get<any>(url);

  }

  getListCursos(idGrupo: number){
    const url = `group/coursesListByGroup/${idGrupo}`;
    return this.http.get<any>(url);
  }

  getAgregarCurso( idGrupo: number, idCourse: number) {
    const url = `group/addCourseToGroup/${idGrupo}/${idCourse}`;
     return this.http.get<any>(url);
 }

 agregarCurso( idGrupo: number, idCourse: number) {
	const url = `group/addAllTargetToGroup/${idGrupo}/${idCourse}`;
	 return this.http.get<any>(url);
	}

	agregarTarget( idGrupo: number, idCourse: number, idTarget: number) {
		const url = `group/addTargetToGroup/${idGrupo}/${idCourse}/${idTarget}`;
		return this.http.get<any>(url);
	}

 eliminarCurso(idGrupo: number, idCourse: number){
    const url = `group/deleteGroupsCourses/${idGrupo}/${idCourse}`;
    return this.http.delete<any>(url);
  }

	eliminarGrafo(idGrupo: number, idCourse: number, idTarget: number){
    const url = `group/deleteTargetToGroup/${idGrupo}/${idCourse}/${idTarget}`;
    return this.http.delete<any>(url);
  }

	/**
	 * Tenemos que revisar si cuando el alumno entra en su grupo, existe creada una sesión de Actividades Instantáneas (socket) para poder comenzar a realizar el examen
	 * @param idGroup
	 * @returns
	 */
	getIfSessionExists(idGroup:number):Observable<any>{
		const url = `group/existsession/${idGroup}`
		return this.http.get<any>(url)
	}

	/**
	 * Función obtiene los usuarios conectados a la sesion del grupo
	 * @param idGroup ID del grupo
	 * @returns
	 */
	getGroupSession(idGroup): Observable<any>{
		const url = `group/getgroupsession/${idGroup}`
		return this.http.get(url);
	}

	/**
	 * Función que crea una sesion para poder realizar actividades mediante WebSocket
	 * @param idGroup ID del grupo
	 * @returns
	 */
	createGroupSession(idGroup): Observable<any>{
		const url = 'group/creategroupsession'
		const body = {
			idGroup: idGroup
		}
		return this.http.post<any>(url, body)
	}

	/**
	 * Función que
	 * @param idGroup ID del grupo
	 * @returns
	 */
	deleteGroupSession(idGroup:number):Observable<any>{
		const url = `group/deletegroupsession/${idGroup}`
		return this.http.delete(url)
	}

	/**
	 * Función para mandar mensaje al servidor para distribuirlo a través del socket y que llegue a los alumnos
	 * @param idQuiz ID del quiz
	 * @param idGroup ID del grupo, que representará la room donde esté incluida la sesion del usuario
	 * @param action Acción: open_quiz,closed_quiz,finish_quiz
	 */
	sendDataToServerSocket(idQuiz:number, idGroup:number,action:string, idTarget?:number, idCourse?:number):Observable<any>{
		let urlParams:HttpParams = new HttpParams().set('idQuiz', idQuiz.toString()).set('room', idGroup.toString())
		if(idTarget >=0)
			urlParams = urlParams.append('idTarget', idTarget.toString())
		if(idCourse >=0)
			urlParams = urlParams.append('idCourse', idCourse.toString())

		const url = `socket/sendMsgToRoomClients/${action}`
		return this.http.post<any>(url,{}, {params: urlParams})
	}

	/**
	 * METODO PARA TRAER LISTADO DE GRAFOS QUE ESTAN EN UN GRUPO
	 * @PostMapping("/targetListAsignedToGroup")
	 */
	targetListAsignedToGroup(idGroup: number, filter: String): Observable<any>{
		const url = `group/targetListAsignedToGroup`
		const body = {
			idGroup: idGroup,
			filter: filter
		}
		return this.http.post<any>(url, body)
	}

	/**
	 * Función para cambiar el nombre de la sesión cuando el profesor abre un socket
	 * @param idSession ID de la session
	 * @param nameSession Nombre nuevo de la sesión
	 * @returns
	 */
	updateNameSession(idSession:number, nameSession:string, idGroup:number): Observable<any>{
		const url = `group/updatenamegroupsession/${idSession}`
		const body = {
			nameSession : nameSession,
      idGroup: idGroup
		}
		return this.http.put(url, body)
	}

	/**
	 *
	 * @param idTaget ID del grafo
	 * @param idGroup ID del grupo (sala/room del socket)
	 * @returns
	 */
	sendChallengeListModeSocket(idTaget:number, idGroup:number, time:number): Observable<any>{
		const urlParams:HttpParams = new HttpParams().set('idTarget', idTaget.toString()).set('room', idGroup.toString()).set('time', time.toString())
		const url = `socket/sendMsgToRoomClientsFreeOrder/order_mod_list`
		return this.http.post<any>(url,{}, {params: urlParams})
	}

	/**
	 * Función para crear el orden de los quizzes cuando se selecciona un grafo con la opción aleatoria
	 * @param idTaget ID del grafo
	 * @param idGroup ID del grupo (sala/room del socket)
	 * @param idCase Número con la configuración elegida ( 1. Por defecto, sincronizado y aleatorio - 2. Sincronizado y no aleatorio - 3. No sincronizado y si aleatorio - 4. No sincronizado y no aleatorio)
	 * @returns
	 */
	sendModAutoChallengeSocket(idTaget:number, idGroup:number, idCase: number): Observable<any>{
		const urlParams:HttpParams = new HttpParams().set('idTarget', idTaget.toString()).set('room', idGroup.toString()).set('idCase', idCase.toString())
		const url = `socket/sendMsgToRoomClientsAutoOrder/order_mod_auto`
		return this.http.post<any>(url,{}, {params: urlParams})
	}

  /**
   * Función para obtener los desafíos que ha generado el profesor
   * @param idGroup ID del grupo
   * @returns
   */
	getChallenges(idGroup:number): Observable<any>{
		const url = `group/getchallengesbygroup/${idGroup}`
    return this.http.get(url)
	}

	deleteChallenge(idGroup:number, idSession:number){
		const url = `group/deletegroupsession/${idGroup}/${idSession}`
		return this.http.delete(url)
	}
}
