import { TestBed } from '@angular/core/testing';

import { QuizzesstackService } from './quizzesstack.service';

describe('QuizzesstackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuizzesstackService = TestBed.get(QuizzesstackService);
    expect(service).toBeTruthy();
  });
});
