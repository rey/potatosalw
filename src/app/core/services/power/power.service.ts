import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PowerService  {
    public powerActive = new Subject();
    public checksVisibility = new Subject();
    tmpObject: any = {};

    /**
     *
     */
    constructor() {
        let tmpObject: any = {};
        tmpObject.power0 = true;
        this.setPower(tmpObject);
    }
    public emitPowerActive(numberPower: Array<number>) {
        this.powerActive.next(numberPower)
    }


    public emitChecksVisibility(visible) {
        this.checksVisibility.next(visible)
    }

    public getPower(): {} {
        return this.tmpObject;
    }

    public setPower(tmpObject: {}) {
        this.tmpObject = tmpObject;
    }

    public convertNumberArrayToPowerObject(powers:Array<number>){
        let tmpObject:any = {}

        powers.forEach(e => {
            switch(e){
                case 3:
                    tmpObject.power3 = true;
                    this.setPower(tmpObject);
                    break;
                case 2:
                    tmpObject.power2 = true;
                    this.setPower(tmpObject);
                    break;
                case 1:
                    tmpObject.power1 = true;
                    this.setPower(tmpObject);
                    break;
                case 0:
                    tmpObject.power0 = true;
                    this.setPower(tmpObject);
                    break;
                case -1:
                    tmpObject.powerNegative1 = true;
                    this.setPower(tmpObject);
                    break;
                case -2:
                    tmpObject.powerNegative2 = true;
                    this.setPower(tmpObject);
                    break;
                case -3:
                    tmpObject.powerNegative3 = true;
                    this.setPower(tmpObject);
                    break;
            }
        })
        return tmpObject;

    }

}

