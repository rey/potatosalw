export interface AppAccessZone {
    id:         number;
    name:       string;
    validUsers: number[];
}
