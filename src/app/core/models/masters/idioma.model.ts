export class IdiomaModel {
    idLanguage:    number;
    idLanguageIso: string;
    language:      string;
    numCourses:    number;
    valid:         string;
}
