export interface CommunityModel{
    idCountryRegion: number,
    regionName: string
}

export interface ResponseCommunityModel{
    error: error
    data: CommunityModel[],
    status: number
}

interface error{
    code:number,
    msg: string
}
