export class MateriaModel {
    idSubject: number;
    subject: string;
    description: string;
    idHumanKnowledge: string;
    humanKnowledgeName: string;
}
