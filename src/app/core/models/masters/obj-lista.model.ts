export class ObjListaModel{
    idCourse: number;
    idTarget: number;
    idNode: number;
    idQuiz: number;
    isMulti: boolean;
    name: string;
    icon: string;
    type: string;
    hasChildren: boolean;
    children?: ObjListaModel[] = [];
}
