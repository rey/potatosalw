export class CenterModel {
    idCenter: number;
    centerName: string;
    centerAddress: string;
    centerCountry: string;
    centerRegion: number;
    centerMail: string;
    extension: string;
    centerPhone: string;
    centerUrl: string;
    centerPicture: string;
}


/**
 * RESPONSE CENTER BY ID USER
 */
export interface ResponseCenterByIdModel{
    status: number,
    data: CenterById[],
    error : ErrorModel
}

export interface CenterById{
    idCenter:      number;
    authorization: number;
    centerAddress: string;
    centerMail:    string;
    centerName:    string;
    extension:     string;
    centerPhone:   string;
    centerPicture: string;
    centerUrl:     string;
    remarks:       string;
    province:      string;
}

interface ErrorModel{
    code: number,
    msg: string
}
