import { ErrorModel } from './../shared/error.model';

export interface PublishResponseModel{
	error: ErrorModel,
	data: boolean,
	status: number
}