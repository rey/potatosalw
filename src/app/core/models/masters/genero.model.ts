export class GeneroModel {
    idGenre: number;
    genre: string;

    constructor(idGenre: number, genre: string) {
        this.idGenre = idGenre;
        this.genre = genre;
    }
}
