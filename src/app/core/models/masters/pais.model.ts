export class PaisModel {
    idCountry:         string;
    country:           string;
    fieldOrder:        number;
    idCountry_ISO3166: string;
    internetDomain:    string;
    centers:           string;
    countriesRegions:  string;
    country_de:string;
    country_es:string;
    country_pt:string;
}
