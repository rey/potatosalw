import { AppAccessZone } from './app-access-zones.model';
//export const APIURL:string = 'https://api.salware.net'
//export const CDNURL:string = 'https://cdn.salware.net'
export const APIURL:string = 'https://salware.com'
export const APIURLLOCAL:string = 'http://localhost:8082'
export const IMAGESASSETS:string = '/assets/images/'
export const CAPTCHAAPIKEY:string = '6Lfkd20gAAAAABesT1OjJ8FzxyQkiRsZBl9V__H_'
export const DEFAULTMOBILEPREFIX:string = '+34'
export const DEFAULTFONTFAMILY:string = 'San Francisco'
export const DEFAULTCLOSEPADS: number = 600000
export const DEMOUSER = { USER: '700000000', PASS : '12345678' }
export const TINYAPYKEY: string = '135w168q0jrddamozfsxjqfwz87twmscvybeb2j878kqj6he'
export const URL_TIKTOK = "https://www.tiktok.com/embed/v2/"
export const API_KEY_CHAT_GTP = "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"

export const DEFAULT_TEACHER_PROMPT:string = 'Para evaluar tu decisión\nNo consideres faltas de ortografía\nNo consideres espacios ni signos de puntuación.\nLas respuestas con abreviaturas son equivalente a las respuestas sin abreviaturas.'

export enum UrlWebDomain{
	LOCAL = 'http://localhost:4200/',
	DEV = 'https://dev.salware.com',
	PRE = 'https://pre.salware.com',
	DEMO = 'https://demo.salware.com',
	TEST = 'https://test.salware.com',
	PRO = 'https://salware.com',
	PSYCAT = 'https://psycast.salware.com',
	FOUNDATION = 'https://fundacionsalware.org'
}

/**
 * Versiones de la app
 */
export enum AppVersions{
	PROD = '1.0.32',
	API = '4.03.37',
}


/**
 * Centros de la aplicación
 */
export enum DomainTypes{
    DEV = 'dev',
    LOCAL = 'local',
    SALWARE = 'salware',
    OMS = 'oms',
    FOUNDATION = 'fundacionsalware',
    PRE = 'pre',
    DEMO = 'demo',
		PSYCAST = 'psycast'
}


/**
 * Carpetas de las imágenes según el entorno en el que nos encontremos
 */
export enum ImageEnvironments{
    DEV = '/images-dev',
    PRO = '/images',
    DEMO = '/images-demo'
}


/**
 * Entornos de la api
 */
export enum ApiEnvironments{
    LOCAL = '/api/rest/',
    DEV = '/api-dev/rest/',
    TEST = '/api-dev-pruebas/rest/',
    PRO = '/api/rest/',
    DEMO = '/api-demo/rest/'
}

export enum WebSocketEnvironment{
	LOCAL = '/api/websocket/',
	DEV = '/api-dev/websocket/',
	TEST = '/api-dev-pruebas/websocket/',
	PRO = '/api/websocket/',
	DEMO = '/api-demo/websocket/'
}

/**
 * Imágenes de fondo del login según los centros creados
 */
export enum LoginImage{
    DEV = 'fondoLogin.png',
    PRO = 'fondoLogin.png',
    OMS = 'fondoLogin.png',
    PRE = 'fondoLogin.png',
    DEMO = 'fondoLogin.png'
}

/**
 * Direccion de documentos de politicas de privacidad según los centros creados
 */
export enum PrivacyPolicies{
	FOUNDATION = 'politica_de_privacidad_salware.pdf',
	PSYCAST = 'politica_de_privacidad_psycast.salware.pdf'
}

/**
 * Logos de salware según los centros creados
 */
export enum HomeLogo{
	FOUNDATION = 'logofundacionsalware.png',
	SALWARE = 'logo-azul.png'
}

/**
 * Registramos las zonas de la app donde vamos a poner restricciones
 */
export enum AppZones{
    QUIZMULTIPLEBUTTON = 'multipleQuizzesButton'
}


/**
 * Aqui tenemos las zonas con los ids de acceso en cada una de las zonas/botones que creamos
 */
export const AccessZones: AppAccessZone[] = [
    {
        id: 1,
        name: AppZones.QUIZMULTIPLEBUTTON,
        validUsers : [62,63,64,87] //Ids de los usuarios que tienen acceso a la zona
    }
]
