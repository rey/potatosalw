import { ErrorModel } from './../shared/error.model';

export class AuthorResponseModel {
    error: ErrorModel
    data: AuthorModel[]
    status: number
}


export interface AuthorModel{
    idUser: number,
    name : string
}


