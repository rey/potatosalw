export interface RegionModel {
    idCountryRegion: number;
    regionName:      string;
    centers:         any[];
    country:         Country;
}

export interface Country {
    idCountry:         string;
    country:           string;
    fieldOrder:        number;
    idCountry_ISO3166: string;
    internetDomain:    string;
    centers:           any[];
    countriesRegions:  any[];
}
