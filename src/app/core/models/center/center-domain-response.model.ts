export interface ResponseCenterDomainModel {
    error:  Error;
    data:   ConfigCenter;
    status: number;
}

export interface ConfigCenter {
    idCenter:  number;
    imgLogin:  string;
    imgLogo:   string;
    subDomain: string;
    idCourse: number;
    idTarget: number;
}

export interface Error {
    code: number;
    msg:  string;
}
