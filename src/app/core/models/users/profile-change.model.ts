import { Profiles } from './../../utils/profiles.enum';
export interface ProfileChange {
    profile:   Profiles;
    sendEvent: boolean;
}
