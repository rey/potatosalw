export class UserStudentCenterModel {
    idUserStudentCenter: number;
    idUser: number;
    idCenter: number;
    creationDate: Date;
    creationDateString: string;
}
