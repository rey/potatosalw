import { Estado } from "../tuturial/tutorial.model";
export class User {
    idUser: number;
    firstName: string;
    surname: string;
    keyWord: string;
    mail: string;
    mobile: string;
    profile: string;
    birthdate: number;
    idGenre: number;
    idLanguageIso: string;
    pictureUser: string;
    tutorialSW: TutorialDataClass;
    invitedToGroup: string;
		identification: string;
		extension?: string;
		profileEditor?: boolean;
		profileStudent?: boolean;
		profileTeacher?: boolean;
		profileParent?: boolean;
		profileAdmin?: boolean;


    constructor( object: any){
        this.idUser = (object.idUser) ? object.idUser : null;
        this.firstName = (object.firstName) ? object.firstName : null;
        this.surname = (object.surname) ? object.surname : null;
        this.keyWord = (object.keyWord) ? object.keyWord : null;
        this.mail = (object.mail) ? object.mail : null;
        this.mobile = (object.mobile) ? object.mobile : null;
        this.profile = (object.profile) ? object.profile : null;
        this.extension = object.extension || '';
				this.identification = (object.identification) ? object.identification : null;
    }
}

export class TutorialDataClass {
  estado: Estado;
  fechaAlta: Date;
  fechaFinal: Date | null;
}


export interface TutorialDataInterface {
  estado: Estado;
  fechaAlta: Date;
  fechaFinal: Date | null;
}

export interface UserDataGroup {
  firstName: Estado;
  surname: string;
  pictureUser: string;
  idUser: number;
}

export interface AdminHistory {
  uniqueUserIds: number[];
  center: string;
}




