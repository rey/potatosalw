export class UserTeacherCenterModel {
    idUserTeacherCenter: number;
    idUser: number;
    idCenter: number;
    creationDate: Date;
    creationDateString: string;
}
