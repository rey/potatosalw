export class UserCenterModel {
    idUserCenter: number;
    idUser: number;
    idCenter: number;
    creationDate: Date;
    creationDateString: string;
}
