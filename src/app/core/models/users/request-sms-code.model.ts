export interface RequestSMSCode {
    status:    string;
    error_id:  string;
    error_msg: string;
}

export interface VerifySMSCode {
    error: Error;
}

interface Error {
    code: number;
    msg:  string;
    status?: number;
}
