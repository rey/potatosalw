export class UserAuthorModel {
    idUserAuthor: number;
    idUser: number;
    bic: string;
    iban: string;
    creationDate: Date;
    creationDateString: string;
}
