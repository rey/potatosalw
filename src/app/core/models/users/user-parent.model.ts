export class UserParentModel {
    idUserParent: number;
    idUser: number;
    creditCard: string;
    expirationDateCard: string;
    cvv: string;
    creationDate: Date;
    creationDateString: string;
}
