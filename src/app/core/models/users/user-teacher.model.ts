export class UserTeacherModel {
    idUserStudentTeacher: number;
    idUser: number;
    firstName: string;
    surname: string;
    validationDate: Date;
    validationDateString: string;
}
