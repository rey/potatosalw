export class UserSettingsProfileModel {
    idUserSettingProfile: number;
    idUser: number;
    profile: string;
    creationDate: Date;
    creationDateString: string;
}
