export class UserLogModel {
    idUser: number;
    action: string;
    result: string;
    ip: string;
    date: string;
    dateString: string;
}
