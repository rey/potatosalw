export class UserEmployeerModel {
    idUserEnterprise: number;
    idUser: number;
    idEnterprise: number;
    creationDate: Date;
    creationDateString: string;
}
