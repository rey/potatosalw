import { QuizElementTypes } from "./quiz-element-types.enum";

export interface QuizElement {
    idQuizzesDataElements: number;
    data: string;
    elementType: QuizElementTypes;
    idQuiz: number;
    responseCheck: number;
    xPosition: number;
    xSize: number;
    yPosition: number;
    ySize: number;
}
