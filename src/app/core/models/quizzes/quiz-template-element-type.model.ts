export interface QuizTemplateElementType {
    idElementType: number;
    description: string;
}
