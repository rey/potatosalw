import { QuizTemplateElementType } from "./quiz-template-element-type.model";

export interface QuizTemplateElement {
    idTemplateElement: number;
    elementsType: QuizTemplateElementType;
    xPosition: number;
    xSize: number;
    yPosition: number;
    ySize: number;
    idTemplate: number;
    responseCheck?: number | boolean;
    data?: string;
    idTemplateElementQuizz?: number;
    idQuiz?: number;
    style?: string;
    icon?: string;
    file?: File;
}
