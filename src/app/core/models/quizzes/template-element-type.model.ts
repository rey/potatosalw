export interface TemplateElementType {
    name: string,
    type: string,
    description: string,
    icon: string,
    isQuestion: boolean,
    isAnswer: boolean,
    isOption: boolean
}
