export class ElementDataModel {
    idTemplateElementQuizz: number;
    idTemplateElement: number;
    idQuiz: number;
    data: string
    responseCheck: number;
}
