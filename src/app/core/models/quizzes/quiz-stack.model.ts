export interface QuizStack {
    idQuiz: number;
    idQuizOriginal: number;
    idUser: number;
    asked: number;
    timesOk?: number;
    answered: number;
    relevance?: any;
    timesAsked?: number;
    result: number;
    timeCreation: number;
    nextQuestion?: number;
    idTarget: number;
    idCourse: number;
    certifiedQuiz: number;
    role: string;
    answersText: string;
    knowledge?: number;
    answersImage?: string;
    answersDocs?: string;
    answersAudio?: string;
    answersVideo?: string;
    selectedOptions?: string

}
