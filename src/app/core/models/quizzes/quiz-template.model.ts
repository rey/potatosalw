import { QuizTemplateElement } from "./quiz-template-element.model";
import { QuizTemplateInfo } from "./quiz-template-info.model";

export interface QuizTemplate {
    idTemplateQuizz: number;
    idQuiz: number;
    idTemplate: number;
    template: QuizTemplateInfo;
    elements: QuizTemplateElement[];
}
