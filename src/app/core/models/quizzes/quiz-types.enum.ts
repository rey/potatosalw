export enum QuizTypes {
	TEXT = 1,
	SINGLE = 2,
	MULTIPLE = 3,
	IA = 4,
}
