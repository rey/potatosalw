import { QuizTemplateElement } from "./quiz-template-element.model";

export interface QuizTemplateInfo {
    idTemplate: number;
    compuCorrect: number;
    multiplexed: number;
    quizInstructions: string;
    templateSnapshot: string;
    templateTittle: string;
    quizTittle: string;
    writable: number;
    templateElement: QuizTemplateElement[];
}
