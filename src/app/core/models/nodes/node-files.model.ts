export class NodeFilesModel {
    idNodeFile: number;
    videoFile: string;
    videoOperadores: any[];
    pictureFile: string;
    pictureOperadores: any[];
    audioFile: string;
    txtFile: string;
    txtOperadores: any[];
    videoImage: string;
    audioImage: string;
    audioOperadores: any[];
    pdfFile: string;
    pdfOperadores: any[];
    createDate: Date;
    modifyDate: Date;
    videoYouTube: string;
		imageYoutube:string;
    start: string;
    end: string;
		videoInstagram:string;
		imageInstagram:string;
		videoTiktok:string;
		imageTiktok:string;
}
