import { ModelIcono } from "../courses/icono.model";
import { Operator } from "../operators/operator.model";

export interface NodeFilesFormat {
    id: number;
    name: string;
    type: string;
    icon: string;
    background?: string;
    operatorIcons: Operator[];
    edit?: boolean;
    visible: boolean;
    accept?: string;
    fileName?:string;
    progress:number;
    idNodeFile?: number;
    url?:string;
    start?: string;
    end?: string;
		isVideoYoutube?:boolean;
		isVideoInstagram?:boolean;
		isVideoTiktok?:boolean;
}
