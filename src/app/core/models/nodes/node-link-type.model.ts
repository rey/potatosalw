export interface NodeLinkType {
    type: string;
    name: string;
}

export interface NodeAsociarGlobal {
        idNodeAss?: number;
        idNodeDes: number;
        IdNodeTargetDest: number;
        idNodeOri: number;
        idNodeCourseDest: number;
        idUser: number;
        label: string;
        type: string;
        size: number;
        color: string;
        idNode_TargetOrig: number;
        idNode_TargetDest: number;
    }
