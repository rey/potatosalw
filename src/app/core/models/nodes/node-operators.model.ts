export class NodeOperatorsModel {
    idNodo: number;
    idCourse: number;
    typeFile: number;
    idFile: number;
    typeOperator: number;
    nomOperator: string;
    createDate: Date;
    modifyDate: Date;
}
