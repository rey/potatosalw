import { ErrorModel } from './../shared/error.model';
import { CourseModel } from 'src/app/core/models/courses';

export interface CourseResponseModel{
	error: ErrorModel,
	data: CourseModel,
	status: number
}