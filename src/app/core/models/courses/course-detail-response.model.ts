import { CourseEditorsModel } from './course-editors.model';
import { ErrorModel } from './../shared/error.model';
import { CourseModel, CourseTargetModel } from 'src/app/core/models/courses';

export interface CourseDetailResponseModel{
	error: ErrorModel,
	data: {
		courses : CourseModel,
    editores : CourseEditorsModel[],
		coursesTarget : CourseTargetModel[]
	},
	status: number
}
