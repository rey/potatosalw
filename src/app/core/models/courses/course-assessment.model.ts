export class CourseAssessmentModel {
    idValoracion: number;
    idCourse: number;
    fecha: string;
    valoracion: number;
    nivel: number;
    textoValoracion: string;
}
