import { SIGMA_CONSTANTS } from "../../utils/sigma-constants";

export class gSticker {

    id: number;
    x: number;
    y: number;
    nodeType: string;
    size: number;
    type: string;
    url: string;

    constructor(id:number, x:number, y:number, url?:string) {
        this.id = id
        this.url = url || ''
        this.nodeType = SIGMA_CONSTANTS.STICKER_TYPE
        this.type = SIGMA_CONSTANTS.STICKER_NODETYPE
        this.size = SIGMA_CONSTANTS.STICKER_SIZE
        this.x = x
        this.y = y
    }

}
