export interface gEdge {
    id?: string;
    idOriginal?: number;
    source: string; //N1
    target: string; //N2
    connectionType: string;
    label?: string;
    color: string;
    type: string;
    originalType: string;
    size: number;
    delete: boolean;
}