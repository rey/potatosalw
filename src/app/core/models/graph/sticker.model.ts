export interface Sticker {
    id: number;
    url: string;
    name: string
}
