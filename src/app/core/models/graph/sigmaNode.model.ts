export interface SigmaNode {
    id: string;
    idOriginal: number;
    nodeType: string;
    x: number;
    y: number;
    size: number;
    delete?: boolean;
    hidden?: boolean;
    idImageTarget?:number;
    edges?: any[]
    color?:string;
    url?:string;
    label?:string
}
