export interface Operator {
    id: number;
    name: string;
    icon: string;
    type: string;
    description: string;
    order: number;
    checked?: boolean;
}
