export interface YoutubeVideo{
  url: string
  start: string
  end: string
  nameFileVideo: string
  isDelete: number
  idNodeFile:number
	videoImage: string
}

export interface InstagramVideo{
  url: string
  videoImage: string
  isDelete: number
  idNodeFile:number
}

export interface TiktokVideo{
  url: string
  videoImage: string
  isDelete: number
  idNodeFile:number
}
