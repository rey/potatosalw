import { StringMap } from "@angular/compiler/src/compiler_facade_interface"

export class ButtonGraph{
    name:string
    icon:string
    clickFunction: any | null
    disabledIcon:string
    

    constructor(name: string, icon:string, disabledIcon:string, clickFunction: any | null){
        this.name = name
        this.icon = icon
        this.disabledIcon = disabledIcon
        this.clickFunction = clickFunction
    }
}