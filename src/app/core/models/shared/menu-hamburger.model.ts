export interface MenuHamburger{
    id:number
    icon:string
    text:string
    clickFunction: string
    canViewElement?:boolean
    iconActive?:string
    
}