import { ApiError } from "./api-error.model";

export interface ApiResponse<T> {
    error: ApiError;
    data: T;
		status: number
}
