import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalCompetitionGroupsComponent } from './modal-competition-groups.component';

describe('ModalCompetitionGroupsComponent', () => {
  let component: ModalCompetitionGroupsComponent;
  let fixture: ComponentFixture<ModalCompetitionGroupsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCompetitionGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCompetitionGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
