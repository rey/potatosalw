import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeadCompetitionGroupsComponent } from './head-competition-groups.component';

describe('HeadCompetitionGroupsComponent', () => {
  let component: HeadCompetitionGroupsComponent;
  let fixture: ComponentFixture<HeadCompetitionGroupsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadCompetitionGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadCompetitionGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
