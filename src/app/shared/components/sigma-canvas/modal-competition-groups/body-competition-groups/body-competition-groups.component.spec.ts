import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodyCompetitionGroupsComponent } from './body-competition-groups.component';

describe('BodyCompetitionGroupsComponent', () => {
  let component: BodyCompetitionGroupsComponent;
  let fixture: ComponentFixture<BodyCompetitionGroupsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyCompetitionGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyCompetitionGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
