import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalRegistrarNodoComponent } from './modal-registrar-nodo.component';

describe('ModalRegistrarNodoComponent', () => {
  let component: ModalRegistrarNodoComponent;
  let fixture: ComponentFixture<ModalRegistrarNodoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRegistrarNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRegistrarNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
