import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeadNodesComponent } from './head-nodes.component';

describe('HeadNodesComponent', () => {
  let component: HeadNodesComponent;
  let fixture: ComponentFixture<HeadNodesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadNodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadNodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
