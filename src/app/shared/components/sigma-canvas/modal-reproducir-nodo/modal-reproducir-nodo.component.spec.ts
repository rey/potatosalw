import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalReproducirNodoComponent } from './modal-reproducir-nodo.component';

describe('ModalReproducirNodoComponent', () => {
  let component: ModalReproducirNodoComponent;
  let fixture: ComponentFixture<ModalReproducirNodoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReproducirNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReproducirNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
