import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalConectarNodoComponent } from './modal-conectar-nodo.component';

describe('ModalConectarNodoComponent', () => {
  let component: ModalConectarNodoComponent;
  let fixture: ComponentFixture<ModalConectarNodoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConectarNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConectarNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
