import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperatorsTableComponent } from './operators-table.component';

describe('OperatorsTableComponent', () => {
  let component: OperatorsTableComponent;
  let fixture: ComponentFixture<OperatorsTableComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatorsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
