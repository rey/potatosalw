import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperatorIconComponent } from './operator-icon.component';

describe('OperatorIconComponent', () => {
  let component: OperatorIconComponent;
  let fixture: ComponentFixture<OperatorIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatorIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
