import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalOperadoresComponent } from './modal-operadores.component';

describe('ModalOperadoresComponent', () => {
  let component: ModalOperadoresComponent;
  let fixture: ComponentFixture<ModalOperadoresComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalOperadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalOperadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
