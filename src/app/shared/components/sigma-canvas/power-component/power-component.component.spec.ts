import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PowerComponentComponent } from './power-component.component';

describe('PowerComponentComponent', () => {
  let component: PowerComponentComponent;
  let fixture: ComponentFixture<PowerComponentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
