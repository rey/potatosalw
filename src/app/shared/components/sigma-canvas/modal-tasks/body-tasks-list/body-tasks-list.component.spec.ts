import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodyTasksListComponent } from './body-tasks-list.component';

describe('BodyTasksListComponent', () => {
  let component: BodyTasksListComponent;
  let fixture: ComponentFixture<BodyTasksListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyTasksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyTasksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
