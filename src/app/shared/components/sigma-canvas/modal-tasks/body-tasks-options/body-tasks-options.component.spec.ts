import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodyTasksOptionsComponent } from './body-tasks-options.component';

describe('BodyTasksOptionsComponent', () => {
  let component: BodyTasksOptionsComponent;
  let fixture: ComponentFixture<BodyTasksOptionsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyTasksOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyTasksOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
