import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeadTasksComponent } from './head-tasks.component';

describe('HeadTasksComponent', () => {
  let component: HeadTasksComponent;
  let fixture: ComponentFixture<HeadTasksComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
