import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalTasksComponent } from './modal-tasks.component';

describe('ModalTasksComponent', () => {
  let component: ModalTasksComponent;
  let fixture: ComponentFixture<ModalTasksComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
