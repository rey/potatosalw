import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RegistrarNodoComponent } from './registrar-nodo.component';

describe('RegistrarNodoComponent', () => {
  let component: RegistrarNodoComponent;
  let fixture: ComponentFixture<RegistrarNodoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarNodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarNodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
