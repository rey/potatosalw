import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FooterQuizzesComponent } from './footer-quizzes.component';

describe('FooterQuizzesComponent', () => {
  let component: FooterQuizzesComponent;
  let fixture: ComponentFixture<FooterQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
