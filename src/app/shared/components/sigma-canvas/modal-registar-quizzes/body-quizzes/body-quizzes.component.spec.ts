import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodyQuizzesComponent } from './body-quizzes.component';

describe('BodyQuizzesComponent', () => {
  let component: BodyQuizzesComponent;
  let fixture: ComponentFixture<BodyQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
