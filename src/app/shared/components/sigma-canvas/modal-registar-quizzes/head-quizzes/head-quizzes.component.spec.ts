import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeadQuizzesComponent } from './head-quizzes.component';

describe('HeadQuizzesComponent', () => {
  let component: HeadQuizzesComponent;
  let fixture: ComponentFixture<HeadQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
