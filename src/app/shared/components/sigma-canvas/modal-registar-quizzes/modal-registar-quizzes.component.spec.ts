import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalRegistarQuizzesComponent } from './modal-registar-quizzes.component';

describe('ModalRegistarQuizzesComponent', () => {
  let component: ModalRegistarQuizzesComponent;
  let fixture: ComponentFixture<ModalRegistarQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRegistarQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRegistarQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
