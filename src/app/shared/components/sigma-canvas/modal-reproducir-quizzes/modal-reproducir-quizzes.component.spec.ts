import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalReproducirQuizzesComponent } from './modal-reproducir-quizzes.component';

describe('ModalReproducirQuizzesComponent', () => {
  let component: ModalReproducirQuizzesComponent;
  let fixture: ComponentFixture<ModalReproducirQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReproducirQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReproducirQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
