import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HeadReproducirQuizzesComponent } from './head-reproducir-quizzes.component';

describe('HeadReproducirQuizzesComponent', () => {
  let component: HeadReproducirQuizzesComponent;
  let fixture: ComponentFixture<HeadReproducirQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadReproducirQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadReproducirQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
