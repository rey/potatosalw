import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodyReproducirQuizzesComponent } from './body-reproducir-quizzes.component';

describe('BodyReproducirQuizzesComponent', () => {
  let component: BodyReproducirQuizzesComponent;
  let fixture: ComponentFixture<BodyReproducirQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyReproducirQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyReproducirQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
