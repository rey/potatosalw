import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlantillaReproducirQuizComponent } from './plantilla-reproducir-quiz.component';

describe('PlantillaReproducirQuizComponent', () => {
  let component: PlantillaReproducirQuizComponent;
  let fixture: ComponentFixture<PlantillaReproducirQuizComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlantillaReproducirQuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantillaReproducirQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
