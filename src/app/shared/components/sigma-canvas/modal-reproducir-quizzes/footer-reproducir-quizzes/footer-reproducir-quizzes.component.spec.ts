import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FooterReproducirQuizzesComponent } from './footer-reproducir-quizzes.component';

describe('FooterReproducirQuizzesComponent', () => {
  let component: FooterReproducirQuizzesComponent;
  let fixture: ComponentFixture<FooterReproducirQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterReproducirQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterReproducirQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
