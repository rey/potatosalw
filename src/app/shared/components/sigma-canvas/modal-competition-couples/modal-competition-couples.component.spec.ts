import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalCompetitionCouplesComponent } from './modal-competition-couples.component';

describe('ModalCompetitionCouplesComponent', () => {
  let component: ModalCompetitionCouplesComponent;
  let fixture: ComponentFixture<ModalCompetitionCouplesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCompetitionCouplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCompetitionCouplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
