import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodyCompetitionCouplesComponent } from './body-competition-couples.component';

describe('BodyCompetitionCouplesComponent', () => {
  let component: BodyCompetitionCouplesComponent;
  let fixture: ComponentFixture<BodyCompetitionCouplesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyCompetitionCouplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyCompetitionCouplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
