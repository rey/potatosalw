import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeadCompetitionCouplesComponent } from './head-competition-couples.component';

describe('HeadCompetitionCouplesComponent', () => {
  let component: HeadCompetitionCouplesComponent;
  let fixture: ComponentFixture<HeadCompetitionCouplesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadCompetitionCouplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadCompetitionCouplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
