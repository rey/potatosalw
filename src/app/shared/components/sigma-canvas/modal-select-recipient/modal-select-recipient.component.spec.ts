import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalSelectRecipientComponent } from './modal-select-recipient.component';

describe('ModalSelectRecipientComponent', () => {
  let component: ModalSelectRecipientComponent;
  let fixture: ComponentFixture<ModalSelectRecipientComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSelectRecipientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSelectRecipientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
