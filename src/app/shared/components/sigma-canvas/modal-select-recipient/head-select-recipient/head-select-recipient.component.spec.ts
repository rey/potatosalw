import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HeadSelectRecipientComponent } from './head-select-recipient.component';

describe('HeadSelectRecipientComponent', () => {
  let component: HeadSelectRecipientComponent;
  let fixture: ComponentFixture<HeadSelectRecipientComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadSelectRecipientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadSelectRecipientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
