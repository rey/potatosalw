import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BodySelectRecipientComponent } from './body-select-recipient.component';

describe('BodySelectRecipientComponent', () => {
  let component: BodySelectRecipientComponent;
  let fixture: ComponentFixture<BodySelectRecipientComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BodySelectRecipientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodySelectRecipientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
