import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalRadarComponent } from './modal-radar.component';

describe('ModalRadarComponent', () => {
  let component: ModalRadarComponent;
  let fixture: ComponentFixture<ModalRadarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalRadarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRadarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
