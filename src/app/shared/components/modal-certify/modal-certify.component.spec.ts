import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalCertifyComponent } from './modal-certify.component';

describe('ModalCertifyComponent', () => {
  let component: ModalCertifyComponent;
  let fixture: ComponentFixture<ModalCertifyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCertifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCertifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
