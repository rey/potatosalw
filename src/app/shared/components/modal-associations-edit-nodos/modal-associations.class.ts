
export interface ModalListAsso {
    idNodeAss: number;
    color:      string;
    fecCreate: number;
    course:     DataInfoNodes;
    nodeDes:    DataInfoNodes;
    nodeOrg:    DataInfoNodes;
    target:     DataInfoNodes;
    idUser:     number;
    label:      string;
    size:       number;
    type:       string;
}

export interface DataInfoNodes {
    idCourse?:   number;
    title:       string;
    description: string;
    image:       string;
    idNode?:     number;
    idTarget?:   number;
}


