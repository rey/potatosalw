import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalAssociationsEditNodosComponent } from './modal-associations-edit-nodos.component';

describe('ModalAssociationsEditNodosComponent', () => {
  let component: ModalAssociationsEditNodosComponent;
  let fixture: ComponentFixture<ModalAssociationsEditNodosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAssociationsEditNodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAssociationsEditNodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
