import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GraphLateralIconsComponent } from './graph-lateral-icons.component';

describe('GraphLateralIconsComponent', () => {
  let component: GraphLateralIconsComponent;
  let fixture: ComponentFixture<GraphLateralIconsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphLateralIconsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphLateralIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
