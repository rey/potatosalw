import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalCursoDetailComponent } from './modal-curso-detail.component';

describe('ModalCursoDetailComponent', () => {
  let component: ModalCursoDetailComponent;
  let fixture: ComponentFixture<ModalCursoDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCursoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCursoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
