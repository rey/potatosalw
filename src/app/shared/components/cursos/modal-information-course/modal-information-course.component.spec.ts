import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalInformationCourseComponent } from './modal-information-course.component';

describe('ModalInformationCourseComponent', () => {
  let component: ModalInformationCourseComponent;
  let fixture: ComponentFixture<ModalInformationCourseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInformationCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInformationCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
