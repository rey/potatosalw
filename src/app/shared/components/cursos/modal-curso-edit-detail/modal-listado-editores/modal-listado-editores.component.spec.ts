import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalListadoEditoresComponent } from './modal-listado-editores.component';

describe('ModalListadoCursosComponent', () => {
  let component: ModalListadoEditoresComponent;
  let fixture: ComponentFixture<ModalListadoEditoresComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListadoEditoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListadoEditoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
