import { ModalEliminarEditorComponent } from "./modal-eliminar-editor/modal-eliminar-editor.component";
import { ModalListadoEditoresComponent } from "./modal-listado-editores/modal-listado-editores.component";
import { HttpEvent, HttpEventType } from "@angular/common/http";
import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";
import {
	UntypedFormBuilder,
	UntypedFormControl,
	UntypedFormGroup,
	FormGroupDirective,
	NgForm,
	Validators,
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { Router } from "@angular/router";
import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { CourseModel, CourseTargetModel } from "src/app/core/models/courses";
import {
	IdiomaModel,
	MateriaModel,
	NivelModel,
	PaisModel,
} from "src/app/core/models/masters";
import { User } from "src/app/core/models/users/user.models";
import { CoursesService } from "src/app/core/services/courses";
import { GetDataService } from "src/app/core/services/get-data/get-data.service";
import { LoginService } from "src/app/core/services/login";
import { AlertService } from "src/app/core/services/shared";
import { TargetsService } from "src/app/core/services/targets";
import { UsersService } from "src/app/core/services/users";
import { MODAL_DIALOG_TYPES } from "src/app/core/utils/modal-dialog-types";
import { Utils } from "src/app/core/utils/utils";
import { ImagenPipe } from "src/app/shared/pipes/imagen.pipe";
import { ModalAceptarCancelarComponent } from "../../modal";
import { ModalEliminarCursoComponent } from "../../modal/modal-eliminar-curso/modal-eliminar-curso.component";
import { ModalEditarCursoMapaComponent } from "../modal-editar-curso-mapa";
import { ModalInformationCourseComponent } from "../modal-information-course/modal-information-course.component";
import { ToasterService } from "src/app/core/services/shared/toaster.service";

export class ParentGroupValidationStateMatcher implements ErrorStateMatcher {
	isErrorState(
		control: UntypedFormControl | null,
		form: FormGroupDirective | NgForm | null
	): boolean {
		const invalidCtrl = !!(
			control &&
			control.invalid &&
			(control.dirty || control.touched)
		);
		const invalidParent = !!(
			control &&
			(control.dirty || control.touched) &&
			control.parent &&
			control.parent.invalid &&
			control.parent.dirty
		);

		return invalidCtrl || invalidParent;
	}
}

@Component({
	selector: "app-modal-curso-edit-detail",
	templateUrl: "./modal-curso-edit-detail.component.html",
	styleUrls: ["./modal-curso-edit-detail.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class ModalCursoEditDetailComponent implements OnInit {
	@Input() id: string;
	@Input() showModalFormat: boolean = true;

	cursoDatos = {
		title: "",
		description: "",
		subject: "",
		lang1: "",
		level: "",
		image: "",
		user: {
			name: "",
			surname: "",
			image: "",
		},
	};
	user: User;

	buscarNodo: any;
	cargando: boolean;
	isOpenInfoCourse: boolean;

	materias: MateriaModel[] = [];
	niveles: NivelModel[] = [];
	idiomas: IdiomaModel[] = [];
	courseTargets: CourseTargetModel[] = [];
	exclusiveCourseTargets: CourseTargetModel[] = [];
	idImage: string = "";
	saving: boolean = false;

	matcher = new ParentGroupValidationStateMatcher();
	public formulario: UntypedFormGroup;

	public formularioInfo: UntypedFormGroup;

	options: any;
	exclusiveOptions: any;

	curso: CourseModel;
	modoTraerNodo: any;
	modoTraerActividad: any;
	public graphView: string = "gridList";
	progress: number;
	clickSidebar: any;
	levelsOfCountry: any;
	countriesList: PaisModel[];

	langList = [
		{
			nombre: "Español",
			idLang: 38,
		},
		{
			nombre: "Ingles",
			idLang: 36,
		},
		{
			nombre: "Alemán",
			idLang: 31,
		},
		{
			nombre: "Francés",
			idLang: 46,
		},
		{
			nombre: "Italiano",
			idLang: 71,
		},
		{
			nombre: "Portugués",
			idLang: 127,
		},
	];
	notCountryLevelsForThisCountry: boolean = false;

	constructor(
		private utils: Utils,
		public loginService: LoginService,
		private getDataService: GetDataService,
		public translateService: TranslateService,
		public coursesService: CoursesService,
		public targetsService: TargetsService,
		private alertService: AlertService,
		private modalService: NgbModal,
		public activeModal: NgbActiveModal,
		private formBuild: UntypedFormBuilder,
		private imagePipe: ImagenPipe,
		public userService: UsersService,
		public router: Router,
		private toaster: ToasterService
	) {
		this.formularioInfo = this.formBuild.group({
			title: [
				"",
				[
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(100),
				],
			],
			description: [
				"",
				[
					Validators.required,
					Validators.minLength(10),
					Validators.maxLength(500),
				],
			],
			subject: ["", [Validators.required]],
			level: [""],
			lang1: ["", [Validators.required]],
			published: [false],
			idCountry: [""],
			idCountryLevel: [0],
			deckSize: [
				5,
				[Validators.required, Validators.min(5), Validators.max(100)],
			],
		});

		this.createForm();

		this.options = {
			draggable: ".draggable",
			onUpdate: (event: any) => {
				this.saveChanges(event, false);
			},
		}; //Drag&Drop update function

		this.exclusiveOptions = {
			draggable: ".draggable",
			onUpdate: (event: any) => {
				this.saveChanges(event, true);
			},
		}; //Drag&Drop update function
	}

	ngOnInit() {
		this.cargando = true;
		this.user = this.loginService.getUser();
		this.materias = this.getDataService.appSubjects;

		// Obtengo los maestros
		this.obtenerDatosMaestros();
		this.obtenerDatosCurso(this.id);
		this.countriesList = this.getDataService.appCountries;
	}
	createForm() {
		this.formulario = this.formBuild.group({
			image: [{ value: "", disabled: false }],
			title: [
				"",
				[
					Validators.required,
					Validators.minLength(3),
					Validators.maxLength(100),
				],
			],
			description: [
				"",
				[
					Validators.required,
					Validators.minLength(10),
					Validators.maxLength(500),
				],
			],
			subject: ["", [Validators.required]],
			level: [""],
			lang1: ["", [Validators.required]],
			idCountry: [""],
			idCountryLevel: [0],
			published: [false],
			isVisible: [false],
			courseType: [false],
		});
	}

	getUserAvatar(): string {
		return this.utils.getUserAvatar(this.cursoDatos.user.image);
	}

	updateUrl(event: any) {
		event.target.src = "../../assets/images/no-image.png";
	}

	obtenerDatosMaestros() {
		this.idiomas = this.getDataService.appLanguages;
		this.niveles = this.getDataService.appLevels;
	}

	searchLevels(country){
		this.coursesService
			.getCountryLevelsByCountry(country)
			.subscribe((countryLevels) => {
				if (countryLevels.data.length == 0) {
					this.notCountryLevelsForThisCountry = true;
					this.formulario.patchValue({idCountryLevel: 0})
				} else {
					this.notCountryLevelsForThisCountry = false;
					this.formulario.patchValue({ idCountryLevel: countryLevels.data[0].idCountryLevel });
				}
				this.levelsOfCountry = countryLevels.data;
			});
	}

	obtenerDatosCurso(id) {
		this.cargando = true;

		this.coursesService.getCourseById(id).subscribe((response) => {
			this.curso = response.data.courses as CourseModel; // Course detail
			this.coursesService
				.getCountryLevelsByCountry(this.curso.idCountry)
				.subscribe((countryLevels) => {
					this.levelsOfCountry = countryLevels.data;
				});
			this.orderCoursesTargets(response.data.coursesTarget);
			this.formulario.patchValue({
				title: this.curso.courseTittle,
				description: this.curso.description,
				subject: this.curso.subject.idSubject,
				level: this.curso.courseSWLevel,
				lang1: this.curso.language.idLanguage,
				image: this.curso.cpicture
					? this.imagePipe.transform(this.curso.cpicture, "cursos")
					: "",
				published: this.curso.published ? true : false,
				isVisible: this.curso.isVisible === 1 ? true : false,
				courseType: this.curso.courseType === 1 ? true : false,
				idCountry: this.curso.idCountry,
				idCountryLevel: this.curso.countryLevel,
				sonsNumber: this.curso.sonsNumber,
			});
			this.cargando = false;
			this.cursoDatos.user.name = this.curso.user.firstName;
			this.cursoDatos.user.surname = this.curso.user.surname;
			this.cursoDatos.user.image = this.curso.user.pictureUser;
		});
	}
	closeModal(sendData) {
		if (
			this.curso !== undefined &&
			this.formulario.valid &&
			this.changues() == true
		) {
			this.savData();
		}

		this.activeModal.close(sendData);
	}
	close() {
		this.activeModal.close();
	}

	changues(): boolean {
		const v = this.formulario.value;
		const cursoPublished = this.curso.published ? true : false;
		const cursoVisible = this.curso.isVisible ? true : false;
		if (
			this.curso.courseTittle !== v.title ||
			this.curso.description !== v.description ||
			cursoPublished !== v.published ||
			cursoVisible !== v.isVisible ||
			this.curso.idCountry !== v.idCountry ||
			this.curso.countryLevel !== v.idCountryLevel ||
			this.curso.language.idLanguage !== v.lang1 ||
			this.imagePipe.transform(this.curso.cpicture, "cursos") !== v.image
		) {
			return true;
		}
		return false;
	}

	/**
	 * Create/update Course
	 */

	savData(): void {
		let image: File | string = "";
		this.saving = true;
		const v = this.formulario.value;
		let request: CourseModel = new CourseModel(
			v.level,
			v.title,
			v.subject,
			v.description,
			v.lang1,
			null,
			this.curso.user.idUser,
			this.curso.idCourse,
			v.idCountry,
			v.idCountryLevel,
			null,
			this.curso.sonsNumber
		);
		if (this.id !== "nuevo") {
			request.idCourse = this.curso.idCourse;
			//Si el curso no tiene imagen
			if (this.curso.cpicture == "" || this.curso.cpicture == null) {
				if (typeof v.image === "object") image = v.image;
			} else {
				//Si el curso tiene imagen
				if (typeof v.image == "string" && v.image != "")
					request.cpicture = this.curso.cpicture;
				else if (typeof v.image == "object") {
					request.cpicture = this.curso.cpicture;
					image = v.image;
				} else if (typeof v.image == "string" && v.image == "") {
					request.cpicture = null;
				}
			}
		} else {
			image = v.image;
		}

		request.published = v.published ? Date.now() : null;
		request.isVisible = v.isVisible ? 1 : 0;
		request.courseType = v.courseType ? 1 : 2;

		this.coursesService.setCourse(request, image).subscribe(
			(event: HttpEvent<any>) => {
				switch (event.type) {
					case HttpEventType.UploadProgress:
						this.progress = Math.round((event.loaded / event.total) * 100);
						break;
					case HttpEventType.Response:
						setTimeout(() => {
							this.progress = 0;
							if (this.id !== "nuevo")
								this.toaster.success(
									this.translateService.instant("EDITARCURSO.OKSAVE")
								);
							this.saving = false;
							this.obtenerDatosCurso(event.body.data.idCourse);
							this.id = event.body.data.idCourse.toString();
							if (this.clickSidebar) {
								this.clickSidebar = false;
							} else {
								this.activeModal.close(
									this.translateService.instant("EDITARCURSO.OKSAVE")
								);
							}
						}, 500);
				}
			},
			(error) => {
				this.activeModal.close(
					this.translateService.instant("EDITARCURSO.CANCELSAVE")
				);
				this.toaster.info(
					this.translateService.instant("EDITARCURSO.CANCELSAVE")
				);
				this.saving = false;
			}
		);
	}

	// MAPAS
	verDetalleMapa(idMapa: number) {
		const modalRef = this.modalService.open(ModalEditarCursoMapaComponent, {
			scrollable: true,
			windowClass: `${MODAL_DIALOG_TYPES.W80} h-100`,
		});

		modalRef.componentInstance.id = idMapa;
		modalRef.componentInstance.curso = this.curso;

		modalRef.result.then(
			(result) => {
				this.obtenerDatosCurso(this.id);

				if (result && result === "closeAll") {
					this.closeModal("Editar nodos mapa");
				} else if (result) {
					this.targetsService
						.getCourseTargetByIdCourse(this.curso.idCourse)
						.subscribe((result) => {
							this.orderCoursesTargets(result.data);
						});
				}
			},
			(err) => {}
		);
	}

	getImageBackground(image: string | null): string {
		let noImg = "../../assets/images/no-image.png";
		if (!image) return `url('${noImg}')`;
		return `url(${this.imagePipe.transform(image, "mapas")})`;
	}

	nuevoMapa() {
		if (!this.loginService.esAutor()) {
			return;
		}

		const modalRef = this.modalService.open(ModalEditarCursoMapaComponent, {
			scrollable: true,
			windowClass: `${MODAL_DIALOG_TYPES.W80} h-100`,
		});

		modalRef.componentInstance.id = "nuevo";
		modalRef.componentInstance.curso = this.curso;

		modalRef.result.then(
			(result) => {
				if (result) {
					this.targetsService
						.getCourseTargetByIdCourse(this.curso.idCourse)
						.subscribe((result) => {
							this.orderCoursesTargets(result.data);
						});
				}
			},
			(reason) => {}
		);
	}

	openInformationCourse() {
		const modalRef = this.modalService.open(ModalInformationCourseComponent, {
			scrollable: true,
			windowClass: MODAL_DIALOG_TYPES.W30,
		});

		modalRef.componentInstance.curso = this.curso;
		modalRef.componentInstance.editarInfo = true;

		modalRef.result.then(
			(result) => {
				switch (result) {
					case this.formularioInfo:
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHAMODIFICADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Curso borrado":
						// tslint:disable-next-line: max-line-length
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHABORRADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Curso modificado":
						// tslint:disable-next-line: max-line-length
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHAMODIFICADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Abrir mapa":
						// Esta opcion se produce cuando un estudiante ha seleccionado un mapa, hay que cerrar todas las modales
						this.closeModal(result);
						break;
					case "borrarcurso":
						// Esta opcion se produce cuando un autor ha seleccionado editar los nodos, hay que cerrar todas las modales
						this.borrarCurso();
						break;
					case "Editar nodos mapa":
						// Esta opcion se produce cuando un autor ha seleccionado editar los nodos, hay que cerrar todas las modales
						this.closeModal(result);
						break;
				}
				// Refresco el listado
				this.obtenerDatosCurso(this.id);
			},
			(reason) => {}
		);
	}

	// BORRAR CURSO
	borrarCurso() {
		if (!this.loginService.esAutor()) {
			return;
		}

		// Abro un modal preguntando si desea borrar el curso o no
		const modalRef = this.modalService.open(ModalEliminarCursoComponent, {
			scrollable: true,
			windowClass: MODAL_DIALOG_TYPES.W40,
		});

		modalRef.componentInstance.mensaje = this.translateService.instant(
			"EDITARCURSO.DESEABORRARCURSO"
		);

		modalRef.result.then((result: boolean) => {
			// Si devuelve true lo borro y si devuelve false no hago nada

			if (result) {
				this.coursesService.deleteCourse(this.curso.idCourse).subscribe(
					(resp: any) => {
						// Cierro la modal del detalle del curso
						this.activeModal.close("Curso borrado");
						this.toaster.success(
							this.translateService.instant("EDITARCURSO.OKDELETE")
						);
					},
					(error) => {
						this.toaster.error(
							this.translateService.instant("EDITARCURSO.KODELETE")
						);
					}
				);
			}
		});
	}
	// FIN BORRAR CURSO

	/**
	 * Publish or unpublish course
	 * @param $ev Click event in the switch component
	 */
	publish($ev) {
		$ev.preventDefault();
		$ev.stopImmediatePropagation();
		let currentValue: boolean = !this.formulario.get("published").value;
		let modalMessage: string = currentValue
			? this.translateService.instant("EDITARCURSO.PUBLISHMSG")
			: this.translateService.instant("EDITARCURSO.UNPUBLISHMSG");
		let errorMessage: string = currentValue
			? this.translateService.instant("GENERAL.KOPUBLISH")
			: this.translateService.instant("GENERAL.KOUNPUBLISH");
		let okMessage: string = currentValue
			? this.translateService.instant("GENERAL.OKPUBLISH")
			: this.translateService.instant("GENERAL.OKUNPUBLISH");

		//Open modal message alert to confirm the selection

		const modalRef = this.modalService.open(ModalAceptarCancelarComponent, {
			scrollable: true,
			windowClass: MODAL_DIALOG_TYPES.W60,
		});

		modalRef.componentInstance.mensaje = modalMessage;

		modalRef.result.then((result: boolean) => {
			if (result) {
				this.getDataService
					.setPublishType("course", Number(this.id), currentValue)
					.subscribe(
						(result) => {
							if (result.data) {
								this.toaster.success(okMessage);
								this.formulario.get("published").setValue(currentValue); // Update form value
							} else
								this.toaster.error(
									this.translateService.instant("EDITARCURSO.KOPUBLISHCONTENT")
								);
						},
						(err) => {
							this.toaster.error(errorMessage);
						}
					);
			}
		});
	}

	makeVisibleCourse() {
		let currentValue: boolean = !this.formulario.get("isVisible").value;
		let isVisible = currentValue ? 1 : 0;

		this.coursesService
			.makeVisibleCourse(this.curso.idCourse, isVisible)
			.subscribe((result) => {
				if (result.error.code === 0) {
					this.toaster.success(this.translateService.instant(result.error.msg));
					this.formulario.get("isVisible").setValue(currentValue);
				} else {
					this.toaster.error(this.translateService.instant(result.error.msg));
					this.formulario.get("isVisible").setValue(currentValue);
				}
			});
	}

	changeCourseType() {
		let currentValue: boolean = !this.formulario.get("courseType").value;
		let courseType = currentValue ? 1 : 2;

		this.coursesService
			.changeCourseType(this.curso.idCourse, courseType)
			.subscribe((result) => {
				if (result.error.code === 0) {
					this.toaster.success(this.translateService.instant(result.error.msg));
					this.formulario.get("courseType").setValue(currentValue);
				} else {
					this.toaster.error(this.translateService.instant(result.error.msg));
					this.formulario.get("courseType").setValue(currentValue);
				}
			});
	}

	saveChanges(event: any, isExclusive: boolean) {
		let currentOrdinal: number = null;
		const newOrdinal: number = event.newIndex + 1;

		if (isExclusive)
			currentOrdinal =
				this.exclusiveCourseTargets[event.newIndex].ordinalExclusive;
		else currentOrdinal = this.courseTargets[event.newIndex].ordinal;

		this.targetsService
			.updateGraphOrder(
				Number(this.id),
				currentOrdinal,
				newOrdinal,
				isExclusive
			)
			.subscribe(
				(result) => {
					this.orderCoursesTargets(result.data.coursesTarget);
				},
				(err) => {
					this.toaster.error(
						this.translateService.instant("EDITARCURSO.KOMOVEGRAPH")
					);
				}
			);
	}

	listadoEditores() {
		const modalRef = this.modalService.open(ModalListadoEditoresComponent, {
			scrollable: false,
			windowClass: `${MODAL_DIALOG_TYPES.W90} h-100`,
		});
		modalRef.componentInstance.id = this.curso.idCourse;
		modalRef.componentInstance.curso = this.curso;

		modalRef.result.then(
			(result) => {
				this.ngOnInit();
			},
			(err) => {}
		);
	}

	borrarEditor() {
		// Abro un modal preguntando si desea borrar el curso o no
		const modalRef = this.modalService.open(ModalEliminarEditorComponent, {
			scrollable: true,
			windowClass: MODAL_DIALOG_TYPES.W40,
		});

		modalRef.componentInstance.mensaje = this.translateService.instant(
			"EDITARCURSO.DESEABORRAREDITOR"
		);

		modalRef.result.then((result: boolean) => {
			// Si devuelve true lo borro y si devuelve false no hago nada

			if (result) {
				this.coursesService
					.deleteEditor(this.curso.idCourse, this.user.idUser)
					.subscribe(
						(resp: any) => {
							// Cierro la modal del detalle del curso
							this.activeModal.close("Editor borrado");
							this.toaster.success(
								this.translateService.instant("EDITARCURSO.OKDELETEEDITOR")
							);
						},
						(error) => {
							this.toaster.error(
								this.translateService.instant("EDITARCURSO.KODALETEEDITOR")
							);
						}
					);
			}
		});
	}

	private orderCoursesTargets(data: CourseTargetModel[]) {
		this.courseTargets = data.filter((element: CourseTargetModel) => {
			if (this.loginService.esAutor()) {
				return element.target && !element.target.exclusive;
			} else {
				return element.target.published !== null && !element.target.exclusive;
			}
		});

		this.exclusiveCourseTargets = data.filter(
			(element: CourseTargetModel, index) => {
				return element.target.exclusive;
			}
		);
	}
}
