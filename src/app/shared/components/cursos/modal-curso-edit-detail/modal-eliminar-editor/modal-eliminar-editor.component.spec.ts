import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalEliminarEditorComponent } from './modal-eliminar-editor.component';

describe('ModalEliminarCursoComponent', () => {
  let component: ModalEliminarEditorComponent;
  let fixture: ComponentFixture<ModalEliminarEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEliminarEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEliminarEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
