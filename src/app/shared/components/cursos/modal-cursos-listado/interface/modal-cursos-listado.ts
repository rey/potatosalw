import { ErrorModel } from "src/app/core/models/shared/error.model";

export interface CourseListModel {
    idCourse:    number;
    idTarget:    number;
    idNode:      number;
    idQuiz:      number;
    name:        string;
    icon:        string;
    type:        string;
    hasChildren: boolean;
    children:    CourseListModel[];
    multi:       boolean;
		color:			 string;
		sizeQuiz:    string;
		exclusive:	 boolean;
		hasResponse: boolean;
		responses:	 number[];
		quizObj:		 any;
		isSelected?: boolean;
}

export enum NEWELEMENTTYPES{
    COURSE = "COURSE",
    GRAPH = "GRAPH",
    NODE = "NODE",
    QUIZ = "QUIZ"
}

export interface ResponseChallengeModel{
	error: ErrorModel,
	data: {
		nameSession:		string;
		courseslist: 		CourseListModel[];
	},
	status: number
}
