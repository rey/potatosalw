import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MatTreeCursosComponent } from './mat-tree-cursos.component';

describe('MatTreeCursosComponent', () => {
  let component: MatTreeCursosComponent;
  let fixture: ComponentFixture<MatTreeCursosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MatTreeCursosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatTreeCursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
