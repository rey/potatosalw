import { Profiles } from 'src/app/core/utils/profiles.enum';
import { CourseListModel, NEWELEMENTTYPES } from './../interface/modal-cursos-listado';
import { Component, Input, OnInit, SimpleChanges, ViewEncapsulation, OnChanges, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { CoursesService } from 'src/app/core/services/courses';
import { LoginService } from 'src/app/core/services/login';
import { UtilsService } from 'src/app/core/services/shared/utils.service';
import { QuizzesstackService } from 'src/app/core/services/quizzesstack/quizzesstack.service';
import { LocalStorage } from 'src/app/core/utils';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
declare var $
@Component({
  selector: 'app-mat-tree-cursos',
  templateUrl: './mat-tree-cursos.component.html',
  styleUrls: ['./mat-tree-cursos.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MatTreeCursosComponent  implements OnChanges {
    @Input() idCourse:number = null // Input con el id del curso selecciona desde el pad del grafo; nulo por defecto si no seleccionamos ninguno
    @Input() treeCourses:CourseListModel[] = []
		@Input() treeGraphs: CourseListModel[] = []
		@Input() optionSelected: number; //Numero de la opción seleccionada para el filtrado de cursos y elementos
		@Input() idGroup: number
		@Input() type: string
		@Input() lastQuizStack: any
		@Input() fromNode?: boolean
		@Input() idCurso?: number
		@Input() idGrafo?: number
		@Input() idNodo?: number

    @Output() clickElement = new EventEmitter<CourseListModel>();
    @Output() createNewElement = new EventEmitter<NEWELEMENTTYPES>();
    isAuthor:boolean = this.loginService.esAutor() || false
		@ViewChild('scrollableParent',{static: true}) private scrollableParent: ElementRef;
		listListQuizzesStack = [];
		hasResponsesArray = [];
		elementFromNode: CourseListModel[] = []

    constructor(public coursesService: CoursesService, public loginService: LoginService, public utilsService: UtilsService, public quizzesStackService: QuizzesstackService, private localStorage: LocalStorage,) {}

		ngOnInit(): void {
			//Called after the constructor, initializing input properties, and the first call to ngOnChanges.
			//Add 'implements OnInit' to the class.
			this.treeCourses.forEach(course => {
				this.hasResponsesArray.push(false)
			})
		}

    ngOnChanges(changes: SimpleChanges): void {
			let currentCourse: CourseListModel = null
			setTimeout(() => {
				this.scrollableParent.nativeElement.scrollTop =  Math.trunc(this.utilsService.scrollMeasureTreeActivities);
			}, 300);

			if(changes.treeCourses){
					if(this.idCourse){
							//Obtengo del array el elemento con el id del curso y llamo para obtener sus hijos
							const courses: CourseListModel[] =  changes.treeCourses.currentValue as CourseListModel[]
							courses.forEach(course => {
									if(course.idCourse === this.idCourse)
											currentCourse = course
							})
							this.loadMoreContent(currentCourse,this.idCourse)
					}
			}

			this.fillStack();
			//SI EL DESAFIO SE ABRE DESDE UN NODO
			if(this.fromNode && this.treeCourses.length > 0){
				this.loadContentFromNode();
			}
    }

		ngDoCheck(): void {
			this.fillStack()
		}

		fillStack(){
			this.listListQuizzesStack = this.quizzesStackService.responseListQuizzesStackChallenges;
			if(this.listListQuizzesStack.length > 0){ //Si hay elementos en el array, es que hay respuestas
				this.listListQuizzesStack.forEach((listQuizzesStack, ind) => {
						this.treeCourses.forEach((element, index) => {
								if(element.idQuiz === listQuizzesStack.idQuiz) {
										element.hasResponse = true;
								}
						});
						this.hasResponsesArray[ind] = true;
				});
			}
		}

    loadMoreContent(element:CourseListModel, currentId: number): void{
        //Hacemos llamada para recuperar y cargar los elementos
         //const rol: string = this.loginService.esAutor() ? Profiles.Editor.toLowerCase() : Profiles.Student.toLowerCase()
		const datos = JSON.parse(this.localStorage.getItem(LOCALSTORAGESTRINGS.USER));
		let rol: string = datos.profile.toLowerCase() === "autor" ? "editor" : datos.profile.toLowerCase();
        if($(`#${currentId}`).hasClass('open')){
            $(`#${currentId}`).removeClass('open')
            $(`#${currentId} > ul`).removeClass('openList').addClass('hide')
        }
        else{
            setTimeout(() => {
                $(`#${currentId}`).addClass('open')
                $(`#${currentId} > ul`).addClass('openList').removeClass('hide')
            }, 0);

        }
        //Si ya tiene contenido, no volvemos a hacer la llamada; simplemente desplegamos el contenido
        if(!element.children.length){
					if(this.optionSelected){
						this.coursesService.newChildrensListMode(this.optionSelected,this.idGroup,element.idCourse,element.idTarget,element.idNode).subscribe(result => {
							element.children = result
						})
					}
					else
            this.coursesService.childrensListMode(element.idCourse, element.idTarget,element.idNode, rol).subscribe(result => {
                element.children = result
             })
        }
    }

		loadContentFromNode(){
			//PRIMERO ABRIMOS EL CURSO
			for (let index = 0; index < this.treeCourses.length; index++) {
				if(this.treeCourses[index].idCourse === this.idCurso){
					this.loadMoreContent(this.treeCourses[index], this.idCurso);
					this.coursesService.newChildrensListMode(this.optionSelected,this.idGroup,this.treeCourses[index].idCourse,this.treeCourses[index].idTarget,this.treeCourses[index].idNode).subscribe(result => {
						this.treeCourses[index].children = result;
						//AHORA ABRIMOS EL GRAFO
						for (let index2 = 0; index2 < this.treeCourses[index].children.length; index2++) {
							if(this.treeCourses[index].children[index2].idTarget === this.idGrafo){
								this.loadMoreContent(this.treeCourses[index].children[index2], this.idGrafo);
								this.coursesService.newChildrensListMode(this.optionSelected,this.idGroup,this.treeCourses[index].idCourse,this.treeCourses[index].children[index2].idTarget,this.treeCourses[index].idNode).subscribe(result => {
									this.treeCourses[index].children[index2].children = result;
									//AHORA ABRIMOS EL NODO
									for (let index3 = 0; index3 < this.treeCourses[index].children[index2].children.length; index3++) {
										if(this.treeCourses[index].children[index2].children[index3].idNode === this.idNodo){
											this.loadMoreContent(this.treeCourses[index].children[index2].children[index3], this.idNodo);
											break;
										}
									}
								})
								break;
							}
						}
					})
					break;
				}
			}
		}

    goTo(element: CourseListModel): void{
			//Envio el evento al padre, para mostrar en el lateral unas cosas u otras
			this.utilsService.scrollMeasureTreeActivities = this.scrollableParent.nativeElement.scrollTop
			this.clickElement.emit(element)
    }

    newElement(type: NEWELEMENTTYPES){
        this.createNewElement.emit(type)
    }

		setSelectedQuiz(e,i){
			this.treeGraphs[0].children[e].children.forEach(item => {
				item.isSelected = false;
			})
			this.treeGraphs[0].children[e].children[i].hasResponse = true;
			this.treeGraphs[0].children[e].children[i].isSelected = true;

		}



}
