import { NodeService } from "./../../../../core/services/node/node.service";
import { CourseListModel } from "./interface/modal-cursos-listado";
import { MatomoAnalyticsUtils } from "./../../../../core/utils/matomo-analytics.utils";
import { CourseTargetModel } from "./../../../../core/models/courses/course-target.model";
import { TargetsService } from "./../../../../core/services/targets/targets.service";
import { LOCALSTORAGESTRINGS } from "src/app/core/models/masters/localstorage.enum";
import { UsersService } from "../../../../core/services/users/users.service";
import { AuthorModel } from "../../../../core/models/masters/author-response.model";
import { Component, HostListener, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";

import { NgbModal, NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

// Models
import { FilterCourseModel, CourseModel } from "src/app/core/models/courses";

// Services
import { TranslateService } from "@ngx-translate/core";
import { CoursesService } from "src/app/core/services/courses";
import { LoginService } from "src/app/core/services/login";
import { AlertService } from "src/app/core/services/shared";

import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from "@angular/forms";
import { startWith, finalize, takeUntil, take, debounceTime  } from "rxjs/operators";
import { SliceStringPipe } from "src/app/shared/pipes/slice-string.pipe";
import { Router } from "@angular/router";

import { ModalCursoDetailComponent } from "../modal-curso-detail/modal-curso-detail.component";
import { ModalCursoEditDetailComponent } from "../modal-curso-edit-detail/modal-curso-edit-detail.component";
import { QuizzesService } from "src/app/core/services/quizzes";
import { RecordarQuizPlayComponent } from "../../quiz-open/quiz-play/quiz-play.component";
import { RecordarQuizPlayMultipleComponent } from "../../quiz-open/quiz-play-multiple/quiz-play-multiple.component";
import { MODAL_DIALOG_TYPES } from "src/app/core/utils/modal-dialog-types";
import { Observable, Subject } from "rxjs";
import { map } from "rxjs/operators";
import { DEFAULTCLOSEPADS } from "src/app/core/models/masters/masters.enum";
import { Utils } from "src/app/core/utils/utils";
import { ModalCelebrationComponent } from "../../modal-celebration/modal-celebration.component";
import { environment } from "src/environments/environment";
import { SigmaToolbarsService } from "src/app/core/services/sigma-toolbars/sigma-toolbars.service";
import { User } from "src/app/core/models/users/user.models";
import { Profiles } from "src/app/core/utils/profiles.enum";
import { MastersService } from "src/app/core/services/masters";
import { ToasterService } from "src/app/core/services/shared/toaster.service";
import { GetDataService } from "src/app/core/services/get-data/get-data.service";
import { MateriaModel, PaisModel } from "src/app/core/models/masters";
import { NbPopoverDirective } from "@nebular/theme";
import { LocalStorage } from "src/app/core/utils";

const USERTYPE = {
	STUDENT: "estudiante",
	EDITOR: "editor",
	TEACHER: "profesor",
};

const ELEMENTTYPE = {
	COURSE: "COURSE",
	GRAPH: "GRAPH",
	NODE: "NODE",
	QUIZ: "QUIZ",
};

const URLCELEBRATIONS: string = environment.celebrations;
@Component({
	selector: "app-modal-cursos-listado",
	templateUrl: "./modal-cursos-listado.component.html",
	styleUrls: ["./modal-cursos-listado.component.scss"],
	providers: [SliceStringPipe],
	encapsulation: ViewEncapsulation.None,
})
export class ModalCursosListadoComponent implements OnInit {
	@ViewChild(NbPopoverDirective) popover: NbPopoverDirective;
	viewList: boolean = false;
	formulario: UntypedFormGroup;
	viewType = { nodes: false, quizzes: true };
	isLoading: boolean = true;
	filtro: FilterCourseModel = new FilterCourseModel();
	cargando = false;
	authorControl: UntypedFormControl = new UntypedFormControl("");
	cursos: CourseModel[] = [];
	isMyCourses: UntypedFormControl = new UntypedFormControl(true);
	filteredOptions: Observable<AuthorModel[]>;
	authors: AuthorModel[] = [];
	options: any;
	opcion: string;
	modoTraerNodo: any;
	modoTraerActividad: any;
	actividadesFlash = false;
	ModoAuto: any;
	ordenSeleccionado: number[] = [0];
	buscarNodo = false;
	courseTargets: CourseTargetModel[] = [];
	curso: CourseModel;
	treeCourses: CourseListModel[] = [];
	mouseInterval: any = "";
	isShowFiles: boolean = true;
	isShowPads: boolean = false;
	isShowLabels: boolean = false;
	viewQuiz: boolean = false;
	idCourse: number = null;
	idSelectedCourse: number = null;
	idGraph: number = null;
	quiz: any = null;
	elements: any[] = [];
	answered: boolean = false;

	saveClicked: Subject<any> = new Subject<any>();
	saveClicked$: Observable<any> = this.saveClicked.asObservable();

	node: any = null;
	user: User = null;
	private destroy$ = new Subject();
	profileEditor: boolean;
	profileStudent: boolean;
	profileTeacher: boolean;

	isStudent: boolean;
	idGroup: number;
	rol: string;
	showElement: boolean = false;
	buttonText: string = 'Filtros';
	isMobile: boolean = false;

	langList = [
		{
			nombre: "Todos",
			idLang: null,
		},
		{
			nombre: "Español",
			idLang: 38,
		},
		{
			nombre: "Ingles",
			idLang: 36,
		},
		{
			nombre: "Alemán",
			idLang: 31,
		},
		{
			nombre: "Francés",
			idLang: 46,
		},
		{
			nombre: "Italiano",
			idLang: 71,
		},
		{
			nombre: "Portugués",
			idLang: 127,
		},
	];

	orderList = [
		{
			nombre: "A - Z",
			idOrder: 1,
		},
		{
			nombre: "Z - A",
			idOrder: 2,
		},
		{
			nombre: "Mas recientes",
			idOrder: 3,
		},
		{
			nombre: "Mas Antiguos",
			idOrder: 4,
		},
		{
			nombre: "Editados Recientemente",
			idOrder: 5,
		},
	];

	filtersList = [
		{
			nombre: "idioma",
			id: 1,
			active: false,
			value: null,
		},
		{
			nombre: "asignatura",
			id: 2,
			active: false,
			value: null,
		},
		{
			nombre: "pais",
			id: 3,
			active: false,
			value: null,
		},
		{
			nombre: "nivel",
			id: 4,
			active: false,
			value: null,
		},
	];

	subjectsList: MateriaModel[] = this.getDataService.appSubjects;
	countriesList: PaisModel[] = this.getDataService.appCountries;
	auxCursos: CourseModel[];
	filteredCursos: CourseModel[] = [];
	notCountryLevelsForThisCountry: boolean = false;
	levelsOfCountry: any;
	filterCountrySelected: boolean = false;
	selectedLang = null;
	selectedSubject = null;
	selectedCountry = null;
	selectedLevelCountry = null;
	selectedOrder = 1;
	constructor(
		public translateService: TranslateService,
		public coursesService: CoursesService,
		public loginService: LoginService,
		private alertService: AlertService,
		private modalService: NgbModal,
		public activeModal: NgbActiveModal,
		public router: Router,
		public userService: UsersService,
		private formBuild: UntypedFormBuilder,
		private quizService: QuizzesService,
		private ma: MatomoAnalyticsUtils,
		public targetsService: TargetsService,
		private utils: Utils,
		private nodeService: NodeService,
		private st: SigmaToolbarsService,
		private MastersService: MastersService,
		private toaster: ToasterService,
		public getDataService: GetDataService,
		private localStorage: LocalStorage
	) {
		this.formulario = this.formBuild.group({ filtrado: [""] });
		this.setupSearchListener();
	}

	ngOnInit(): void {
		switch (this.loginService.getProfile()) {
			case "ESTUDIANTE":
				this.rol = USERTYPE.STUDENT;
				localStorage.removeItem("selectedGroupInfo");
				break;
			case "PROFESOR":
				this.rol = USERTYPE.TEACHER;
				break;
			case "AUTOR":
				this.rol = USERTYPE.EDITOR;
				break;
			default:
				break;
		}
		this.isStudent =
			this.loginService.getProfile() === Profiles.Student ? true : false;

		this.userService
			.getListUserProfile()
			.pipe(takeUntil(this.destroy$), take(1))
			.subscribe((res: any) => {
				res.data.forEach((element) => {
					if (element.idProfile == 1) {
						this.profileEditor = true;
					}
					if (element.idProfile == 2) {
						this.profileStudent = true;
					}
					if (element.idProfile == 3) {
						this.profileTeacher = true;
					}
				});
			});
		this.isMyCourses.patchValue(this.loginService.esAutor());
		if(this.actividadesFlash){
			this.cursosActividadesFlash();
		} else if(!this.actividadesFlash && this.viewList){
			this.getDataToTree();
		} else if(!this.actividadesFlash && !this.viewList){
			this.filtrarCursos();
		}

		this.isMobile = window.innerWidth < 1024;
	}

	ngOnDestroy() {
		this.destroy$.next(true);
	}

	setupSearchListener() {
		this.formulario
			.get("filtrado")
			.valueChanges.pipe(debounceTime(500))
			.subscribe(() => {
				this.filtrarCursosLupa();
			});
	}



	applyFilters(value, filter) {
		switch (filter) {
			case 1:
				this.filtersList[0].value = value;
				break;
			case 2:
				this.filtersList[1].value = value;
				break;
			case 3:
				this.filtersList[2].value = value;
				break;
			case 4:
				this.filtersList[3].value = value;
				break;
			default:
				console.log("El número no coincide con ningún caso");
				break;
		}

		this.filterCourses();
	}

	filterCourses() {
		this.cursos = this.auxCursos.filter((curso) => {
			return (
				(!this.filtersList[0].value ||
					curso.language.idLanguage === this.filtersList[0].value) &&
				(!this.filtersList[1].value ||
					curso.subject.idSubject === this.filtersList[1].value) &&
				(!this.filtersList[2].value ||
					curso.idCountry === this.filtersList[2].value) &&
				(!this.filtersList[3].value ||
					curso.countryLevel === this.filtersList[3].value)
			);
		});
	}

	changeOrder(order) {
		switch (order) {
			case 1:
				this.cursos.sort((a, b) =>
					a.courseTittle.localeCompare(b.courseTittle)
				);
				break;
			case 2:
				this.cursos = this.cursos.sort((a, b) =>
					b.courseTittle.localeCompare(a.courseTittle)
				);
				break;
			case 3:
				this.cursos = this.cursos.sort(
					(a, b) =>
						new Date(b.creationDate).getTime() -
						new Date(a.creationDate).getTime()
				);
				break;
			case 4:
				this.cursos = this.cursos.sort(
					(a, b) =>
						new Date(a.creationDate).getTime() -
						new Date(b.creationDate).getTime()
				);
				break;
			case 5:
				this.cursos = this.cursos.sort(
					(a, b) =>
						new Date(b.editDate).getTime() - new Date(a.editDate).getTime()
				);
				break;
			default:
				console.log("El número no coincide con ningún caso");
				break;
		}
	}

	clearFilters() {
		this.selectedLang = null;
		this.selectedSubject = null;
		this.selectedCountry = null;
		this.selectedLevelCountry = null;
		this.cursos = this.auxCursos;
	}

	clearFiltersList(filter){
		switch (filter) {
			case 1:
				this.filtersList[0].value = null;
				this.selectedLang = null;
				break;
			case 2:
				this.filtersList[1].value = null;
				this.selectedSubject = null;
				break;
			case 3:
				this.filtersList[2].value = null;
				this.selectedCountry = null;
				break;
			case 4:
				this.filtersList[3].value = null;
				this.selectedLevelCountry = null;
				break;
			default:
				console.log("El número no coincide con ningún caso");
				break;
		}
		this.filterCourses();
	}

	searchLevels(country) {
		this.coursesService
			.getCountryLevelsByCountry(country)
			.subscribe((countryLevels) => {
				this.filterCountrySelected = true;
				if (countryLevels.data.length == 0) {
					this.notCountryLevelsForThisCountry = true;
					this.formulario.patchValue({ idCountryLevel: 0 });
				} else {
					this.notCountryLevelsForThisCountry = false;
					this.formulario.patchValue({
						idCountryLevel: countryLevels.data[0].idCountryLevel,
					});
				}
				this.levelsOfCountry = countryLevels.data;
			});
	}

	openFiltersPanel() {
		if (this.popover.isShown) {
			this.popover.hide();
		} else {
			this.popover.show();
		}
	}

	obtenerDatosMaestros() {
		this.filteredOptions = this.authorControl.valueChanges.pipe(
			startWith(""),
			map((value) => (typeof value === "string" ? value : value.name)),
			map((name) => this._filter(name))
		);
	}
	private _filter(name: string): AuthorModel[] {
		if (name) {
			this.isMyCourses.setValue(false);
			this.isMyCourses.disable();
			return this.authors.filter(
				(author) => author.name.toLowerCase().indexOf(name.toLowerCase()) === 0
			);
		} else {
			this.isMyCourses.enable();
			return this.authors.slice();
		}
	}

	filtrarCursos() {
		this.cargando = true;
		const filtradoValue = this.formulario.value;

		if (this.modoTraerActividad || this.modoTraerNodo) {
			this.coursesService
				.filtradoCursosNew(filtradoValue.filtrado, false, this.rol)
				.subscribe(
					(res) => {
						this.cursos = res;
						this.auxCursos = this.cursos;
						this.cargando = false;
					},
					(err) => {
						console.log(err);
						this.cargando = false;
					}
				);
		} else {
			if (this.loginService.esAutor()) {
				this.coursesService
					.filtradoCursosNew(filtradoValue.filtrado, true, this.rol)
					.subscribe(
						(res) => {
							this.cursos = res;
							this.auxCursos = this.cursos;
							this.cargando = false;
						},
						(err) => {
							console.log(err);
							this.cargando = false;
						}
					);
			} else {
				//revisar el form y el check y enviarlo
				const myCourses = this.isMyCourses.value;
				this.coursesService
					.filtradoCursosNew(filtradoValue.filtrado, myCourses, this.rol)
					.subscribe(
						(res) => {
							this.cursos = res;
							this.auxCursos = this.cursos;
							this.cargando = false;
							for (let index = 0; index < this.cursos.length; index++) {
								let element = this.cursos[index];
								element.averageKnowledge = element.averageKnowledge.toFixed(1);
							}
						},
						(err) => {
							console.log(err);
							this.cargando = false;
						}
					);
			}
		}
	}

	cursosActividadesFlash() {
		this.cargando = true;
		const filtradoValue = this.formulario.value;
		if (this.modoTraerActividad || this.modoTraerNodo) {
			this.coursesService
				.getCoursesListResolved()
				.subscribe(
					(res) => {
						this.cursos = res.data;
						this.cargando = false;
					},
					(err) => {
						console.log(err);
						this.cargando = false;
					}
				);
		} else {
			if (this.loginService.esAutor()) {
				this.coursesService
					.getCoursesListResolved()
					.subscribe(
						(res) => {
							this.cursos = res.data;
							this.cargando = false;
						},
						(err) => {
							console.log(err);
							this.cargando = false;
						}
					);
			} else {
				//revisar el form y el check y enviarlo
				const myCourses = this.isMyCourses.value;
				this.coursesService
					.getCoursesListResolved()
					.subscribe(
						(res) => {
							this.cursos = res.data;
							this.cargando = false;
							for (let index = 0; index < this.cursos.length; index++) {
								let element = this.cursos[index];
								element.averageKnowledge = element.averageKnowledge.toFixed(1);
							}
						},
						(err) => {
							console.log(err);
							this.cargando = false;
						}
					);
			}
		}
	}

	verDetalleCurso(idCurso) {
		this.popover.hide();
		if (this.actividadesFlash){
			this.openCursoEstudiante(idCurso);
		} else{
			this.opcion === "editar"
				? this.openCursoEditor(idCurso)
				: this.openCursoEstudiante(idCurso);
		}
	}

	abrirListadoQuizes(idCurso) {
		this.coursesService.recordarQuizesListado(idCurso).subscribe((res: any) => {
			if (res.data.length > 0) {
				if (res.data.length < 20) {
					this.toaster.success(this.translateService.instant("CURSOS.ERROR1"));
				} else {
					this.ModoAuto = res.data;
					let indice = 0;
					this.openModalModeAuto(indice);
				}
			} else
				this.toaster.success(this.translateService.instant("CURSOS.ERROR1"));
		});
	}

	openModalModeAuto(indice) {
		if (indice < this.ModoAuto.length - 1) {
			if (
				this.ModoAuto[indice].idQuiz !== this.ModoAuto[indice].idQuizOriginal
			) {
				this.quizService
					.getQuizMultipleCanvasQuizMultiple(
						this.ModoAuto[indice].idQuiz,
						this.ModoAuto[indice].idCourse,
						this.ModoAuto[indice].idTarget
					)
					.pipe(finalize(() => (this.isLoading = false)))
					.subscribe((res: any) => {
						const modalRef = this.modalService.open(
							RecordarQuizPlayMultipleComponent,
							{
								scrollable: true,
								windowClass: MODAL_DIALOG_TYPES.W100,
								backdrop: "static",
							}
						);
						modalRef.componentInstance.quiz = {
							...res.quiz,
							user: res.quiz.user,
							idOriginal: res.quiz.idOriginal,
							id: res.quiz.idQuiz,
							originalX: res.quiz.originalX,
							originalY: res.quiz.originalY,
							size: res.quiz.size,
							sizeQuiz: res.quiz.sizeQuiz,
							x: res.quiz.x,
							y: res.quiz.y,
						};
						modalRef.componentInstance.elements = res.elements;
						modalRef.componentInstance.listQuiz = this.ModoAuto;

						modalRef.result.then(
							(res) => {
								if (res < this.ModoAuto.length - 1) {
									this.openModalModeAuto(res);
								} else
									this.toaster.success(
										this.translateService.instant("CURSOS.OKALLQUIZZES")
									);
							},
							(err) => {
								this.toaster.success(
									this.translateService.instant("CURSOS.OKALLQUIZZES")
								);
							}
						);
					});
			} else {
				this.quizService
					.getQuiz(
						this.ModoAuto[indice].idQuiz,
						this.ModoAuto[indice].idCourse,
						this.ModoAuto[indice].idTarget
					)
					.pipe(finalize(() => (this.isLoading = false)))
					.subscribe((res: any) => {
						const modalRef = this.modalService.open(RecordarQuizPlayComponent, {
							scrollable: true,
							windowClass: MODAL_DIALOG_TYPES.W100,
							backdrop: "static",
						});
						modalRef.componentInstance.quiz = {
							...res.quiz,
							user: res.quiz.user,
							idOriginal: res.quiz.idOriginal,
							id: res.quiz.idQuiz,
							originalX: res.quiz.originalX,
							originalY: res.quiz.originalY,
							size: res.quiz.size,
							sizeQuiz: res.quiz.sizeQuiz,
							x: res.quiz.x,
							y: res.quiz.y,
						};
						modalRef.componentInstance.elements = res.elements;
						modalRef.componentInstance.listQuiz = this.ModoAuto;

						modalRef.result.then(
							(res) => {
								if (res < this.ModoAuto.length - 1) {
									this.openModalModeAuto(res);
								} else {
									this.toaster.success(
										this.translateService.instant("CURSOS.OKALLQUIZZES")
									);
								}
							},
							(err) => {
								this.toaster.success(
									this.translateService.instant("CURSOS.OKALLQUIZZES")
								);
							}
						);
					});
			}
		} else
			this.toaster.success(
				this.translateService.instant("CURSOS.OKALLQUIZZES")
			);
	}

	openCursoEstudiante(idCurso: any) {
		this.ma.event("click", "view_item", "Curso");
		const modalRef = this.modalService.open(ModalCursoDetailComponent, {
			scrollable: false,
			windowClass: `${MODAL_DIALOG_TYPES.W95} h-100`,
		});

		modalRef.componentInstance.id = idCurso;
		modalRef.componentInstance.modoTraerNodo = this.modoTraerNodo;
		modalRef.componentInstance.modoTraerActividad = this.modoTraerActividad;
		modalRef.componentInstance.profileTeacher = this.profileTeacher;
		modalRef.componentInstance.idGroup = this.idGroup;
		modalRef.componentInstance.actividadesFlash = this.actividadesFlash;

		modalRef.result.then(
			(result) => {
				switch (result) {
					case "Curso borrado":
						// tslint:disable-next-line: max-line-length
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHABORRADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Curso modificado":
						// tslint:disable-next-line: max-line-length
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHAMODIFICADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Abrir mapa":
						// Esta opcion se produce cuando un estudiante ha seleccionado un mapa, hay que cerrar todas las modales
						this.closeModal(result);
						break;
					case "Editar nodos mapa":
						// Esta opcion se produce cuando un autor ha seleccionado editar los nodos, hay que cerrar todas las modales
						this.closeModal(result);
						break;
					case "Ver como lista":
						// Esta opcion se produce cuando un estudiante ha seleccionado ver como lista desde la pantalla de grafos
						this.viewList = true;
						this.getDataToTree();
						break;
				}
				// Refresco el listado
				if(this.actividadesFlash){
					this.cursosActividadesFlash();
				} else if(!this.actividadesFlash && this.viewList){
					this.getDataToTree();
				} else if(!this.actividadesFlash && !this.viewList){
					this.filtrarCursos();
				}
			},
			(reason) => {}
		);
	}

	closeModal(result: any) {
		this.popover.hide();
		this.activeModal.close(result);
		this.st.changeUpdateGraph(true);
	}

	close() {
		this.activeModal.close();
		this.st.changeUpdateGraph(true);
	}

	openCursoEditor(idCurso: number) {
		const modalRef = this.modalService.open(ModalCursoEditDetailComponent, {
			scrollable: true,
			windowClass: MODAL_DIALOG_TYPES.W95,
		});

		modalRef.componentInstance.id = idCurso;
		modalRef.result.then(
			(result) => {
				switch (result) {
					case "Curso borrado":
						// tslint:disable-next-line: max-line-length
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHABORRADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Curso modificado":
						// tslint:disable-next-line: max-line-length
						this.alertService.success(
							this.translateService.instant(
								"CURSOS.ELCURSOSEHAMODIFICADOCORRECTAMENTE"
							),
							AlertService.AlertServiceContextValues.ModalCurso
						);
						break;
					case "Abrir mapa":
						// Esta opcion se produce cuando un estudiante ha seleccionado un mapa, hay que cerrar todas las modales
						this.closeModal(result);
						break;
					case "Editar nodos mapa":
						// Esta opcion se produce cuando un autor ha seleccionado editar los nodos, hay que cerrar todas las modales
						this.closeModal(result);
						break;
				}

				// Refresco el listado
				this.filtrarCursos();
			},
			(reason) => {}
		);
	}

	updateUrl(event: any) {
		event.target.src = "../../assets/images/no-image.png";
	}

	nuevoCurso() {
		//const modalRef = this.modalService.open(ModalEditarCursoComponent, { scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W100} ${MODAL_DIALOG_TYPES.w100NEW}` });
		//modalRef.componentInstance.clickSidebar = true;
		//modalRef.componentInstance.id = 'nuevo';
		//modalRef.result.then((result) => { this.filtrarCursos(); }, (reason) => { });

		//localStorage.setItem('dontLoadMenu', 'true')
		this.utils.loadMenu = false;
		this.MastersService.nuevoCurso(Profiles.Author);
		this.activeModal.close();

		//this.coursesService.createProjectAutomatically().subscribe(res => {
		//	this.router.navigate([`/course/${res.data.idCourse}/graph/${res.data.idTarget}`]);
		//	window.location.reload()
		//})
	}

	checkValue() {
		const filtradoValue = this.formulario.value;

		if (this.viewList) {
			this.treeCourses = [];
			this.getDataToTree();
		} else {
			this.coursesService
				.filtradoCursosNew(
					filtradoValue.filtrado,
					this.isMyCourses.value,
					this.rol
				)
				.subscribe(
					(res) => {
						this.cursos = res;
						this.cargando = false;
					},
					(err) => {
						console.log(err);
						this.cargando = false;
					}
				);
			this.authorControl.setValue("");
			this.authorControl.disable();
		}
	}

	changeView(list) {
		this.viewList = list;
		if (this.viewList) {
			this.isLoading = false;
			this.getDataToTree();
			this.cargando = this.treeCourses.length ? false : true;
		} else {
			this.filtrarCursos();
			this.cargando = this.cursos.length ? false : true;
		}
	}

	getDataToTree() {
		const datos = JSON.parse(this.localStorage.getItem(LOCALSTORAGESTRINGS.USER));
		const filter: string = this.formulario.value.filtrado;
		let rol: string = datos.profile.toLowerCase() === "autor" ? "editor" : datos.profile.toLowerCase();
		if (!this.treeCourses.length)
			this.coursesService
				.coursesListMode(filter, rol, this.isMyCourses.value)
				.subscribe((result) => {
					this.treeCourses = result;
					this.cargando = false;
				});
	}

	filtrarCursosLupa() {
		const filtradoValue = this.formulario.value;
		if (filtradoValue.filtrado.length >= 3) {
			if (this.viewList) {
				this.treeCourses = [];
				this.getDataToTree();
			} else {
				this.cargando = true;
				if (this.loginService.esAutor()) {
					this.coursesService
						.filtradoCursosNew(filtradoValue.filtrado, true, this.rol)
						.subscribe(
							(res) => {
								this.cursos = res;
								this.cargando = false;
							},
							(err) => {
								console.log(err);
								this.cargando = false;
							}
						);
				} else {
					const myCourses = this.isMyCourses.value;
					this.coursesService
						.filtradoCursosNew(filtradoValue.filtrado, myCourses, this.rol)
						.subscribe(
							(res) => {
								this.cursos = res;
								this.cargando = false;
							},
							(err) => {
								console.log(err);
								this.cargando = false;
							}
						);
				}
			}
		} else if (filtradoValue.filtrado.length === 0) {
			this.filtrarCursos();
		}
	}

	verDetalleMapa(idGrafo: number, idCourse: number) {
		if (this.modoTraerNodo) localStorage.setItem("modoTraerNodo", "true");
		else if (this.modoTraerActividad)
			localStorage.setItem(LOCALSTORAGESTRINGS.GETACTIVITY, "true");

		this.buscarNodo = JSON.parse(localStorage.getItem("buscarActividad"));
		this.router.navigateByUrl(`/course/${idCourse}/graph/${idGrafo}`);
		this.closeModal("Abrir mapa");
		this.close();
	}

	//Función que recibe los valores del elemento seleccionado en el listado de los cursos
	clickElement(element: CourseListModel) {
		const isAuthor: boolean = this.loginService.esAutor();

		//De forma provisional, sólo las acciones se contemplan desde el rol de estudiante para poder visualizar los nodos y actividades
		if (!isAuthor) {
			this.node = this.quiz = this.idSelectedCourse = null;
			this.answered = false;

			switch (element.type) {
				case ELEMENTTYPE.COURSE:
					break;
				case ELEMENTTYPE.GRAPH:
					break;
				case ELEMENTTYPE.NODE:
					this.isLoading = true;
					this.idSelectedCourse = element.idCourse;
					this.nodeService
						.getNode(element.idNode, element.idCourse, element.idTarget)
						.pipe(finalize(() => (this.isLoading = false)))
						.subscribe((result) => {
							this.node = result.data[0];
						});
					break;
				case ELEMENTTYPE.QUIZ:
					this.isLoading = true;
					this.idSelectedCourse = element.idCourse;
					this.getQuiz(
						element.idQuiz,
						element.idCourse,
						element.idTarget,
						element.multi
					);
					break;
			}
		}
	}

	private getQuiz(
		idQuiz: number,
		idCourse: number,
		idTarget: number,
		isMultiplexed: boolean
	) {
		this.idGraph = idTarget;

		if (isMultiplexed) {
			this.quizService
				.getQuizMultipleCanvasQuizMultiple(idQuiz, idCourse, idTarget)
				.pipe(finalize(() => (this.isLoading = false)))
				.subscribe((res) => {
					this.quiz = res.quiz;
					this.elements = res.elements;
				});
		} else {
			this.quizService
				.getQuiz(idQuiz, idCourse, idTarget)
				.pipe(finalize(() => (this.isLoading = false)))
				.subscribe((res: any) => {
					this.quiz = {
						...res.quiz,
						user: res.quiz.user,
						idOriginal: res.quiz.idOriginal,
						id: res.quiz.idQuiz,
						originalX: res.quiz.originalX,
						originalY: res.quiz.originalY,
						size: res.quiz.size,
						sizeQuiz: res.quiz.sizeQuiz,
						x: res.quiz.x,
						y: res.quiz.y,
					};
					this.elements = res.elements;
				});
		}
	}

	//Función que recibe valores para crear un nuevo elemento y añadirlo a la colección
	// createNewElement(newElement: NEWELEMENTTYPES){}
	// createUpdateGraph(idCurso: number) {}
	// createUpdateNode(){}
	// createUpdateQuiz(){}
	// viewGraph(){}

	// FUNCIONES DE LOS COMPONENTES DE QUIZ Y NODO

	showFiles(value) {
		let interval: number = 1;
		if (value) clearInterval(this.mouseInterval);

		this.mouseInterval = setInterval(() => {
			interval++;
			if (interval === 4) {
				this.isShowFiles = false;
				this.isShowPads = false;
				this.isShowLabels = false;
				clearInterval(this.mouseInterval);
			}
		}, DEFAULTCLOSEPADS / 3);

		this.isShowFiles = true; //Change variable value

		//QUENTAL
		if (this.viewQuiz) {
			if (this.utils.padsStatus.showPadsQuiz) this.isShowPads = true;
			if (this.utils.labelsStatus.showLabelsQuiz) this.isShowLabels = true;
		} else {
			if (this.utils.padsStatus.showPadsNode) this.isShowPads = true;
			if (this.utils.labelsStatus.showLabelsNode) this.isShowLabels = true;
		}
	}

	onViewGif(result: boolean) {
		let url: string = "";

		//Si result es true, gif ok; si es false, gif KO
		const numRandon = this.getRandomInt(1, 19);
		const numRandonNeg = this.getRandomInt(1, 12);

		if (result) url = URLCELEBRATIONS + "/positive/image-" + numRandon + ".gif";
		else url = URLCELEBRATIONS + "/negative/image-" + numRandonNeg + ".gif";

		//Mostrar un modal con el gif y que se cierre en 3 segundos o lo haga el usuario
		const modalRef = this.modalService.open(ModalCelebrationComponent, {
			scrollable: true,
			windowClass: MODAL_DIALOG_TYPES.W80 + " celebration-modal-window",
			backdropClass: "celebration-modal-backdrop",
		});

		modalRef.componentInstance.url = url;

		modalRef.result.then(
			(res) => {
				modalRef.close();
			},
			(err) => {}
		);

		setTimeout(() => {
			modalRef.close();
		}, 3000);
	}

	hidenGif(event) {}

	private getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}

	onAnswered() {
		this.answered = true;
	}

	// FIN FUNCIONES DE LOS COMPONENTES DE QUIZ Y NODO

	toggleFilters() {
    this.showElement = !this.showElement;
		this.buttonText = this.showElement ? 'Cerrar' : 'Filtros';
  }

	@HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    if (window.innerWidth <= 1020) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }
}
