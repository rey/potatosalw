import { Title } from '@angular/platform-browser';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { CourseModel, DetailCourseTargetModel } from 'src/app/core/models/courses';
import { LoginService } from 'src/app/core/services/login';
import { TargetsService } from 'src/app/core/services/targets';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';
import { CoursesService } from 'src/app/core/services/courses';
import { startWith, finalize, takeUntil, take, debounceTime  } from "rxjs/operators";
import { ModalAceptarCancelarComponent } from '../../../modal';
import { MODAL_DIALOG_TYPES } from 'src/app/core/utils/modal-dialog-types';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-proyecto-destino',
    templateUrl: './proyecto-destino.component.html',
    styleUrls: ['./proyecto-destino.component.scss'],
})
export class ProyectoDestinoComponent implements OnInit {

	@Input() target: DetailCourseTargetModel;
	@Input() curso: CourseModel;
	@Input() idGroup: number;
	@Output() action: EventEmitter<number> = new EventEmitter<number>();

	cargando: boolean = false;
	formulario: UntypedFormGroup;
	cursos: CourseModel[] = [];

    constructor(
			public targetsService: TargetsService,
			public translateService: TranslateService,
			private toaster: ToasterService,
			private formBuild: UntypedFormBuilder,
			public coursesService: CoursesService,
			private modalService: NgbModal,
			public activeModal: NgbActiveModal,
		) {
			this.formulario = this.formBuild.group({ filtrado: [""] });
			this.setupSearchListener();
		}

    ngOnInit() {
			this.filtrarCursos();
		}

		back(){
			this.action.emit(1);
		}

		setupSearchListener() {
			this.formulario.get('filtrado').valueChanges
				.pipe(debounceTime(500))
				.subscribe(() => {
					this.filtrarCursos();
				});
		}

		filtrarCursos() {
			this.cargando = true;
			const filtradoValue = this.formulario.value;
			this.coursesService
					.filtradoCursosNew(filtradoValue.filtrado, false, "editor")
					.subscribe(
						(res) => {
							this.cursos = res;
							this.cargando = false;
						},
						(err) => {
							console.log(err);
							this.cargando = false;
						}
					);
		}

		moverGrafo(course: any){
			// Abrimo un modal preguntando si desea mover el grafo a ese curso o no
			const modalRef = this.modalService.open(ModalAceptarCancelarComponent, {
				scrollable: true,
				windowClass: MODAL_DIALOG_TYPES.W60,
			});
			//modalRef.componentInstance.optionalTitle = this.translateService.instant("EDITARCURSOMAPA.DELETEGRAPH");
			modalRef.componentInstance.mensaje = "¿" + this.translateService.instant("EDITARCURSOMAPA.MOVERGRAFO") + " '" + course.courseTittle + "'?";
			modalRef.result.then((result: boolean) => {
				if (result) {
					this.targetsService.moveTargetToOtherProject(this.curso.idCourse, course.idCourse, this.target.idTarget).subscribe(
						(result) => {
							this.activeModal.close("Delete graph");
							this.toaster.success(
								this.translateService.instant("EDITARCURSOMAPA.OKMOVE")
							);
						},
						(err) => {
							this.toaster.error(
								this.translateService.instant("EDITARCURSOMAPA.KOMOVE")
							);
						}
					);
				}
			});
		}

		updateUrl(event: any) {
			event.target.src = "../../assets/images/no-image.png";
		}
}
