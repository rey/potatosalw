import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalReproducirComponent } from './modal-reproducir.component';

describe('ModalReproducirComponent', () => {
  let component: ModalReproducirComponent;
  let fixture: ComponentFixture<ModalReproducirComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReproducirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReproducirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
