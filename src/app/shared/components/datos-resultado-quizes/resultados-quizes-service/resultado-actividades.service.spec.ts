import { TestBed } from '@angular/core/testing';

import { ResultadoActividadesService } from './resultado-actividades.service';

describe('ResultadoActividadesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResultadoActividadesService = TestBed.get(ResultadoActividadesService);
    expect(service).toBeTruthy();
  });
});
