import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CenterIconComponent } from './center-icon.component';

describe('CenterIconComponent', () => {
  let component: CenterIconComponent;
  let fixture: ComponentFixture<CenterIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CenterIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CenterIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
