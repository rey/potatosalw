import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GroupModel } from 'src/app/core/models/groups/groups-model';
import { CourseModel, DetailCourseTargetModel } from 'src/app/core/models/courses';
import { User } from 'src/app/core/models/users/user.models';
import { GruposService } from 'src/app/core/services/groups/grupos.service';
import { LoginService } from 'src/app/core/services/login';
import { Profiles } from 'src/app/core/utils/profiles.enum';
import { TargetsService } from 'src/app/core/services/targets';
import { CoursesService } from 'src/app/core/services/courses';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';

@Component({
  selector: 'app-modal-listado-cursos',
  templateUrl: './modal-listado-cursos.component.html',
  styleUrls: ['./modal-listado-cursos.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class ModalListadoCursosComponent implements OnInit {

  group: GroupModel;
  public coursesList: CourseModel[] = [];
  public cursosGrupo: any[] = [];
  filter: {
    idGrupo: number,
    filter: string
  }
  public formulario: UntypedFormGroup;
  cargando: boolean;

	profile: string;
	user: User;
	newGroup: boolean = false;
	screenView: number = 1;
	listMultiples: any;
	configMultiples: any;
	target: DetailCourseTargetModel;
	curso: CourseModel;

  constructor(
    public activeModal: NgbActiveModal,
    private groupService: GruposService,
    private formBuild: UntypedFormBuilder,
    private loginService: LoginService,
    private translateService: TranslateService,
		public targetsService: TargetsService,
		public coursesService: CoursesService,
		private toaster: ToasterService

  ) {
    this.formulario = this.formBuild.group({
      filtrado: [''],
  });

   }

  ngOnInit() {
		this.profile = this.loginService.getProfile();
		this.user = this.loginService.getUser();
    this.getListadoCursos();
    this.filtrarCursos();
  }


  filtrarCursos(){
    this.cargando = true;
    const filtradoValue = this.formulario.value
    this.groupService.filtrarCursosBusqueda(this.group.idGroup, filtradoValue, this.group.idProfessor).subscribe((res:any) => {
      // console.log(res)
      this.coursesList = res.data;
      this.cargando = false;
    },
    err => console.log(err));
  }

  getListadoCursos() {
		this.cargando = true;
    this.groupService.targetListAsignedToGroup(this.group.idGroup, "").subscribe((res:any) => {
      // console.log(res)
      // if (condition) {

      // }
      this.cursosGrupo = res.data;
			this.cargando = false;
			this.group.assignedProjects = res.data.length;
    });
  }

  closeModal(sendData) {
		if (this.profile === Profiles.Teacher) {
			const selectedGroupInfo = {
				idGroup: this.group.idGroup,
				title: this.group.title,
				description: this.group.description,
				group: this.group,
				idUser: this.user.idUser,
				assignedProjects: this.group.assignedProjects,
				assignedStudents: this.group.assignedStudents
			}
			localStorage.removeItem('selectedGroupInfo');
			localStorage.setItem('selectedGroupInfo', JSON.stringify(selectedGroupInfo));
		}
    this.activeModal.close(sendData);
  }

  agregarCurso(course){
      // console.log(userInvitar)
      this.cargando = true;
      this.groupService.agregarCurso(this.group.idGroup, course.idCourse).subscribe(res => {
        // console.log(res)
        this.toaster.success(this.translateService.instant('MODALLISTCOURSES.ADDEDCOURSE'));
        this.cargando = false;
        this.getListadoCursos();
        this.filtrarCursos();
      },
      err => console.log(err));
  }

	agregarTarget(course, grafo){
		// console.log(userInvitar)
		this.cargando = true;
		this.groupService.agregarTarget(this.group.idGroup, course.idCourse, grafo.idTarget).subscribe(res => {
			// console.log(res)
			this.toaster.success(this.translateService.instant('MODALLISTCOURSES.ADDEDTARGET'));
			this.cargando = false;
			this.getListadoCursos();
			this.filtrarCursos();
		},
		err => console.log(err));
}

  eliminarCurso(curso){
    console.log(curso)

    this.cargando = true;
    this.groupService.eliminarCurso(this.group.idGroup, curso.idCourse).subscribe(res => {
      // console.log(res)
      // let  elementIndex = this.users.findIndex((user => userInvitar.idUser == user.idUser));
      // this.users[elementIndex].invitedToGroup = "T";
      this.toaster.success(this.translateService.instant('MODALLISTCOURSES.REMOVEDCOURSE'));
      this.cargando = false;
      this.getListadoCursos();
      this.filtrarCursos();
    },
    err => console.log(err));
  }

	eliminarGrafo(curso, grafo){
    console.log(curso);
		console.log(grafo);

    this.cargando = true;
    this.groupService.eliminarGrafo(this.group.idGroup, curso.idCourse, grafo.idTarget).subscribe(res => {
      // console.log(res)
      // let  elementIndex = this.users.findIndex((user => userInvitar.idUser == user.idUser));
      // this.users[elementIndex].invitedToGroup = "T";
      this.toaster.success(this.translateService.instant('MODALLISTCOURSES.REMOVEDTARGET'));
      this.cargando = false;
      this.getListadoCursos();
      this.filtrarCursos();
    },
    err => console.log(err));
  }

	editarMultiples(idCourse: number, idTarget: number){
		this.cargando = true;
		this.getListMultiples(idCourse, idTarget);
	}

	getListMultiples(idCourse: number, idTarget: number){
		this.targetsService.getListMultiplesByTarget(idCourse, idTarget).subscribe(res => {
			this.listMultiples = res.data;
			if(this.listMultiples != null){
				this.targetsService.getListTargetQuizzesMultiplesActiveByTarget(idTarget, this.group.idGroup).subscribe(res => {
					this.configMultiples = res.data;
					this.targetsService.getTargetById(idTarget).subscribe(result => {
						this.target = result.data;
						this.coursesService.getCourseById(idCourse).subscribe(result => {
							this.curso = result.data.courses;
							this.screenView = 2;
							this.cargando = false;
						});
					});


				})
			}
		});
	}

	changeScreen(screen: number){
		this.screenView = screen;
	}

  updateUrl(event: any) {
    event.target.src = '../../assets/images/no-image.png'
}

}
