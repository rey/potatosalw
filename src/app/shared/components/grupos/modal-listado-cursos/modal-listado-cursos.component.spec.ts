import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalListadoCursosComponent } from './modal-listado-cursos.component';

describe('ModalListadoCursosComponent', () => {
  let component: ModalListadoCursosComponent;
  let fixture: ComponentFixture<ModalListadoCursosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListadoCursosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListadoCursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
