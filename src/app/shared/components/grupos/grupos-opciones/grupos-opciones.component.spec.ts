import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GruposOpcionesComponent } from './grupos-opciones.component';

describe('GruposOpcionesComponent', () => {
  let component: GruposOpcionesComponent;
  let fixture: ComponentFixture<GruposOpcionesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposOpcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruposOpcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
