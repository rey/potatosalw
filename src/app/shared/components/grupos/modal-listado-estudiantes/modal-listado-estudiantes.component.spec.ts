import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalListadoEstudiantesComponent } from './modal-listado-estudiantes.component';

describe('ModalListadoEstudiantesComponent', () => {
  let component: ModalListadoEstudiantesComponent;
  let fixture: ComponentFixture<ModalListadoEstudiantesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListadoEstudiantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListadoEstudiantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
