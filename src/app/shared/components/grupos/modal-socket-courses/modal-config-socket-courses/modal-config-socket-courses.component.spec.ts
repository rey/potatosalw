import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalConfigSocketCoursesComponent } from './modal-config-socket-courses.component';

describe('ModalConfigSocketCoursesComponent', () => {
  let component: ModalConfigSocketCoursesComponent;
  let fixture: ComponentFixture<ModalConfigSocketCoursesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalConfigSocketCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfigSocketCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
