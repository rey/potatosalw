import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalSocketCoursesComponent } from './modal-socket-courses.component';

describe('ModalSocketCoursesComponent', () => {
  let component: ModalSocketCoursesComponent;
  let fixture: ComponentFixture<ModalSocketCoursesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSocketCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSocketCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
