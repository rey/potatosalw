import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalEditarGrupoComponent } from './modal-editar-grupo.component';

describe('ModalEditarGrupoComponent', () => {
  let component: ModalEditarGrupoComponent;
  let fixture: ComponentFixture<ModalEditarGrupoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditarGrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditarGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
