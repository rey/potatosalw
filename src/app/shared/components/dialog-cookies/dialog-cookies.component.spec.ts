import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogCookiesComponent } from './dialog-cookies.component';

describe('DialogCookiesComponent', () => {
  let component: DialogCookiesComponent;
  let fixture: ComponentFixture<DialogCookiesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCookiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCookiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
