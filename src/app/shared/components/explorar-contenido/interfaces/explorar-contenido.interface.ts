import { User } from "src/app/core/models/users/user.models";

export interface ICoursesExplorarContenido{
    idCourse: string;
    courseTittle: string;
    idCountry: string;
    courseSWLevel: string;
    user: string;
    listIdeoGrafos: IdeografosExplorarContenidoInterface[];
  }
  
  export interface IdeografosExplorarContenidoInterface{
    idTarget: string;
    backgroundImage: string;
    title: string;
    description: string;
  }
  