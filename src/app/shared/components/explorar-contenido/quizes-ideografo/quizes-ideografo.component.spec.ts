import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizesIdeografoComponent } from './quizes-ideografo.component';

describe('QuizesIdeografoComponent', () => {
  let component: QuizesIdeografoComponent;
  let fixture: ComponentFixture<QuizesIdeografoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizesIdeografoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizesIdeografoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
