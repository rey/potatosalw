import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExplorarContenidoComponent } from './explorar-contenido.component';

describe('ExplorarContenidoComponent', () => {
  let component: ExplorarContenidoComponent;
  let fixture: ComponentFixture<ExplorarContenidoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExplorarContenidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExplorarContenidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
