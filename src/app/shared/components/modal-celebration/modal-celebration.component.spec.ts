import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalCelebrationComponent } from './modal-celebration.component';

describe('ModalCelebrationComponent', () => {
  let component: ModalCelebrationComponent;
  let fixture: ComponentFixture<ModalCelebrationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCelebrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCelebrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
