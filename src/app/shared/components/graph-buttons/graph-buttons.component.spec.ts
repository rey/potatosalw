import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GraphButtonsComponent } from './graph-buttons.component';

describe('GraphButtonsComponent', () => {
  let component: GraphButtonsComponent;
  let fixture: ComponentFixture<GraphButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
