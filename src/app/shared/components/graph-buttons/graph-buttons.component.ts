import { take, takeUntil } from 'rxjs/operators';
import { ACTIONS } from '../../../core/utils/actions';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActionModel } from 'src/app/core/models/shared/actions.model';
import { LoginService } from 'src/app/core/services/login';
import { UsersService } from 'src/app/core/services/users';
import { Profiles } from 'src/app/core/utils/profiles.enum';
import { Subject } from 'rxjs';
import { Utils } from 'src/app/core/utils/utils';

@Component({
  selector: 'app-graph-buttons',
  templateUrl: './graph-buttons.component.html',
  styleUrls: ['./graph-buttons.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GraphButtonsComponent implements OnInit {
  @Output() action: EventEmitter<ActionModel> = new EventEmitter<ActionModel>()
  @Input() canEdit: boolean;
	isFullscreen:boolean = false
	profileBeforeChanges: string;
	private destroy$ = new Subject();

  constructor(private loginService: LoginService, private usersService: UsersService, private utils: Utils,) { }

  ngOnInit() {
		this.profileBeforeChanges = this.loginService.getProfile();
  }
	ngOnDestroy(): void {
		this.destroy$.next(true);
	}

  public esAutor(){
    return this.loginService.esAutor();
  }
  public esEstudiante(){
      return this.loginService.esEstudiante();
  }

/** LATERAL BUTTONS */

  zoomIn() {
    let action = new ActionModel(ACTIONS.ZOOMIN, true)
    this.action.emit(action)
  }
  zoomOut() {
    let action = new ActionModel(ACTIONS.ZOOMOUT, true)
    this.action.emit(action)
  }
  centerGraph() {
    let action = new ActionModel(ACTIONS.CENTERGRAPH, true)
    this.action.emit(action)
  }

  cambiarEstudiante(){
		this.usersService.getListUserProfile().pipe(takeUntil(this.destroy$), take(1)).subscribe((res: any) => {
			res.data.forEach((element) => {
				// if (element.idProfile == 1) {
				// 	this.user.profileEditor = true;
				// }
				if (element.idProfile == 2) {
					this.loginService.setProfile(Profiles.Student);
					localStorage.setItem('dontLoadMenu', 'true')
					this.utils.loadMenu = false;
					location.reload();
				}
				// if (element.idProfile == 3) {
				// 	this.loginService.setProfile(Profiles.Teacher);
				// 	location.reload();
				// }
			})
		})


  }

  cambiarEditor(){
      this.loginService.setProfile(Profiles.Author);
			localStorage.setItem('dontLoadMenu', 'true')
			this.utils.loadMenu = false;
      location.reload();
  }

	showFullscreen(){
		this.isFullscreen = !this.isFullscreen
		if(this.isFullscreen)
			document.documentElement.requestFullscreen()
		else
			document.exitFullscreen();
		}

}
