import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditComponent } from './quiz-edit.component';

describe('QuizzEditComponent', () => {
    let component: QuizEditComponent;
    let fixture: ComponentFixture<QuizEditComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [QuizEditComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuizEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
