import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditBodyInfoComponent } from './quiz-edit-body-info.component';

describe('QuizEditBodyInfoComponent', () => {
  let component: QuizEditBodyInfoComponent;
  let fixture: ComponentFixture<QuizEditBodyInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditBodyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditBodyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
