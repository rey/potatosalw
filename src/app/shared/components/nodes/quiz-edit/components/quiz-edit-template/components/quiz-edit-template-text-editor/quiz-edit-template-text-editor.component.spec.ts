import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditTemplateTextEditorComponent } from './quiz-edit-template-text-editor.component';

describe('QuizEditTemplateTextEditorComponent', () => {
  let component: QuizEditTemplateTextEditorComponent;
  let fixture: ComponentFixture<QuizEditTemplateTextEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditTemplateTextEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditTemplateTextEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
