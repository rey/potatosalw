import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditTemplateElementComponent } from './quiz-edit-template-element.component';

describe('QuizEditTemplateElementComponent', () => {
  let component: QuizEditTemplateElementComponent;
  let fixture: ComponentFixture<QuizEditTemplateElementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditTemplateElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditTemplateElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
