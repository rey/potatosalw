import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditTemplateComponent } from './quiz-edit-template.component';

describe('QuizEditTemplateComponent', () => {
  let component: QuizEditTemplateComponent;
  let fixture: ComponentFixture<QuizEditTemplateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
