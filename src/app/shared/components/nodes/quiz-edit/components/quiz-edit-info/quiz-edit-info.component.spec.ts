import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditInfoComponent } from './quiz-edit-info.component';

describe('QuizzEditInfoComponent', () => {
    let component: QuizEditInfoComponent;
    let fixture: ComponentFixture<QuizEditInfoComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [QuizEditInfoComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuizEditInfoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
