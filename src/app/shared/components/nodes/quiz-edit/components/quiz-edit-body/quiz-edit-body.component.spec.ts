import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditBodyComponent } from './quiz-edit-body.component';

describe('QuizEditBodyComponent', () => {
  let component: QuizEditBodyComponent;
  let fixture: ComponentFixture<QuizEditBodyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
