import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditBodyAnswerComponent } from './quiz-edit-body-answer.component';

describe('QuizEditBodyAnswerComponent', () => {
    let component: QuizEditBodyAnswerComponent;
    let fixture: ComponentFixture<QuizEditBodyAnswerComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [QuizEditBodyAnswerComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuizEditBodyAnswerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
