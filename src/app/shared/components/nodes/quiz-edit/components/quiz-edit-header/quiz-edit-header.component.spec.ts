import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditHeaderComponent } from './quiz-edit-header.component';

describe('QuizzEditHeaderComponent', () => {
    let component: QuizEditHeaderComponent;
    let fixture: ComponentFixture<QuizEditHeaderComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [QuizEditHeaderComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuizEditHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
