import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditBodyOptionComponent } from './quiz-edit-body-option.component';

describe('QuizEditBodyAnswerComponent', () => {
    let component: QuizEditBodyOptionComponent;
    let fixture: ComponentFixture<QuizEditBodyOptionComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [QuizEditBodyOptionComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(QuizEditBodyOptionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
