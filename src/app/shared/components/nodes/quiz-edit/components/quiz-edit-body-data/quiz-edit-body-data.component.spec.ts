import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditBodyDataComponent } from './quiz-edit-body-data.component';

describe('QuizEditBodyDataComponent', () => {
  let component: QuizEditBodyDataComponent;
  let fixture: ComponentFixture<QuizEditBodyDataComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditBodyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditBodyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
