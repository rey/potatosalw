import { HypermediaTypes } from "./hypermedia-types.enum";

export interface HypermediaItem {
    type: HypermediaTypes,
    name: string,
    isFile: boolean,
    accept?: string,
    icon: string
}
