import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizHypermediaComponent } from './quiz-hypermedia.component';

describe('QuizHypermediaComponent', () => {
  let component: QuizHypermediaComponent;
  let fixture: ComponentFixture<QuizHypermediaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizHypermediaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizHypermediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
