import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayElementComponent } from './quiz-play-element.component';

describe('QuizPlayElementComponent', () => {
  let component: QuizPlayElementComponent;
  let fixture: ComponentFixture<QuizPlayElementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
