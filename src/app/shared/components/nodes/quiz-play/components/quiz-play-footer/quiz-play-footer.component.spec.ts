import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayFooterComponent } from './quiz-play-footer.component';

describe('QuizPlayFooterComponent', () => {
  let component: QuizPlayFooterComponent;
  let fixture: ComponentFixture<QuizPlayFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
