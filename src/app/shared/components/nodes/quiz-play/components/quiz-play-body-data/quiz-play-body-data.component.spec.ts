import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayBodyDataComponent } from './quiz-play-body-data.component';

describe('QuizPlayBodyDataComponent', () => {
  let component: QuizPlayBodyDataComponent;
  let fixture: ComponentFixture<QuizPlayBodyDataComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayBodyDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayBodyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
