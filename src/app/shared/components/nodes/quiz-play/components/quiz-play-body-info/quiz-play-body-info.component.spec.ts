import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayBodyInfoComponent } from './quiz-play-body-info.component';

describe('QuizPlayBodyInfoComponent', () => {
  let component: QuizPlayBodyInfoComponent;
  let fixture: ComponentFixture<QuizPlayBodyInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayBodyInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayBodyInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
