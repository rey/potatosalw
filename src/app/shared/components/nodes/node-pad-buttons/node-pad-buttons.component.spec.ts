import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodePadButtonsComponent } from './node-pad-buttons.component';

describe('NodePadButtonsComponent', () => {
  let component: NodePadButtonsComponent;
  let fixture: ComponentFixture<NodePadButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodePadButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodePadButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
