import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalListStickersComponent } from './modal-list-stickers.component';

describe('ModalListStickersComponent', () => {
  let component: ModalListStickersComponent;
  let fixture: ComponentFixture<ModalListStickersComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalListStickersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalListStickersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
