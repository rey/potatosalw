import { SigmaToolbarsService } from './../../../../core/services/sigma-toolbars/sigma-toolbars.service';
import { CourseEditorsModel } from './../../../../core/models/courses/course-editors.model';
import { gEdge } from './../../../../core/models/graph/gEdge.model';
import { BackgroundUtils } from './utils/background.utils';
import { NodeOnTopResponse, QuizModel } from 'src/app/core/models/quizzes';
import { NodeUtils } from './utils/node.utils';
import { QuizUtils } from './utils/quiz.utils';
import { StickerUtils } from './utils/sticker.utils';
import { SigmaCanvasUtils } from './utils/canvas.utils';
import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SigmaUtils } from 'src/app/core/utils/sigma-utils';
import { GraphService } from 'src/app/core/services/graph/graph.service';
// Components
import { gNode, ObjetoNodoAuto } from 'src/app/core/models/graph/gNode.model';
import { NodeService } from 'src/app/core/services/node/node.service';
import { PowerService } from 'src/app/core/services/power/power.service';
import { QuizzesService } from 'src/app/core/services/quizzes';
import { LoginService } from 'src/app/core/services/login';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ModalComponent } from 'src/app/shared/components/modal';
import { finalize, map, take } from 'rxjs/operators';
import { NodeLinkComponent } from '../node-link/node-link.component';
import { TranslateService } from '@ngx-translate/core';
import { SIGMA_CONSTANTS, MULTIPLEXEDQUIZZES } from 'src/app/core/utils/sigma-constants';
import { SigmaCanvasService } from './sigma-canvas.service';
import { forkJoin, Subscription } from 'rxjs';
import { ImagenPipe } from 'src/app/shared/pipes/imagen.pipe';
import { NodeCoverPipe } from 'src/app/shared/pipes/node-cover.pipe';
import { HttpEventType } from '@angular/common/http';
import { ActionModel } from 'src/app/core/models/shared/actions.model';
import { NodeModeAutoComponent } from '../../../../shared/components/nodes/node-auto/node-mode-auto.component';
import { StickersService } from 'src/app/core/services/stickers/stickers.service';
import { QuizEditMultipleComponent } from '../quiz-edit-multiple/quiz-edit-multiple.component';
import { CoursesService } from 'src/app/core/services/courses';
import { User } from 'src/app/core/models/users/user.models';
import { MastersService } from 'src/app/core/services/masters';
import { PointNodeService } from 'src/app/core/services/point-node/point-node.service';
import { Profiles } from 'src/app/core/utils/profiles.enum';
import { CanvasData, Estado } from 'src/app/core/models/tuturial/tutorial.model';
import { Utils } from 'src/app/core/utils/utils';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { LOCALSTORAGESTRINGS } from 'src/app/core/models/masters/localstorage.enum';
import { ModalCursosListadoComponent } from 'src/app/shared/components/cursos/modal-cursos-listado';
import { environment } from 'src/environments/environment';
import { DomainTypes } from 'src/app/core/models/masters/masters.enum';
import { NodeConnectModel, OptionsNodeConnectModel } from './sigma-canvas.model';
import { MODAL_DIALOG_TYPES } from 'src/app/core/utils/modal-dialog-types';
import { ACTIONS } from 'src/app/core/utils/actions';
import { PadsUtils } from 'src/app/core/utils/pads.utils';
import { ModalProfileActionsComponent } from '../../../../shared/components/modal-profile-actions/modal-profile-actions.component';
import { UsersService } from 'src/app/core/services/users';
import { LocalStorage } from 'src/app/core/utils';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';

declare var $: any;
@Component({
	selector: 'app-sigma-canvas',
	templateUrl: './sigma-canvas.component.html',
	styleUrls: ['./sigma-canvas.component.scss'],
	providers: [ImagenPipe]
})
export class SigmaCanvasComponent implements OnInit, OnChanges {

	@ViewChild('modalConnect') modalConnect: ModalComponent;
	@ViewChild('modalAlert') modalAlert: ModalComponent;
	@Output() nextEvenCopilot = new EventEmitter<number>();

	@Input() idCurso: number;
	@Input() tutorialGrafo: CanvasData;
	@Input() idMapa: number;
	@Input() canEdit: boolean = false;
	@Input() isGuest: boolean = false;
	@Input() background: string;
	@Input() changedToEditor: boolean;
	@Input() graphTitle: string;
	@Input() defaultViewType: number;

	isDemo: boolean = environment.domain === DomainTypes.DEMO ? true : false
	loginRoute = '/auth/login';
	register = '/auth/sign-up';

	private sigmaUtils: SigmaUtils;
	private eventSubscriptions = [];
	private currentGraph;
	private selection = [];
	listQuiz: any = [];
	public ModoAuto: ObjetoNodoAuto;
	public powerListString = {
		power0: 'power0', power1: 'power1', power2: 'power2', power3: 'power3',
		powerNegative1: 'powerNegative1', powerNegative2: 'powerNegative2', powerNegative3: 'powerNegative3'
	}
	public nextBackNode = { avanzar: false, regresar: false }
	zoomNode: number = 0;

	explicacionesQuizzes: any = []
	ordenSeleccionado: number[] = [0];
	paginaSeleccionada: number = 0;
	usuario: User;
	tutorialSW: any;
	version: string = ''
	imagePlaceHolder: any;
	description: string;
	infoNodeTitlePlaceHolder: string;
	idElements: gNode[];
	anteriorNodo: gNode;
	nodeClickPopover: gNode;
	countHelp: number = 0;
	mostrarAyuda: boolean;
	mostrarAyudaQuiz: boolean;
	countHelpQuiz: number = 0;
	buscarNodo: boolean = false;
	isNode: boolean = false
	isModalOpen: boolean = false
	modoTraerNodo: boolean = false;
	modoTraerActividad: boolean = false;
	public formulario: UntypedFormGroup;
	lastNodeId: any = '';
	viewTypeGraph = { viewType: 0, set: false };
	user: User;
	userIsEditor: boolean;
	nextEnabled: boolean = true;
	backEnabled: boolean = false;

	public get selectedNodes(): any[] {
		return this.selection;
	}

	// Mode
	addMode: boolean = false;
	infoMode: boolean = false;
	selectionMode: boolean = true;
	connectMode: boolean = false;
	disconnectMode: boolean = false;

	infoNodeTitle: string = '';
	infoNodeDesc: string = '';
	overNode: boolean = false;
	clickNode: boolean = false;
	rightclickNode: boolean = false;
	clickEdge: boolean = false
	overEdge: boolean = false

	startDragCoords: any;
	private showStaircase: boolean = false;

	isShowPads: boolean = false
	isShowLabels: boolean = false

	// Detectar si se hace drag del fondo del canvas.
	isDragging: boolean = false;
	clickStageCoords: any;

	// Dibujar Ejes
	drawAxis: boolean = false;

	public viewType = {
		nodes: true,
		quizzes: true
	};

	private hiddedSticker = null
	public progress: number = 0

	// HTML vars
	public connectN1: NodeConnectModel = { id: '', idOriginal: 0, url: '', label: '' };
	public connectN2: NodeConnectModel = { id: '', idOriginal: 0, url: '', label: '' };

	public connectIcon = 'link';
	public loading = false;
	public g;

	//INI QUIZ TIENE TEMPLATE
	templateDatos: any[] = [];
	isLoading: boolean = false;
	courseTitle: string = ''
	actualProfile: string;
	private _profileSubscription: Subscription;
	private _showModalSubscription: Subscription;
	private _updateGraph: Subscription;

	private _sizeInterval: any

	@ViewChild('sigmaContainer', { static: true }) sigmaContainer: ElementRef<HTMLElement>;
	@ViewChild('nodeQuizzEditPopover', { static: true }) nqEditPopover: ElementRef<HTMLElement>;
	@ViewChild('nodeQuizzPlaceHolderPopover', { static: true }) nqPlaceholderPopover: ElementRef<HTMLElement>;
	@ViewChild('pActions', { static: true }) pActions: ElementRef<HTMLElement>;
	constructor(
		private graphServ: GraphService,
		private powerServ: PowerService,
		private pointNodeService: PointNodeService,
		private nodeService: NodeService,
		private quizService: QuizzesService,
		private courseService: CoursesService,
		private router: Router,
		private route: ActivatedRoute,
		private modalService: NgbModal,
		private toolsService: SigmaToolbarsService,
		public loginService: LoginService,
		public dialog: MatDialog,
		private toaster: ToasterService,
		private translateService: TranslateService,
		private sigmaCanvasService: SigmaCanvasService,
		private nodeCoverPipe: NodeCoverPipe,
		private stickersServices: StickersService,
		private masterServices: MastersService,
		private utils: Utils,
		public scUtils: SigmaCanvasUtils,
		public nodeUtils: NodeUtils,
		private stickerUtils: StickerUtils,
		public quizUtils: QuizUtils,
		public bgUtils: BackgroundUtils,
		private formBuild: UntypedFormBuilder,
		private padsUtils: PadsUtils,
		public usersService: UsersService,
		private localStorage: LocalStorage,

	) {this.user = this.loginService.getUser()}

	ngOnInit() {
		this.initComponent()
		this.formulario = this.formBuild.group({ certificadoQuiz: [false] })
		this.showProfileActionsModal()
		this.actualProfile = this.loginService.getProfile();
		this.nodeService.getRedirectedNode().subscribe((node: gNode) => {
			if(Object.keys(node).length != 0 ){
				this.playNode(node)
			}
		 });

		this.nodeService.getRedirectedQuiz().subscribe((quiz: any) => {
		if(Object.keys(quiz).length != 0 ){
			this.playQuiz(quiz)
		}
		});
	}

	addSticker() {
		this.stickerUtils.addSticker()
	}

	addText() {
		this.stickerUtils.addText()
	}


	tutorialCanvasGrafo(step: number): void {
		this.nextEvenCopilot.emit(step);

		if (step === 5) {
			this.tutorialGrafo.nodoNodo = true;
		}
		if (step === 6) {
			this.tutorialGrafo.quizQuiz = true;
		}

	}

	setViewType() {

		switch (this.defaultViewType) {
			case 1:
				setTimeout(() => {
					this.toolsService.setHideElements()
				}, 500);
				this.viewTypeGraph.set = true
				break;
			case 2:
				setTimeout(() => {
				}, 500);
				this.viewTypeGraph.set = true
				break;
			case 3:
				setTimeout(() => {
					this.toolsService.changeVisibility({ nodes: true, quizzes: false });
				}, 200);
				this.viewTypeGraph.set = true
				break;
			case 4:
				setTimeout(() => {
					this.toolsService.changeVisibility({ nodes: false, quizzes: true });
				}, 200);
				this.viewTypeGraph.set = true
				break;
		}
	}

	private _getInfoCourse(): void {
		this.courseService.getCourseById(this.idCurso).subscribe(course => {
			//se hace que si el titulo es LanguageRounded, se acorta y se ponen ... suspensivos
			const title = course.data.courses.courseTittle;
			if (title.length > 24)
				this.courseTitle = title.substring(0,22) + '...';
			else
				this.courseTitle = title;
		})
	}

	modAutoPrototype(listQuiz: any) {
		let currentPower = this.powerServ.getPower();

		//devuelve el objeto ya ordenado para recorrer en modo auto

		this.ModoAuto = new ObjetoNodoAuto(listQuiz.nodes, currentPower);

		this.idElements = this.ModoAuto.explicacionesPower0;
		this.graphServ.getFlickNode(this.idCurso, this.idMapa).subscribe(res => {
			if (res.data.idNode !== 0) {
				this.lastNodeId = 'n' + res.data.idNode;
			}
			this.susbcribeChangedPointNode();
		}, err => {
			this.susbcribeChangedPointNode();
		})
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.userIsEditor = this.loginService.esAutor();
		if (changes.background) {
			this.bgUtils.background = changes.background.currentValue
			this.bgUtils.setGraphBackground(changes.background.currentValue)
		} else if (changes.canEdit) {
			this.scUtils.hidePopWindows()
			$('#pActions').hide();
		}
	}

	ngOnDestroy() {
		this.loginService.currentProfile.next(null)
		this._profileSubscription.unsubscribe()
		this._updateGraph != undefined ? this._updateGraph.unsubscribe(): '';
		this._showModalSubscription != undefined ? this._showModalSubscription.unsubscribe() : '';
		this.eventSubscriptions.forEach((s) => s.unsubscribe());
	}

	// ---------------------------------------
	//  TOOLBARS SUBSCRIPTIONS
	// ---------------------------------------
	private subscribeToolbars() {

		this.toolsService.visibility.subscribe(visibility => {
			this.changeView(visibility);
		});

		this._showModalSubscription = this.toolsService.showModalCourses.subscribe(value => {
			//Se muestra el modal de los cursos con el modo listado o en detalle del curso con los grafos

			if (value === 'inlineList')
				this.scUtils.showListCourses(this.idCurso)
			else
				this.scUtils.showCourseGraphs(this.idCurso)
		})

		this._updateGraph = this.toolsService.updateGraph.subscribe(value => {
			if (this._sizeInterval)
				clearInterval(this._sizeInterval)
			this.initComponent(value)
		})
	}

	private subscribePowerActive() {
		this.powerServ.powerActive.subscribe((powersActive: Array<number>) => {
			this.ordenSeleccionado = powersActive;
			this.zoomNode = 0;
			let filter = this.powerServ.convertNumberArrayToPowerObject(powersActive);
			this.changeView({ nodes: true, quizzes: true })
			let vReturn: boolean = false

			//Escondo todo y luego muestro las cosas
			this.sigmaUtils.allNodes().map(node => {
				if (node.nodeType === SIGMA_CONSTANTS.NODE_TYPE || node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE)
					node.hidden = true
			})

			let nodes: any[] = this.sigmaUtils.allNodes().filter(n => n.nodeType === SIGMA_CONSTANTS.NODE_TYPE)

			//Si el editor decide crear un quiz sin tener creada ninguna idea, hay que mostrarlo
			if (!nodes.length)
				this.sigmaUtils.allNodes().filter(n => n.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE).map(quiz => quiz.hidden = false)
			else {

				nodes.map(node => {

					if (node.power0 && filter[this.powerListString.power0]) {
						this.idElements = this.ModoAuto.explicacionesPower0;
						vReturn = true;
						node.hidden = !vReturn
					}
					else if (node.power1 && filter[this.powerListString.power1]) {
						this.idElements = this.ModoAuto.explicacionesPower1;
						vReturn = true;
						node.hidden = !vReturn
					}
					else if (node.power2 && filter[this.powerListString.power2]) {
						this.idElements = this.ModoAuto.explicacionesPower2;
						vReturn = true;
						node.hidden = !vReturn
					}
					else if (node.power3 && filter[this.powerListString.power3]) {
						this.idElements = this.ModoAuto.explicacionesPower3;
						vReturn = true;
						node.hidden = !vReturn
					}
					else if (node.powerNegative1 && filter[this.powerListString.powerNegative1]) {
						this.idElements = this.ModoAuto.explicacionesPowerMinus1;
						vReturn = true;
						node.hidden = !vReturn
					}
					else if (node.powerNegative2 && filter[this.powerListString.powerNegative2]) {
						this.idElements = this.ModoAuto.explicacionesPowerMinus2;
						vReturn = true;
						node.hidden = !vReturn
					}
					else if (node.powerNegative3 && filter[this.powerListString.powerNegative3]) {
						this.idElements = this.ModoAuto.explicacionesPowerMinus3;
						vReturn = true;
						node.hidden = !vReturn
					}
					else {
						vReturn = false;
						node.hidden = !vReturn
					}

					if (vReturn)
						this.scUtils.showQuizzes(node.id) // Mostramos los quizzes asociados a los nodos que estén en la potencia/s selecciona/s
				});

			}


			this.sigmaUtils.refresh();
		});
	}


	private susbcribeChangedPointNode() {
		let start = false;
		let idNode;



		let pos: number = this.lastNodeId !== '' ? this.idElements.findIndex(element => {
			return element.id === this.lastNodeId
		}) : 0;

		this.zoomNode = pos;

		setTimeout(() => {
			if (this.loginService.esEstudiante()) {
				start = true;
				if (this.idElements[pos]) {
					idNode = this.idElements[pos].id;
					this.actionInCanvas(idNode, this.anteriorNodo);
				}
			}
		}, 1000);

		this.pointNodeService.pointNodeActive.subscribe((values: { avanzar: boolean; regresar: boolean }) => {

			if (this._sizeInterval)
				clearInterval(this._sizeInterval)

			this.nextBackNode = values;
			if (!start && this.nextBackNode.avanzar && this.scUtils.existsElement(pos, this.idElements)) {
				this.zoomNode = pos;
				start = true;
				idNode = this.idElements[pos].id;
				this.actionInCanvas(idNode, this.anteriorNodo);
			}
			//**Metodo que verifica si existe un siguiente elemento this.existsElement(pos)*/
			else if (start && this.nextBackNode.avanzar && this.scUtils.existsElement(pos + 1, this.idElements)) {
				pos = pos + 1;
				//si es el ultimo bloqueamos el boton derecho
					let pos2 = pos;
					while (this.scUtils.existsElement(pos2 + 1, this.idElements)) {
						pos2 += 1;
					}
					if(pos === pos2){
						this.sigmaCanvasService.setNextEnabled(false);
					}
					this.sigmaCanvasService.setBackEnabled(true);
				this.zoomNode = pos;
				idNode = this.idElements[pos].id;
				this.actionInCanvas(idNode, this.anteriorNodo);
			}
			else if (start && this.nextBackNode.regresar && this.scUtils.existsElement(pos - 1, this.idElements)) {
				pos = pos - 1;
				//si es el primero bloqueamos el boton izquierdo
				if(pos === 0){
					this.sigmaCanvasService.setBackEnabled(false);
				}
				this.sigmaCanvasService.setNextEnabled(true);
				this.zoomNode = pos;
				idNode = this.idElements[pos].id;
				this.actionInCanvas(idNode, this.anteriorNodo);
			}
		})
	}
	actionInCanvas(idNode: string, anteriorNodo: gNode) {
		if (this.lastNodeId !== '') {
			if (anteriorNodo !== undefined && anteriorNodo.id !== idNode) {
				// idNode = this.lastNodeId;
				this.lastNodeId = idNode;
			}
			else {
				idNode = this.lastNodeId;
			}
		}
		if (anteriorNodo !== undefined) {
			this.sigmaUtils.allNodes().filter(sigmaNode => sigmaNode.id === anteriorNodo.id).map(value => {
				this.sigmaUtils.decrementNodeSize(value);
			})
		}
		this.sigmaUtils.allNodes().filter(sigmaNode => sigmaNode.id === idNode).map(value => {
			this._sizeInterval = this.sigmaUtils.nodeAnimation(value)
			this.anteriorNodo = { ...value };
		}, err => { console.log(err) })
	}


	// ---------------------------------------
	//  JQUERY EVENT SUBSCRIPTIONS
	// ---------------------------------------
	private subscribejQueryClickEvent() {
		let self = this

		$('canvas').click(function (e) {

			if (this.loginService.esAutor() && this.canEdit) {
				$('.popover, .popover2').hide();
				if ((!this.isDragging) && (!this.clickNode) && (!this.clickEdge)) {
					this.addMode = true;
					// if ((this.addMode) && (!this.isDragging) && (!this.clickNode)) {    // ADD NEW NODE PANEL
					var pos = $('canvas').offset(); // $('canvas').position();
					var left = e.clientX; //e.pageX;
					var top = e.clientY; //e.pageY;
					var height = $('#pActions').height();

					$('#pActions').css('left', (left - pos.left + 15) + 'px');
					$('#pActions').css('top', (top - (height / 2) - 10) + 'px');

					$('#pActions').show();

					var width = $('#pActions').width();
					self._resizePopover(width)

				} else {
					this.addMode = false;
					this.sigmaUtils.dropNode('temp');
					if (this.showStaircase)
						this.sigmaUtils.drawAxis();
				}
			}
			self.toolsService.setHideElements()
			self.toolsService.setHideMenuLateral()

		}.bind(this));

	}

	private _resizePopover(width: number) {

		let currentClass = ''
		$('#pActions').find('.w-20').length !== 0 ? currentClass = '.w-20' : currentClass = '.w-25'

		if (width >= 250 && width < 300) {
			$(`#pActions ${currentClass}`).addClass('w-50')
			$(`#pActions ${currentClass}`).removeClass('w-100')
		}
		else if (width < 250) {
			$(`#pActions ${currentClass}`).removeClass('w-50')
			$(`#pActions ${currentClass}`).addClass('w-100')
		}
		else
			$(`#pActions ${currentClass}`).removeClass('w-100 w-50')
	}

	private subscribeJQueryMouseUp() {
		$('canvas').on('mouseup', function (e) {
			this.isDragging = false;
			if (Math.abs(e.clientX - this.clickStageCoords.x) > 5 || Math.abs(e.clientY - this.clickStageCoords.y) > 5)
				this.isDragging = true;
		}.bind(this));
	}

	private subscribeJQueryMouseDown() {
		$('canvas').on('mousedown', function (e) {
			this.clickStageCoords = { x: e.clientX, y: e.clientY };
		}.bind(this));
	}

	private subscribejQueryMouseMoveEvent() {
		$('canvas').on('mousemove', function (e) {
			$('#pConnect').hide();
			if ((!this.addMode) && (this.infoMode)) {
				$('#pNodeInfo').hide();
				$('#pActions').hide();
				this.sigmaUtils.dropNode('temp');

				if (this.overNode && ((!$('#pNodeMenu').is(':visible')) || (!$('#pConnectInfo').is(':visible')))) {
					var left = e.pageX;
					var top = e.pageY;
					var pos = $('canvas').offset(); // $("canvas").position();
					var height = $('#pNodeInfo').height();
					$('#pNodeInfo').css('left', (left - pos.left + 30) + 'px');
					$('#pNodeInfo').css('top', (top - (height / 2) - 10) + 'px');
					$('#pNodeInfo').show();
				}
			}

			if (this.connectMode) {
				var pos = $("canvas").position();
				var left = e.pageX;
				var top = e.pageY;
				var height = $('#pConnect').height();

				$('#pConnect').css('left', (left - pos.left) + 'px');
				$('#pConnect').css('top', (top - (height / 2) - 10) + 'px');
				$('#pConnect').show();
			}
		}.bind(this));
	}

	private jqueryContextMenu() { }

	private subscribeUpdateCoords() {
		var c = this.sigmaUtils.sigma.camera;
		let self = this;
		c.bind("coordinatesUpdated", function () {
			if (self.showStaircase)
				self.scUtils.viewStaircaseChart(self.showStaircase, self.sigmaUtils, self.viewType);
		});
	}

	private subscribeNewNode() {
		this.nodeService.newNode.subscribe(newNode => {
			this.sigmaUtils.addNode(newNode);
			this.sigmaUtils.refresh();
		});
	}

	private subscribeNewQuiz() {
		this.quizService.newQuiz.subscribe(newQuiz => {
			this.sigmaUtils.addNode(newQuiz);
			this.sigmaUtils.refresh();
		});
	}

	// ---------------------------------------
	//  SIGMAJS EVENT SUBSCRIPTIONS
	// ---------------------------------------

	// Cuando se hace click en una parte vacia del canvas crea un nodo temporal que se ve como un punto
	private subscribeClickStageEvent() {
		this.eventSubscriptions['clickStageEvent'] = this.sigmaUtils.getemitclickStageEvent()
			.subscribe(e => {
				$('.popover3').hide();
				this.rightclickNode = false;
				this.connectMode = false;

				if (!this.isDragging && this.loginService.esAutor() && this.canEdit) {
					var cx = e.data.captor.x;
					var cy = e.data.captor.y;
					var node = this.graphServ.emptygNode();
					this.sigmaUtils.addTempNode(node, cx, cy, this.loginService.getUser());
					this.clickNode = false;
					var positionClick = { "clientX": cx, "clientY": cy };
					var canvas = document.getElementById("sigma-container");
					var pos = this.sigmaUtils.getMousePos(canvas, positionClick);
					localStorage.setItem('mouseClick', JSON.stringify(pos));
				}
			});
	}

	private subscribedoubleClickNodeEvent() {
		this.eventSubscriptions['doubleclickNodeEvent'] = this.sigmaUtils.getemitdoubleclickNodeEvent()
			.subscribe(e => {
				this.clickNode = true;
				this.rightclickNode = false;
				this.connectMode = false;
				if (e.data.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
					$('.pActions').hide();
					this.scUtils.hidePopWindows()
					if (this.canEdit) {
						this.editNode(e.data.node);
					} else {
						this.playNode(e.data.node);
					}
				}

			});
	}

	// Cuando se situa el cursor sobre un nodo cambia el estilo del cursor y rellena las propiedades infoNodeTitle y infoNodeDesc si se está en modo info. Marca overNode a true
	//y muestra un popover con info del nodo.
	private subscribeOverNodeEvent() {
		this.eventSubscriptions['overNodeEvent'] = this.eventSubscriptions.push(
			this.sigmaUtils.getemitoverNodeEvent().subscribe(
				e => {
					this.sigmaContainer.nativeElement.style.cursor = 'pointer';
					if (e.data.node.nodeType !== SIGMA_CONSTANTS.QUIZ_TYPE) {
						this.overNode = true;

						//Esconder sticker cuando el usuario vaya a interactuar con un nodo y un sticker esté por debajo
						if (!this.loginService.esAutor()) {
							if (e.data.node.nodeType == SIGMA_CONSTANTS.STICKER_TYPE && e.data.node.link === '') {
								this.hiddedSticker = e.data.node
								e.data.node.hidden = true

							}
						}

						if ((this.infoMode) && (e.data.node)) {
							this.infoNodeTitle = e.data.node.label;
							if (e.data.node.description) {
								this.infoNodeDesc = e.data.node.description;
							} else {
								this.infoNodeDesc = "...";
							}

						}
					}
					else {
						if (!this.loginService.esAutor()) {
							if (e.data.node.nodeType == SIGMA_CONSTANTS.STICKER_TYPE) {
								this.hiddedSticker = e.data.node
								e.data.node.hidden = true
							}
						}
					}
				}
			)
		);
	}


	// Al quitar el curso de un nodo marca overNode a false y devuelve el cursor a estilo default
	private subscribeOutNodeEvent() {
		this.eventSubscriptions['outNodeEvent'] = this.sigmaUtils.getemitoutNodeEvent()
			.subscribe(e => {
				this.overNode = false;
				this.sigmaContainer.nativeElement.style.cursor = 'default';

				if (!this.loginService.esAutor()) {
					if (this.hiddedSticker) {
						if (this.currentGraph.stickers.length) {
							this.currentGraph.stickers.forEach(e => {
								this.sigmaUtils.sigma.graph.nodes(e.id).hidden = false
							})
							this.hiddedSticker = null
							this.sigmaUtils.refresh()
						}
					}
				}
			});
	}

	/**
	 * CLICK ON AN EDGE
	 */
	private subscribeClickEdgeEvent() {

		this.eventSubscriptions['clickEdgeEvent'] = this.sigmaUtils.getemitclickEdgeEvent().subscribe(e => {

			this.connectMode = false;
			this.clickNode = false;
			this.rightclickNode = false;
			this.clickEdge = true
			this.scUtils.hidePopWindows()

			let edge: gEdge = e.data.edge
			let nodeFrom: gNode, nodeTo: gNode = null

			nodeFrom = this.sigmaUtils.sigma.graph.nodes(edge.source)
			nodeTo = this.sigmaUtils.sigma.graph.nodes(edge.target)

			let connectN1: NodeConnectModel = { id: nodeFrom.id, idOriginal: nodeFrom.idOriginal, url: nodeFrom.url, label: nodeFrom.label }
			let connectN2: NodeConnectModel = { id: nodeTo.id, idOriginal: nodeTo.idOriginal, url: nodeTo.url, label: nodeTo.label }
			let options: OptionsNodeConnectModel = { type: edge.type, color: edge.color, size: edge.size, label: edge.label, id: edge.id }


			if (edge.connectionType === SIGMA_CONSTANTS.NODES_NODES && this.loginService.esAutor() && this.canEdit
				&& nodeFrom.nodeType === SIGMA_CONSTANTS.NODE_TYPE && nodeTo.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
				this.conectarNodos(connectN1, connectN2, options);
			}

		})

	}

	private subscribeOverEdgeEvent() {
		this.eventSubscriptions['overEdgeEvent'] = this.eventSubscriptions.push(
			this.sigmaUtils.getemitoverEdgeEvent().subscribe(
				e => {
					let edge: gEdge = e.data.edge
					if (edge.connectionType === SIGMA_CONSTANTS.NODES_NODES && this.loginService.esAutor() && this.canEdit)
						this.sigmaContainer.nativeElement.style.cursor = 'pointer';
				}
			)
		);
	}


	// Al quitar el curso de un nodo marca overNode a false y devuelve el cursor a estilo default
	private subscribeOutEdgeEvent() {
		this.eventSubscriptions['outEdgeEvent'] = this.sigmaUtils.getemitoutEdgeEvent()
			.subscribe(e => {
				let edge: gEdge = e.data.edge
				this.overEdge = false;
				this.clickEdge = false
				if (edge.connectionType === SIGMA_CONSTANTS.NODES_NODES && this.loginService.esAutor() && this.canEdit)
					this.sigmaContainer.nativeElement.style.cursor = 'default';
			});
	}

	// Cuando se hace click en un nodo o quizz llama a playNode si tiene nodeType Node o a playQuizz si no
	private subscribeClickNodeEvent() {
		this.eventSubscriptions['clickNodeEvent'] = this.sigmaUtils.getemitclickNodeEvent()
			.subscribe(e => {
				this.connectMode = false;
				this.clickNode = true;
				this.rightclickNode = false;

				if (!this.loginService.esAutor()) {
					if (this.hiddedSticker) {
						if (this.currentGraph.stickers.length) {
							this.currentGraph.stickers.forEach(e => {
								this.sigmaUtils.sigma.graph.nodes(e.id).hidden = false
							})
							this.hiddedSticker = null
							this.sigmaUtils.refresh()
						}
					}
				}

				if (!this.isDragging && !this.isLoading) {

					if (e.data.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
						$('.pActions').hide();
						$('.popover3').hide();
						this.openPopoverInfoNodo(e);

					}
					else if (e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE) {
						if (this.canEdit) {
							//ASI Sabemos si el quiz lo ha creado el dueño del grafo o es un quiz alquilado

							//if (e.data.node.user.idUser !== this.loginService.getUser().idUser) {
							//Alex: recoger la lista de editores y permitir editar, si no, entonces si es alquilado
							//se hardcodea en false hasta que se cambie
							if (false) {
								/**
								 * Mientras se desallora el backen se desarrolla Si el
								 * quiz es alquilado de momento mostraremos un mensaje  que no se puede editar
								**/
								this.toaster.success(this.translateService.instant('SIGMACOMPONENT.ACTIVITYSTUDENTMODE'));
								this.playQuiz(e.data.node);
							}
							else {
								if (e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE && e.data.node.isMultiplexed === MULTIPLEXEDQUIZZES.ISMULTIPLEXED) {
									this.editQuizMultiple(e.data.node);
								}
								else if (e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE && e.data.node.isMultiplexed === MULTIPLEXEDQUIZZES.ISNOTMULTIPLEXED) {
									this.isModalOpen = true
									this.quizUtils.editQuiz(e.data.node, this.countHelpQuiz).finally(() => this.isModalOpen = false)
								}
							}

						}
						else {
							if (e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE && e.data.node.isMultiplexed === MULTIPLEXEDQUIZZES.ISMULTIPLEXED) {
								this.playQuiz(e.data.node);
							}
							else if (e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE && e.data.node.isMultiplexed === MULTIPLEXEDQUIZZES.ISNOTMULTIPLEXED) {
								this.playQuiz(e.data.node);
							}
						}
					}
					else if (e.data.node.nodeType === SIGMA_CONSTANTS.STICKER_TYPE) {
						const link: string = e.data.node.link
						if (link)
							window.open(link)
					}
				}
			});
	}


	openPopoverInfoNodo(e: any) {

		$('.popover3').hide();
		window.oncontextmenu = function () { return false };
		if (e.data.node.id !== 'temp') {

			this.nodeClickPopover = e.data.node as gNode;
			if (e.data.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
				var nqPlaceholderElement = this.nqPlaceholderPopover.nativeElement;
				nqPlaceholderElement.style.display = "block";
				this.imagePlaceHolder = `url(${e.data.node.url})`;

				this.description = e.data.node.description.trim() == '' ? this.translateService.instant('SIGMACOMPONENT.WITHOUTDESCRIPTION') : e.data.node.description;
				this.infoNodeTitlePlaceHolder = ((!e.data.node.label) || (e.data.node.label == '') || (e.data.node.label.length < 1)) ? this.translateService.instant('SIGMACOMPONENT.WITHOUTTITLE') : e.data.node.label;
				let top: number;
				let left: number;
				var pos = this.sigmaUtils.getMousePos(this.sigmaContainer.nativeElement, e.data.captor);
				if (pos.y < 100) {
					top = 20;
				} else if (this.sigmaContainer.nativeElement.clientHeight - pos.y < 150) {
					top = this.sigmaContainer.nativeElement.clientHeight / 2;
				}
				else {
					top = nqPlaceholderElement.clientHeight / 2;
				}
				nqPlaceholderElement.style.top = `${top}px`;

				if (this.sigmaContainer.nativeElement.clientWidth - pos.x < 350) {
					left = this.sigmaContainer.nativeElement.clientWidth - (nqPlaceholderElement.clientWidth + 30);
					nqPlaceholderElement.style.left = left + "px";
				} else {
					nqPlaceholderElement.style.left = pos.x + 20 + "px";
				}
			}
		}
	}

	// Al hacer click derecho sobre un nodo o quizz muestra un popover con diferentes acciones
	private subscriberightClickNodeEvent() {
		this.eventSubscriptions['rightclickNodeEvent'] = this.sigmaUtils.getemitrightClickNodeEvent()
			.subscribe(e => {
				if (this.canEdit) {
					$('.popover').hide();
					$('.popover3').hide();
					window.oncontextmenu = function () { return false };
					this.clickNode = false;
					this.overNode = false;
					this.connectMode = false;
					this.rightclickNode = true;
					this.isNode = false
					if (!this.isDragging && e.data.node.id !== 'temp') {
						var nqEditPopoverElement = this.nqEditPopover.nativeElement;
						nqEditPopoverElement.style.display = "block";

						if (e.data.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
							this.isNode = true
							this.infoNodeTitle = ((!e.data.node.label) || (e.data.node.label == '') || (e.data.node.label.length < 1))
								? this.translateService.instant('SIGMACOMPONENT.COURSENONAME')
								: this.translateService.instant('SIGMACOMPONENT.COURSE') + "'" + e.data.node.label + "'";
						}
						else if (e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE) {
							this.infoNodeTitle = ((!e.data.node.quizTittle) || (e.data.node.quizTittle == '') || (e.data.node.quizTittle.length < 1))
								? this.translateService.instant('SIGMACOMPONENT.QUIZNONAME')
								: this.translateService.instant('SIGMACOMPONENT.QUIZ') + "'" + e.data.node.quizTittle + "'";
							this.formulario.patchValue({
								certificadoQuiz: e.data.node.certifiedQuiz === 1 ? true : false
							})
						}
						else if (e.data.node.nodeType === SIGMA_CONSTANTS.STICKER_TYPE) { this.infoNodeTitle = SIGMA_CONSTANTS.STICKER_TYPE }

						var pos = this.sigmaUtils.getMousePos(this.sigmaContainer.nativeElement, e.data.captor);
						nqEditPopoverElement.style.left = pos.x + "px";
						nqEditPopoverElement.style.top = pos.y + "px";
						$('.popover').placement = 'auto';
						localStorage.setItem('nodeData', JSON.stringify(e.data.node));
					}
				}
			});
	}

	// Evita que el click derecho en el lienzo vacío haga algo
	private subscriberightClickStageEvent() {
		this.eventSubscriptions['rightclickStageEvent'] = this.sigmaUtils.getemitrightClickStageEvent()
			.subscribe(e => {
				this.addMode = false
				this.connectMode = false;
				this.rightclickNode = false;
				this.scUtils.hidePopWindows()
			});
	}

	// Controla cuando un nodo se está arrastrando si se encuentra encima de otro o no para mostrar la opción de crear un enlace
	private subscribeDragEvent() {
		this.eventSubscriptions['dragEvent'] = this.sigmaUtils.getemitDragEvent()
			.subscribe(e => {
				$('.popover3').hide();
				this.connectMode = false;
				var nodes = this.sigmaUtils.getNodesOnDropEvent(e);
				let result: NodeOnTopResponse = null

				if (nodes && nodes.length && e.data.node.nodeType !== SIGMA_CONSTANTS.STICKER_TYPE) {
					if (this.canEdit && (result = this.sigmaCanvasService.isNodeOnTopOfAnother(e.data.node, nodes)).value) {

						//If the elment is a node or a quiz, to connect it
						if (result.node.nodeType !== SIGMA_CONSTANTS.STICKER_TYPE) {

							result.node.color = 'red';
							e.data.node.color = 'red';
							this.connectMode = true;

							if (this.sigmaUtils.checkConnection(e.data.node, result.node)) {
								this.connectIcon = 'link_off';
							} else {
								this.connectIcon = 'link'
							}

						}
						else
							this.sigmaUtils.restoreAllOriginalNodeColors()
					}
					else {
						this.sigmaUtils.restoreAllOriginalNodeColors()
					}
				} else {
					this.connectMode = false;
				}
			});
	}

	// Para asociar un nodo con otro nodo
	private subscribeAsociarNodoEvent() {
		this.eventSubscriptions['clickNodeEvent'] = this.sigmaUtils.getemitclickNodeEvent()
			.subscribe(e => {
				this.connectMode = false;
				this.clickNode = true;
				this.rightclickNode = false;
				if (!this.isDragging && !this.isLoading) {
					if (this.buscarNodo) {
						let n1 = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.N1)) as any;
						this.conectarNodosAss(n1, e.data.node);

					}
					else {
						this.conectarNodos(this.connectN1, this.connectN2);
					}
				}
			});
	}

	// Controla cuando se suelta un nodo, si está encima de otro al cual no está conectado muestra un modal para añadir la conexión
	private subscribeDropEvent() {
		this.eventSubscriptions['dropEvent'] = this.sigmaUtils.getemitDropEvent()
			.subscribe(e => {
				$('.popover3').hide();
				this.scUtils.hidePopWindows()
				this.connectMode = false;
				// Recupera si se ha soltado sobre otro para crear una conexion
				let result: NodeOnTopResponse = null
				var nodes = this.sigmaUtils.getNodesOnDropEvent(e);
				if (nodes && nodes.length && e.data.node.nodeType !== SIGMA_CONSTANTS.STICKER_TYPE) {
					if (this.canEdit && (result = this.sigmaCanvasService.isNodeOnTopOfAnother(e.data.node, nodes)).value && result.node.nodeType !== SIGMA_CONSTANTS.STICKER_TYPE) {


						this.connectN1 = { id: e.data.node.id, idOriginal: e.data.node.idOriginal, url: e.data.node.url, label: e.data.node.label };
						this.connectN2 = { id: result.node.id, idOriginal: result.node.idOriginal, url: result.node.url, label: result.node.label };

						//Revisar cuando se crea uno nuevo
						if (!this.sigmaUtils.checkConnection(e.data.node, result.node) && this.loginService.esAutor() && this.canEdit) {

							if (e.data.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE && result.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
								this.conectarNodos(this.connectN1, this.connectN2);
							} else {

								if (this.scUtils.checkAlternativeQuizzes(e.data.node, result.node, this.sigmaUtils.sigma.graph.nodes(), this.sigmaUtils.sigma.graph.edges())) {

									e.data.node.x = this.startDragCoords.x;
									e.data.node.y = this.startDragCoords.y;
									this.sigmaUtils.restoreAllOriginalNodeColors()
									this.sigmaUtils.refresh();
									return false
								}

								else {

									this.sigmaCanvasService.connectQuizzes(e.data.node, result.node)
										.subscribe(
											res => {
												res.data != null ? e.data.node.ordinal = res.data : 0;
												if (this.loginService.getUser().tutorialSW.estado == Estado.PENDIENTE) {
													if (!this.tutorialGrafo.quizQuiz && e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE && result.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE) {
														this.tutorialCanvasGrafo(6);
													}
												}
												this.toaster.success(this.translateService.instant('NODOS.LINKOK'));
												this.sigmaUtils.restoreAllOriginalNodeColors()
											},
											err => {
												this.toaster.error(this.translateService.instant('NODOS.LINKNOK'))
												this.sigmaUtils.restoreAllOriginalNodeColors()
											}

										);
								}
							}
						} else if (this.canEdit) {
							this.sigmaCanvasService.deleteEdge(e.data.node, result.node)
								.pipe(
									finalize(() => this.sigmaUtils.restoreAllOriginalNodeColors())
								)
								.subscribe(
									(res: any) => {
										this.toaster.success(this.translateService.instant('NODOS.UNLINKOK'));
									},
									err => {
										this.toaster.error(this.translateService.instant('NODOS.UNLINKNOK'));

									}
								);
						}
						if (this.startDragCoords !== undefined) {
							e.data.node.x = this.startDragCoords.x;
							e.data.node.y = this.startDragCoords.y;
							this.sigmaUtils.refresh();
						}
					}
					else
						this.sigmaUtils.restoreAllOriginalNodeColors()
				}
				else
					this.sigmaUtils.restoreAllOriginalNodeColors()

				this.startDragCoords = undefined;

				//UPDATE ELEMENT POSITION
				if (this.canEdit) {
					this.sigmaCanvasService.savePosition(e.data.node.idOriginal, e.data.node.x, e.data.node.y, e.data.node.nodeType, this.loginService.getUser().idUser, e.data.node);
				}
			});
	}

	// Almacena las coordenadas originales de un nodo cuando se empieza a arrastrar
	private subscribeStartDragEvent() {
		this.eventSubscriptions['startDragEvent'] = this.sigmaUtils.getemitstartDragEvent()
			.subscribe(e => {
				this.startDragCoords = { x: e.data.node.x, y: e.data.node.y };
			});
	}

	// ---------------------------------------
	//  ACTIONS
	// ---------------------------------------

	getActionFromRightMenu(action: ActionModel) {

		switch (action.name) {
			case ACTIONS.SHOWHIDEPADS:
				if (action.value) {
					if (this.isShowLabels)
						this.showHideLabels(true)
					else
						this.showHideLabels(false)
				}
				this.isShowPads = this.utils.padsStatus.showPadsGraph = action.value
				this.utils.savePadsLabelsStatus(LOCALSTORAGESTRINGS.PADSLOCALCATEGORY)
				break;
			case ACTIONS.SHOWHIDELABELS:
				this.isShowLabels = this.utils.labelsStatus.showLabelsGraph = action.value
				this.utils.savePadsLabelsStatus(LOCALSTORAGESTRINGS.LABELSLOCALCATEGORY)
				break
			case ACTIONS.PRINT:
				this.sigmaUtils.adjustGraphToPrint()
				setTimeout(() => {
					window.print()
					this.sigmaUtils.centerGraph()
				}, 0)
				break
		}
	}

	getActionFromPads(event: ActionModel) {
		switch (event.name) {
			case ACTIONS.ZOOMIN:
				this.sigmaUtils.zoomIn()
				break
			case ACTIONS.ZOOMOUT:
				this.sigmaUtils.zoomOut()
				break
			case ACTIONS.CENTERGRAPH:
				this.sigmaUtils.centerGraph();
		}
	}

	// --------------------------------------
	// TOOLBAR MENU RECEIVED EVENTS
	// --------------------------------------

	private changeView($event) {

		if ($event.nodes !== this.viewType.nodes) {
			this.scUtils.changeView(SIGMA_CONSTANTS.NODE_TYPE, $event.nodes, this.sigmaUtils)
		}
		if ($event.quizzes !== this.viewType.quizzes) {
			this.scUtils.changeView(SIGMA_CONSTANTS.QUIZ_TYPE, $event.quizzes, this.sigmaUtils)
		}
		this.viewType = $event;
	}

	onRightClick() {
		// Disable context menu.
		return false;
	}

	onResize($event) {
		if (this.showStaircase)
			this.scUtils.viewStaircaseChart(this.showStaircase, this.sigmaUtils, this.viewType);
	}

	public registrarNodo() {
		this.nodeUtils.registrarNodo().subscribe(res => {
			switch (res.type) {
				case HttpEventType.Response:
					this.sigmaUtils.dropNode('temp');
					this.sigmaUtils.addNode(res.body.data);
					if (this.loginService.getUser().tutorialSW.estado === Estado.PENDIENTE) {
						this.countHelp++;
						if (this.countHelp === 2) {
							this.mostrarAyuda = true;
						}
					}
					this.editNode(res.body.data, true);
			}
		}, err => {
			this.toaster.error(this.translateService.instant('NODEFORMCOMPONENT.ERROR'));
		})
	}

	public createQuizMultiple() {
		this.scUtils.hidePopWindows()

		var tempSigmaNode = this.sigmaUtils.sigma.graph.nodes('temp');
		tempSigmaNode.nodeType = SIGMA_CONSTANTS.QUIZ_TYPE;
		tempSigmaNode.type = SIGMA_CONSTANTS.QUIZ_NODE_TYPE;
		tempSigmaNode.quizTittle = this.translateService.instant('QUIZZES.DEFAULTTITLEMULTIPLE');
		tempSigmaNode.isMultiplexed = MULTIPLEXEDQUIZZES.ISMULTIPLEXED;
		this.quizService.createQuiz(this.idCurso, this.idMapa, tempSigmaNode).pipe(map((res: any) => res.data)).subscribe(res => {
			this.sigmaUtils.dropNode('temp');
			this.sigmaUtils.addNode(res);
			this.editQuizMultiple(res);
		});
	}

	private editQuizMultiple(quiz: any) {
		this.isModalOpen = true
		this.scUtils.hidePopWindows()
		this.quizService.getQuizMultiple(quiz.idOriginal, this.idCurso, this.idMapa).subscribe(res => {
			const modalRef = this.modalService.open(QuizEditMultipleComponent,
				{
					scrollable: false,
					windowClass: MODAL_DIALOG_TYPES.W100,
					backdrop: 'static',
				}
			);

			modalRef.componentInstance.quiz = { ...res.quiz, user: quiz.user, idOriginal: quiz.idOriginal, id: quiz.id, ordinal: quiz.ordinal, originalX: quiz.originalX, originalY: quiz.originalY, size: quiz.size, sizeQuiz: quiz.sizeQuiz, x: quiz.x, y: quiz.y };
			modalRef.componentInstance.elements = res.elements;
			modalRef.componentInstance.quizFiles = res.quizFiles;
			modalRef.componentInstance.quizzes = res.quizzes;
			modalRef.componentInstance.courseId = this.idCurso;
			modalRef.componentInstance.graphId = this.idMapa;


			modalRef.result.then().finally(() => this.isModalOpen = false);
		});
	}

	public createQuiz() {
		this.quizUtils.createQuiz().subscribe(res => {

			this.sigmaUtils.dropNode('temp');
			this.sigmaUtils.addNode(res);
			if (this.loginService.getUser().tutorialSW.estado === Estado.PENDIENTE) {
				this.countHelp++;
				this.countHelpQuiz++;
				if (this.countHelp === 2) {
					this.mostrarAyuda = true;
				}
			}
			this.isModalOpen = true
			this.quizUtils.editQuiz(res, this.countHelpQuiz).finally(() => this.isModalOpen = false)
		})
	}

	private playQuiz(quiz) {
		this.scUtils.hidePopWindows()
		this.customModeAuto('quiz', quiz)
	}

	private conectarNodos(connectN1: NodeConnectModel, connectN2: NodeConnectModel, options?: OptionsNodeConnectModel | null) {
		const modalRef = this.modalService.open(NodeLinkComponent,
			{
				scrollable: true,
				windowClass: MODAL_DIALOG_TYPES.W65
			}
		);
		modalRef.componentInstance.nodeFrom = connectN1;
		modalRef.componentInstance.nodeTo = connectN2;
		modalRef.componentInstance.courseId = this.idCurso;
		modalRef.componentInstance.graphId = this.idMapa;
		modalRef.componentInstance.options = options || null
		modalRef.result.then((response) => {
			this.buscarNodo = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.SEARCHNODE));
			if (this.buscarNodo) {
				localStorage.setItem(LOCALSTORAGESTRINGS.SEARCHNODE, 'false');
				this.initComponent();
			}
			switch (response.result) {
				case 'OK':
					if (!options)
						this.nodeUtils.conectarNodo(response.datos, this.sigmaUtils)
					else
						this.sigmaUtils.updateEdge(options.id, response.datos) //Actualizar el edge
					this.sigmaUtils.restoreAllOriginalNodeColors()
					break;
				case 'ERROR':
					this.sigmaUtils.restoreAllOriginalNodeColors()
					break;
			}
		}, (reason) => {
			this.sigmaUtils.restoreAllOriginalNodeColors()
		}

		);
	}
	private conectarNodosAss(connectN1: any, connectN2: any) {
		const modalRef = this.modalService.open(NodeLinkComponent,
			{
				scrollable: true,
				windowClass: MODAL_DIALOG_TYPES.W65
			}
		);
		modalRef.componentInstance.nodeFrom = connectN1;
		modalRef.componentInstance.nodeTo = connectN2;
		modalRef.componentInstance.courseId = this.idCurso;
		modalRef.componentInstance.graphId = this.idMapa;
		modalRef.result.then((response) => {
			this.buscarNodo = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.SEARCHNODE));
			localStorage.setItem(LOCALSTORAGESTRINGS.SEARCHNODE, 'false');
			this.initComponent();
			const id = Math.floor(Math.random() * (10000));
			// Y lo mando a dashboard
			this.router.navigate(['/dashboard', id]);
			let idNode_Course: number = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.COURSEID))
			let idNode_Target: number = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.GRAPHID))
			this.router.navigate([`/course/${idNode_Course}/graph/${idNode_Target}`]);
		}, (reason) => {
			this.sigmaUtils.restoreAllOriginalNodeColors()
		}

		);
	}

	public editNode(node: gNode, isNewNode?: boolean) {
		this.isModalOpen = true
		this.nodeUtils.editNode(node, isNewNode, this.canEdit, this.mostrarAyuda, this.tutorialGrafo).then(value => {
			if (value)
				this.tutorialCanvasGrafo(value)

		}).finally(() => this.isModalOpen = false)
	}

	public playNode(node: gNode) {
		this.scUtils.hidePopWindows();
		this.registerNodeLastTable(node);
		this.customModeAuto('node', node)
	}
	registerNodeLastTable(node: gNode) {
		this.nodeService.registerLastNodeTable(this.idCurso, this.idMapa, this.loginService.getUser().idUser, node.idOriginal).subscribe(res => { })
	}

	public editElement() {
		var e: any = JSON.parse(localStorage.getItem('nodeData'));
		if (!this.isLoading) {
			if (e.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
				this.editNode(e);
			} else if (e.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE) {
				if (this.canEdit) {

					if (e.isMultiplexed === MULTIPLEXEDQUIZZES.ISMULTIPLEXED) {
						this.editQuizMultiple(e);
					}
					else {
						this.isModalOpen = true
						this.quizUtils.editQuiz(e, this.countHelpQuiz).finally(() => this.isModalOpen = false)
					}
				} else {
					this.playQuiz(e);
				}
			}
			else if (e.nodeType === SIGMA_CONSTANTS.STICKER_TYPE) {
				if (this.canEdit)
					this.stickerUtils.editSticker(e)
			}
			else if (e.nodeType === SIGMA_CONSTANTS.TEXT_TYPE) {
				if (this.canEdit)
					this.stickerUtils.editText(e)
			}
		}
	}

	// ----------------------------------------------------------------------
	//   M A P     A C T I O N S
	// ----------------------------------------------------------------------

	public newImage(file) {
		this.bgUtils.addBackground(file)
		this.bgUtils.progress.subscribe(value => {
			this.progress = value
			if (this.progress == 100) {
				setTimeout(() => {
					this.progress = 0
				}, 500);
			}
		})
	}

	public removeBackground() {
		this.bgUtils.rmBackground()
	}

	openModalModeAuto() {
		this.scUtils.openModalModeAuto(this.ModoAuto, this.ordenSeleccionado, this.viewType);
	}

	public customModeAuto(type: string, value: gNode | any) {

		let listQuiz = [];

		if (this.viewType.nodes && this.viewType.quizzes) {
			//Obtener listado de un power especifico.
			listQuiz = this.ModoAuto.obtenerListado(this.ordenSeleccionado);

			//para poder abris solo los quizzes cuando no hay nodos en el grafo
			if (listQuiz.length === 0) {
				listQuiz = this.ModoAuto.listadoSoloQuiz();
			}
		}
		else if (this.viewType.nodes) {
			listQuiz = this.ModoAuto.listSoloExplicaciones();
		}
		else if (this.viewType.quizzes) {
			listQuiz = this.ModoAuto.listadoSoloQuiz();
		}

		if (listQuiz.length > 0) {

			if (type == SIGMA_CONSTANTS.NODE_TYPE.toLowerCase()) {

				this.nodeService.getNode(value.idOriginal, this.idCurso, this.idMapa).pipe(map((res: any) => res.data[0]), finalize(() => this.isLoading = false)).subscribe(res => {

					const modalRef = this.modalService.open(NodeModeAutoComponent,
						{
							scrollable: true,
							windowClass: MODAL_DIALOG_TYPES.W100,
							backdrop: 'static'
						}
					);

					modalRef.componentInstance.listQuiz = [];
					modalRef.componentInstance.listQuiz = listQuiz;
					modalRef.componentInstance.node = res;
					modalRef.componentInstance.hideBar = true;
					modalRef.componentInstance.idCurso = this.idCurso;
					modalRef.componentInstance.idMapa = this.idMapa;
					modalRef.componentInstance.viewQuiz = this.viewType.quizzes && !this.viewType.nodes ? true : false;;
					modalRef.componentInstance.soloQuiz = this.viewType.quizzes && !this.viewType.nodes ? true : false;
					modalRef.componentInstance.soloNodos = !this.viewType.quizzes && this.viewType.nodes ? true : false;
					modalRef.componentInstance.autoTodo = this.viewType.quizzes && this.viewType.nodes ? true : false;
					modalRef.componentInstance.currentGraph = listQuiz;
					modalRef.componentInstance.type = SIGMA_CONSTANTS.NODE_TYPE.toLowerCase();
					modalRef.componentInstance.course = this.sigmaCanvasService.course;
					modalRef.componentInstance.adjustWindow = true
					modalRef.componentInstance.allItems = this.ModoAuto
					modalRef.componentInstance.quiz = QuizModel;

					modalRef.result.then((res) => {
						if (res !== undefined) {
							this.lastNodeId = res.id
							this.actionInCanvas(this.lastNodeId.toString(), this.anteriorNodo = undefined)
						}

					}).catch(res => { })
				});



			}
			else if (type == SIGMA_CONSTANTS.QUIZ_TYPE.toLowerCase()) {
				if (value.isMultiplexed === MULTIPLEXEDQUIZZES.ISMULTIPLEXED) {
					this.quizService.getQuizMultipleCanvasQuizMultiple(value.idOriginal, this.idCurso, this.idMapa).pipe(finalize(() => this.isLoading = false)).subscribe(res => {
						const modalRef = this.modalService.open(NodeModeAutoComponent,
							{
								scrollable: true,
								windowClass: MODAL_DIALOG_TYPES.W100,
								backdrop: 'static'
							}
						);

						modalRef.componentInstance.quiz = { ...res.quiz, user: value.user, idOriginal: value.idOriginal, id: value.id, originalX: value.originalX, originalY: value.originalY, size: value.size, sizeQuiz: value.sizeQuiz, x: value.x, y: value.y };;
						modalRef.componentInstance.elements = res.elements;
						modalRef.componentInstance.courseId = this.idCurso;
						modalRef.componentInstance.graphId = this.idMapa;
						modalRef.componentInstance.hideBar = true;
						modalRef.componentInstance.listQuiz = [];
						modalRef.componentInstance.listQuiz = listQuiz;
						modalRef.componentInstance.firstQuiz = 1
						modalRef.componentInstance.idCurso = this.idCurso;
						modalRef.componentInstance.idMapa = this.idMapa;
						modalRef.componentInstance.viewQuiz = true
						modalRef.componentInstance.soloQuiz = this.viewType.quizzes && !this.viewType.nodes ? true : false;
						modalRef.componentInstance.soloNodos = !this.viewType.quizzes && this.viewType.nodes ? true : false;
						modalRef.componentInstance.autoTodo = this.viewType.quizzes && this.viewType.nodes ? true : false;
						modalRef.componentInstance.currentGraph = listQuiz;
						modalRef.componentInstance.type = SIGMA_CONSTANTS.QUIZ_TYPE.toLowerCase();
						modalRef.componentInstance.adjustWindow = true
						modalRef.componentInstance.course = this.sigmaCanvasService.course;
						modalRef.componentInstance.allItems = this.ModoAuto

						modalRef.result.then(color => { })
							.catch(res => { })
					});
				}
				else {
					this.quizService.getQuizSimpleCanvasQuizSimple(value.idQuiz? value.idQuiz : value.idOriginal, this.sigmaCanvasService.courseId, this.sigmaCanvasService.graphId).pipe(finalize(() => this.isLoading = false)).subscribe(res => {
						const modalRef = this.modalService.open(NodeModeAutoComponent,
							{
								scrollable: true,
								windowClass: MODAL_DIALOG_TYPES.W100,
								backdrop: 'static'
							}
						);

						modalRef.componentInstance.quiz = { ...res.quiz, promptText: res.promptText, user: value.user, idOriginal: value.idOriginal, id: value.id, originalX: value.originalX, originalY: value.originalY, size: value.size, sizeQuiz: value.sizeQuiz, x: value.x, y: value.y };
						modalRef.componentInstance.rawQuiz = res;
						modalRef.componentInstance.elements = res.elements;
						modalRef.componentInstance.courseId = this.idCurso;
						modalRef.componentInstance.graphId = this.idMapa;
						modalRef.componentInstance.hideBar = true;
						modalRef.componentInstance.listQuiz = [];
						modalRef.componentInstance.listQuiz = listQuiz;
						modalRef.componentInstance.firstQuiz = 1
						modalRef.componentInstance.idCurso = this.idCurso;
						modalRef.componentInstance.idMapa = this.idMapa;
						modalRef.componentInstance.viewQuiz = true
						modalRef.componentInstance.soloQuiz = this.viewType.quizzes && !this.viewType.nodes ? true : false;
						modalRef.componentInstance.soloNodos = !this.viewType.quizzes && this.viewType.nodes ? true : false;
						modalRef.componentInstance.autoTodo = this.viewType.quizzes && this.viewType.nodes ? true : false;
						modalRef.componentInstance.currentGraph = listQuiz;
						modalRef.componentInstance.type = SIGMA_CONSTANTS.QUIZ_TYPE.toLowerCase();
						modalRef.componentInstance.adjustWindow = true
						modalRef.componentInstance.course = this.sigmaCanvasService.course;
						modalRef.componentInstance.allItems = this.ModoAuto

						modalRef.result.then(color => { })
							.catch(res => { })
					});
				}
			}
		}
		else
			this.toaster.success(this.translateService.instant('SIGMACOMPONENT.ERRORCONFIG'))
	}

	private subscribeChangedProfile(): void {
		this._profileSubscription = this.loginService.currentProfile.subscribe(val => {
			if (val !== null) {
				if (val.profile !== undefined) {
					if ((val.profile.toLowerCase() === Profiles.Student.toLowerCase() || val.profile.toLowerCase() === Profiles.Author.toLowerCase()) && val.sendEvent) {
						if (this._sizeInterval)
							clearInterval(this._sizeInterval)
						this.initComponent(val.sendEvent)
					}
				}
			}
		})
	}

	private showHideLabels(value: boolean) {
		this.isShowLabels = false
		setTimeout(() => {
			this.isShowLabels = this.utils.labelsStatus.showLabelsGraph = value
			this.utils.savePadsLabelsStatus(LOCALSTORAGESTRINGS.LABELSLOCALCATEGORY)
		}, 1200);
	}

	asociarNodo(nodo) {
		this.buscarNodo = true
		this.nodeUtils.associateNode(nodo, this.idCurso, this.idMapa, this.canEdit)
	}

	private initComponent(isKillSigma?: boolean) {

		if (isKillSigma) {
			this.sigmaUtils.sigma.kill()
			this._showModalSubscription.unsubscribe()
			this._updateGraph.unsubscribe()
		}

		this.loading = true;
		this.sigmaUtils = new SigmaUtils(this.sigmaContainer.nativeElement, this.nodeCoverPipe, this.loginService.getProfile() ? this.loginService.getProfile().toUpperCase() : '', this.utils, this.loginService);
		this.sigmaCanvasService.courseId = this.idCurso;
		this.sigmaCanvasService.graphId = this.idMapa;
		this.sigmaCanvasService.sigmaUtils = this.sigmaUtils;

		try {


			this.courseService.getCourseById(this.idCurso).subscribe(res => {
				this.canEdit = this.scUtils.userCanEditGraph(res.data.editores)
				this.sigmaCanvasService.course = res.data.courses
			})
			const obs1$ = this.graphServ.getNodesFromCourseGraph(this.idCurso, this.idMapa)
			//const obs2$ = this.loginService.esAutor() ? this.graphServ.getQuizzesFromCourseGraph(this.idCurso, this.idMapa) : this.graphServ.getQuizzesFromCourseGraphRoleStudent(this.idCurso, this.idMapa);
			let storedGroupInfo = JSON.parse(localStorage.getItem('selectedGroupInfo'));
			const idGrupo = storedGroupInfo ? storedGroupInfo.idGroup : 0;
			const obs2$ = this.loginService.esEstudiante() ? this.graphServ.getQuizzesFromCourseGraphRoleStudent(this.idCurso, this.idMapa, idGrupo): this.graphServ.getQuizzesFromCourseGraph(this.idCurso, this.idMapa);
			const obs3$ = this.stickersServices.loadStickers(this.idCurso, this.idMapa)
			const obs4$ = this.masterServices.getVersionApi()

			const CALLS = forkJoin(obs1$, obs2$, obs3$, obs4$)

			CALLS.subscribe(([o1, o2, o3, o4]) => {
				const res = { nodes: o1.nodes.concat(o2.nodes), edges: o1.edges.concat(o2.edges), stickers: o3 }

				this.currentGraph = { ...res };
				this.listQuiz = { ...this.currentGraph };
				this.modAutoPrototype(this.listQuiz);
				this.sigmaUtils.addGraph(this.currentGraph);
				this.sigmaUtils.centerGraph();
				this.version = o4.data

				//If the profile of the current user is Student
				if (this.loginService.esEstudiante()) {
					let view = { nodes: true, quizzes: true }
					this.toolsService.changeVisibility(view)
					this.changeView(view)
					this.quizService.setAutomatic(true);
					this.setViewType();
				}
				if (this.currentGraph.nodes.length || this.currentGraph.stickers.length)
					this.sigmaUtils.setZoomAnimation(SIGMA_CONSTANTS.MAXRATIO)

				this.sigmaUtils.sigma.settings('enableEdgeHovering', this.loginService.esAutor() ? true : false)

			}, err => {
				console.error('ERROR LOADING GRAPH: ', err)
				this.currentGraph = { nodes: [], edges: [], stickers: [] }
				this.loading = false
			})

		} catch (error) {
			console.error("ERROR LOADING GRAPH: ", error)
		} finally {
			setTimeout(() => {
				this.sigmaUtils.refresh();
			}, 100);
			setTimeout(() => {
				this.loading = false
				this.isShowPads = this.utils.padsStatus.showPadsGraph

				this.isShowLabels = this.utils.labelsStatus.showLabelsGraph
				this.showHideLabels(this.isShowLabels)

			}, 100)
		}

		this.buscarNodo = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.SEARCHNODE));
		this.modoTraerNodo = JSON.parse(localStorage.getItem('modoTraerNodo'));
		this.modoTraerActividad = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.GETACTIVITY));
		if (!this.buscarNodo && !this.modoTraerNodo && !this.modoTraerActividad) {
			this.subscribeToolbars();

			//POWER
			this.subscribePowerActive();

			// JQUERY Events
			this.subscribejQueryClickEvent();
			this.subscribejQueryMouseMoveEvent();
			this.jqueryContextMenu();
			this.subscribeJQueryMouseUp();
			this.subscribeJQueryMouseDown();
			// SIGMAJS Events
			this.subscribeClickNodeEvent();
			this.subscribedoubleClickNodeEvent();
			this.subscribeClickStageEvent();
			this.subscribeOutNodeEvent();
			this.subscribeOverNodeEvent();
			//this.subscribeDoubleClickStageEvent();
			this.subscriberightClickNodeEvent();
			this.subscriberightClickStageEvent();
			this.subscribeDragEvent();
			this.subscribeDropEvent();
			this.subscribeStartDragEvent();

			// CAMERA events
			this.subscribeUpdateCoords();

			// DATABASE subscriptions
			this.subscribeNewNode();
			this.subscribeNewQuiz();

			this.subscribeClickEdgeEvent()
			this.subscribeOutEdgeEvent()
			this.subscribeOverEdgeEvent()

		}
		else if (this.buscarNodo) {
			this.subscribeJQueryMouseUp();
			this.subscribeJQueryMouseDown();
			this.subscribeOutNodeEvent();
			this.subscribeOverNodeEvent();
			this.subscribeAsociarNodoEvent();
		}
		else if (this.modoTraerNodo) {
			this.subscribeJQueryMouseUp();
			this.subscribeJQueryMouseDown();
			this.subscribeOutNodeEvent();
			this.subscribeOverNodeEvent();
			this.subscribeAlquilarIdea();
		}
		else if (this.modoTraerActividad) {
			this.subscribeJQueryMouseUp();
			this.subscribeJQueryMouseDown();
			this.subscribeOutNodeEvent();
			this.subscribeOverNodeEvent();
			this.subscribeAlquilarActividad();
		}

		this._getInfoCourse()

		//PERFIL CAMBIADO
		if (!isKillSigma)
			this.subscribeChangedProfile();

	}
	subscribeAlquilarActividad() {
		this.eventSubscriptions['clickNodeEvent'] = this.sigmaUtils.getemitclickNodeEvent()
			.subscribe(e => {
				this.clickNode = true;
				this.rightclickNode = false;
				if (!this.isDragging && !this.isLoading && e.data.node.nodeType === SIGMA_CONSTANTS.QUIZ_TYPE) {
					this.nodeUtils.openModalActividadAlquilarConfirm(e.data.node)
				}
			});
	}
	subscribeAlquilarIdea() {
		this.eventSubscriptions['clickNodeEvent'] = this.sigmaUtils.getemitclickNodeEvent()
			.subscribe(e => {
				this.clickNode = true;
				this.rightclickNode = false;
				if (!this.isDragging && !this.isLoading && e.data.node.nodeType === SIGMA_CONSTANTS.NODE_TYPE) {
					this.nodeUtils.openModalNodeAlquilarConfirm(e.data.node)
				}
			});
	}


	alquilarIdea() {
		//Guardamos los datos de la url en el locaL storage
		localStorage.setItem(LOCALSTORAGESTRINGS.COURSEID, JSON.stringify(this.idCurso));
		localStorage.setItem(LOCALSTORAGESTRINGS.GRAPHID, JSON.stringify(this.idMapa));
		$('.popover, .popover2, .popover3').hide();
		var tempNode = this.sigmaUtils.sigma.graph.nodes('temp');
		localStorage.setItem(LOCALSTORAGESTRINGS.NODEX, tempNode.x); //Guardamos coordenadas
		localStorage.setItem(LOCALSTORAGESTRINGS.NODEY, tempNode.y); //Guardamos coordenadas
		const modalRef = this.modalService.open(ModalCursosListadoComponent, { scrollable: true, windowClass: MODAL_DIALOG_TYPES.W95 });
		modalRef.componentInstance.modoTraerNodo = true;
		modalRef.componentInstance.modoTraerActividad = false;
	}

	alquilarActividad() {
		//Guardamos los datos de la url en el locaL storage
		localStorage.setItem(LOCALSTORAGESTRINGS.COURSEID, JSON.stringify(this.idCurso));
		localStorage.setItem(LOCALSTORAGESTRINGS.GRAPHID, JSON.stringify(this.idMapa));
		$('.popover, .popover2, .popover3').hide();
		var tempNode = this.sigmaUtils.sigma.graph.nodes('temp');
		localStorage.setItem(LOCALSTORAGESTRINGS.NODEX, tempNode.x); //Guardamos coordenadas
		localStorage.setItem(LOCALSTORAGESTRINGS.NODEY, tempNode.y); //Guardamos coordenadas
		const modalRef = this.modalService.open(ModalCursosListadoComponent, { scrollable: true, windowClass: MODAL_DIALOG_TYPES.W95 });
		modalRef.componentInstance.modoTraerNodo = false;
		modalRef.componentInstance.modoTraerActividad = true;
	}

	certifiedQuiz($ev) {
		$ev.preventDefault()
		$ev.stopImmediatePropagation()
		this.quizUtils.certificarQuiz().subscribe(value => {
			this.formulario.patchValue({ certificadoQuiz: value })
		}, err => {
			this.formulario.patchValue({ certificadoQuiz: false })
		})
	}


	login() {
		location.href = 'https://www.salware.com/auth/login';
	}
	registrarme() {
		location.href = 'https://www.salware.com/auth/sign-up';
	}

	cancelarOperacionAlquilar() {
		localStorage.setItem('modoTraerNodo', 'false');
		localStorage.setItem('modoTraerActividad', 'false');
		this.modoTraerActividad = false;
		this.modoTraerNodo = false;
		let idNode_Course: number = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.COURSEID))
		let idNode_Target: number = JSON.parse(localStorage.getItem(LOCALSTORAGESTRINGS.GRAPHID))
		this.router.navigate([`/course/${idNode_Course}/graph/${idNode_Target}`]);
		this.toaster.success(this.translateService.instant('SIGMACOMPONENT.ALQUILARCANCELADOCORRECTAMENTE'));
	}

	emitshowModalCourse(value: string) {
		this.toolsService.changeShowModalCourses(value);
		this.padsUtils.vibratePad()
		this.toolsService.setHideElements()
	}

	showProfileActionsModal() {
		let dontLoadMenu = this.localStorage.getItem('dontLoadMenu');
		if ((this.utils.loadMenu === true && dontLoadMenu === 'false') || dontLoadMenu == null) {
			this.modalService.open(ModalProfileActionsComponent, { scrollable: false , windowClass: MODAL_DIALOG_TYPES.W80 });
		} else {
			setTimeout(() => {
				localStorage.setItem('dontLoadMenu', 'false')
				this.utils.loadMenu = true
			}, 300);
		}
	}
}
