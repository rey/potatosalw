export interface NodeConnectModel{
    id: string,
    idOriginal: number,
    url: string,
    label: string
}

export interface OptionsNodeConnectModel{
    id: string,
    type: string,
    label:string,
    size: number,
    color: string
}
