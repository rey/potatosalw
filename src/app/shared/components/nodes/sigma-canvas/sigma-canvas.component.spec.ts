import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SigmaCanvasComponent } from './sigma-canvas.component';

describe('SigmaCanvasComponent', () => {
  let component: SigmaCanvasComponent;
  let fixture: ComponentFixture<SigmaCanvasComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SigmaCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigmaCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
