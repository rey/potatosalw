import { TestBed } from '@angular/core/testing';

import { SigmaCanvasService } from './sigma-canvas.service';

describe('SigmaCanvasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SigmaCanvasService = TestBed.get(SigmaCanvasService);
    expect(service).toBeTruthy();
  });
});
