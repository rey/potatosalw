import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SubQuizzesComponent } from './sub-quizzes.component';

describe('SubQuizzesComponent', () => {
  let component: SubQuizzesComponent;
  let fixture: ComponentFixture<SubQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SubQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
