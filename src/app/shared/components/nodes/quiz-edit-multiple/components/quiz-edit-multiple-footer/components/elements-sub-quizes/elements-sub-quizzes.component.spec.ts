import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ElementsSubQuizzesComponent } from './elements-sub-quizzes.component';

describe('ElementsSubQuizzesComponent', () => {
  let component: ElementsSubQuizzesComponent;
  let fixture: ComponentFixture<ElementsSubQuizzesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementsSubQuizzesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementsSubQuizzesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
