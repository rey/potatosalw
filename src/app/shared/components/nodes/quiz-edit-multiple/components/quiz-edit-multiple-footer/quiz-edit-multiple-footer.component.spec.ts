import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditMultipleFooterComponent } from './quiz-edit-multiple-footer.component';

describe('QuizEditMultipleFooterComponent', () => {
  let component: QuizEditMultipleFooterComponent;
  let fixture: ComponentFixture<QuizEditMultipleFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditMultipleFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditMultipleFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
