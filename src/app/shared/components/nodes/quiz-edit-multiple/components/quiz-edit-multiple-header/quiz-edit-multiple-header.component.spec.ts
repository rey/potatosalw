import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditMultipleHeaderComponent } from './quiz-edit-multiple-header.component';

describe('QuizEditMultipleHeaderComponent', () => {
  let component: QuizEditMultipleHeaderComponent;
  let fixture: ComponentFixture<QuizEditMultipleHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditMultipleHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditMultipleHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
