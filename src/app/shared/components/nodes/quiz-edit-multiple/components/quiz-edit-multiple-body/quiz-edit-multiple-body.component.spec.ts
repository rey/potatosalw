import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizEditMultipleBodyComponent } from './quiz-edit-multiple-body.component';

describe('QuizEditMultipleBodyComponent', () => {
  let component: QuizEditMultipleBodyComponent;
  let fixture: ComponentFixture<QuizEditMultipleBodyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizEditMultipleBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizEditMultipleBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
