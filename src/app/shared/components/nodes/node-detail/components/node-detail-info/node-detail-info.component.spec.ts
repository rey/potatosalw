import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailInfoComponent } from './node-detail-info.component';

describe('NodeDetailInfoComponent', () => {
  let component: NodeDetailInfoComponent;
  let fixture: ComponentFixture<NodeDetailInfoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
