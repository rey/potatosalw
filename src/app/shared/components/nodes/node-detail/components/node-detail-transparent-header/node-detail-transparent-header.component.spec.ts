import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailTransparentHeaderComponent } from './node-detail-transparent-header.component';

describe('NodeDetailTransparentHeaderComponent', () => {
  let component: NodeDetailTransparentHeaderComponent;
  let fixture: ComponentFixture<NodeDetailTransparentHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailTransparentHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailTransparentHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
