import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeColumnFilesComponent } from './node-column-files.component';

describe('NodeColumnFilesComponent', () => {
  let component: NodeColumnFilesComponent;
  let fixture: ComponentFixture<NodeColumnFilesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeColumnFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeColumnFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
