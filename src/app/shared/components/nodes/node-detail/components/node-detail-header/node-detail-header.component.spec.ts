import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailHeaderComponent } from './node-detail-header.component';

describe('NodeDetailHeaderComponent', () => {
  let component: NodeDetailHeaderComponent;
  let fixture: ComponentFixture<NodeDetailHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
