import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalInstagramVideoComponent } from './modal-instagram-video.component';

describe('ModalInstagramVideoComponent', () => {
  let component: ModalInstagramVideoComponent;
  let fixture: ComponentFixture<ModalInstagramVideoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalInstagramVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalInstagramVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
