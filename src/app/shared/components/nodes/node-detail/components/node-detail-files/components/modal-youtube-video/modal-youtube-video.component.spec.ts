import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalYoutubeVideoComponent } from './modal-youtube-video.component';

describe('ModalYoutubeVideoComponent', () => {
  let component: ModalYoutubeVideoComponent;
  let fixture: ComponentFixture<ModalYoutubeVideoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalYoutubeVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalYoutubeVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
