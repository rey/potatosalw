import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailFilesComponent } from './node-detail-files.component';

describe('NodeDetailFilesComponent', () => {
  let component: NodeDetailFilesComponent;
  let fixture: ComponentFixture<NodeDetailFilesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
