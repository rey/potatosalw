import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailFilesTextEditComponent } from './node-detail-files-text-edit.component';

describe('NodeDetailFilesTextEditComponent', () => {
  let component: NodeDetailFilesTextEditComponent;
  let fixture: ComponentFixture<NodeDetailFilesTextEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailFilesTextEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailFilesTextEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
