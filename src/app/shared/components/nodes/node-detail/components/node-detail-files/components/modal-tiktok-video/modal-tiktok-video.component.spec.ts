import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalTiktokVideoComponent } from './modal-tiktok-video.component';

describe('ModalTiktokVideoComponent', () => {
  let component: ModalTiktokVideoComponent;
  let fixture: ComponentFixture<ModalTiktokVideoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTiktokVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTiktokVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
