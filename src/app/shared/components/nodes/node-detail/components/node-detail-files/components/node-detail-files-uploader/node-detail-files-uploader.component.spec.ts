import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailFilesUploaderComponent } from './node-detail-files-uploader.component';

describe('NodeDetailFilesUploaderComponent', () => {
  let component: NodeDetailFilesUploaderComponent;
  let fixture: ComponentFixture<NodeDetailFilesUploaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailFilesUploaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailFilesUploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
