import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodeDetailFilesPlayerComponent } from './node-detail-files-player.component';

describe('NodeDetailFilesPlayerComponent', () => {
  let component: NodeDetailFilesPlayerComponent;
  let fixture: ComponentFixture<NodeDetailFilesPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeDetailFilesPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeDetailFilesPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
