import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPadButtonsComponent } from './quiz-pad-buttons.component';

describe('QuizPadButtonsComponent', () => {
  let component: QuizPadButtonsComponent;
  let fixture: ComponentFixture<QuizPadButtonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPadButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPadButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
