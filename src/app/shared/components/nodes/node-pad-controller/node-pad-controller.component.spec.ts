import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NodePadControllerComponent } from './node-pad-controller.component';

describe('NodePadControllerComponent', () => {
  let component: NodePadControllerComponent;
  let fixture: ComponentFixture<NodePadControllerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NodePadControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodePadControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
