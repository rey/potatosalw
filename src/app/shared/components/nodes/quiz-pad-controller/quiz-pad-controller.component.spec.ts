import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPadControllerComponent } from './quiz-pad-controller.component';

describe('QuizPadControllerComponent', () => {
  let component: QuizPadControllerComponent;
  let fixture: ComponentFixture<QuizPadControllerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPadControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPadControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
