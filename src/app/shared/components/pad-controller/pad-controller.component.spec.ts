import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PadControllerComponent } from './pad-controller.component';

describe('PadRightComponent', () => {
  let component: PadControllerComponent;
  let fixture: ComponentFixture<PadControllerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PadControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PadControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
