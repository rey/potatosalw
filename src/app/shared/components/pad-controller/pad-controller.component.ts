import { ACTIONS } from 'src/app/core/utils/actions';
import { LoginService } from './../../../core/services/login/login.service';
import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ActionModel } from 'src/app/core/models/shared/actions.model';
import { PowerService } from 'src/app/core/services/power/power.service';
import { SigmaToolbarsService } from 'src/app/core/services/sigma-toolbars/sigma-toolbars.service';
import { PadsUtils } from 'src/app/core/utils/pads.utils';
import { PointNodeService } from 'src/app/core/services/point-node/point-node.service';
import { Subscription } from 'rxjs';
import { ObjetoNodoAuto } from 'src/app/core/models/graph/gNode.model';
import { SigmaCanvasService } from '../nodes/sigma-canvas/sigma-canvas.service';

const MAXVALUE: number = 3
const MINVALUE: number = -3

@Component({
    selector: 'app-pad-controller',
    templateUrl: './pad-controller.component.html',
    styleUrls: ['./pad-controller.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PadControllerComponent implements OnInit, OnDestroy {

    @HostListener('window:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
        if(!this.isModalOpen)
            this.getCurrentKeyboardKey(event.key)
    }

		@Output() action = new EventEmitter<string>()
    @Input() isShow: boolean
    @Input() isShowLabels: boolean = false
    @Input() canEdit: boolean;
    @Input() NodeViewPads: number;
    @Input() listNodeAuto: ObjetoNodoAuto;
    @Input() isModalOpen:boolean = false
		@Input() upAndDownOff:boolean = false;
		@Input() nodeView:boolean = false;
		@Input() nextEnabled: boolean = true;
		@Input() backEnabled: boolean = false;
		@Output() nodeViewStatus = new EventEmitter<boolean>()
    public showPowerSelector: boolean = false;
    public arrayValues: number[] = []
    private _totalSup:number = 0
    private _totalInf:number = 0
    isEnablePowerUp:boolean = false
    isEnablePowerDown:boolean = false

    private subs = new Subscription

    constructor(private powerService: PowerService,
        private pointNodeService: PointNodeService,
        private toolsService: SigmaToolbarsService,
        private loginService:LoginService,
				private sigmaCanvasService: SigmaCanvasService,
        private padsUtils:PadsUtils) {
             this.subs = this.toolsService.hideElements.subscribe(val => {
                 this.showPowerSelector = false
             })
         }

    ngOnInit() {
			console.log(this.listNodeAuto);
			//Si el usuario es autor, marcadas todas las potencias
			if(this.loginService.esAutor()){
					this.arrayValues.push(-3,-2,-1,0,1,2,3);
					this.powerService.emitPowerActive(this.arrayValues)
			}
			else{
					this.arrayValues.push(0);
					let valMax: number = Math.max.apply(Math, this.arrayValues)
					this._updateMoreLessValue(valMax, MAXVALUE, 0)
			}
			this._getCurrentPowers();
			//para activar y desactivar los botones laterales
			this.sigmaCanvasService.getBackEnabled();
    }

    ngOnDestroy(): void {
        if(this.subs)
            this.subs.unsubscribe()
    }

    public get maxPower(): number {
        return Math.max.apply(Math, this.arrayValues);
    }

    /** LATERAL BUTTONS */

    /** POWER RIGHT PAD */

    addPower() {
        let valMax: number = Math.max.apply(Math, this.arrayValues)

        if(this.arrayValues.length === 7)
            valMax -= 1

        this._updateMoreLessValue(valMax, MAXVALUE, 1)
        this._vibratePad()

    }
    removePower() {
        let valMin: number = Math.min.apply(Math, this.arrayValues)
        if(this.arrayValues.length === 7)
            valMin += 1
        this._updateMoreLessValue(valMin, MINVALUE, -1)
        this._vibratePad()
    }

    private _updateMoreLessValue(value: number, limitValue: number, nextValue: number): void {
        let currentValue: number = value

        if (this.arrayValues.length === 0) {
            this.arrayValues.push(0)
        }
        else if (this.arrayValues.length === 1) {

            if (value !== limitValue) {
                currentValue += nextValue
                this.arrayValues.pop()
                this.arrayValues.push(currentValue)
            }
        }
        else {

            if (value !== limitValue) {
                currentValue += nextValue

                this.arrayValues = []
                this.arrayValues.push(currentValue)
            }
            else {
                let tmp: Array<number> = []
                if (nextValue < 0)
                    tmp = this.arrayValues.splice(1, this.arrayValues.length - 1)
                else
                    tmp = this.arrayValues.splice(0, this.arrayValues.length - 1)
            }
        }

        this.arrayValues = [...this.arrayValues];


        if(this.arrayValues.length === 1){

            if(this._totalSup && !this._totalInf){
                if(this.arrayValues[0] > 0)
                    this.isEnablePowerDown = true
                else
                    this.isEnablePowerDown = false
            }

            else if(!this._totalSup && this._totalInf){
                if(this.arrayValues[0] < 0)
                    this.isEnablePowerUp = true
                else
                    this.isEnablePowerUp = false
            }
        }

        if (value !== limitValue)
            this.powerService.emitPowerActive(this.arrayValues); //Lanza el evento al padre para recoger los valores del power.
    }

    private _vibratePad():void{
        this.padsUtils.vibratePad()
    }

    onArrayValuesChanged(newValues: number[]) {
        this.arrayValues = [...newValues];
        if(this.arrayValues.length > 1)
            this._getCurrentPowers()
        this.powerService.emitPowerActive(this.arrayValues);
    }

     /**
     * Moverse al siguiente nodo o el anterior
     */
    pointNodeAction(avanzar, regresar) {
        const nextBackNode = { avanzar: avanzar, regresar: regresar };
				if(avanzar == true && this.nextEnabled){
					this.action.emit('next')
					this.pointNodeService.emitNextPointNodeActive(nextBackNode);
        	this._vibratePad();
					this.backEnabled = true;
					this.sigmaCanvasService.getNextEnabled().subscribe(res => {
						res ? this.nextEnabled = true : this.nextEnabled = false;
					})
				} else if(regresar == true && this.backEnabled){
					this.action.emit('back')
					this.pointNodeService.emitNextPointNodeActive(nextBackNode);
        	this._vibratePad();
					this.sigmaCanvasService.getBackEnabled().subscribe(res => {
						res ? this.backEnabled = true : this.backEnabled = false;
					})
				}
    }

    private _getCurrentPowers(){
			if(this.listNodeAuto != undefined){
				let power1 = this.listNodeAuto.explicacionesPower1.length
        let power2 = this.listNodeAuto.explicacionesPower2.length
        let power3 = this.listNodeAuto.explicacionesPower3.length
        let powerminus1 = this.listNodeAuto.explicacionesPowerMinus1.length
        let powerminus2 = this.listNodeAuto.explicacionesPowerMinus2.length
        let powerminus3 = this.listNodeAuto.explicacionesPowerMinus3.length

        this._totalSup = power1 + power2 + power3
        this._totalInf = powerminus1 + powerminus2 + powerminus3

        if(this._totalSup > 0)
            this.isEnablePowerUp = true
        if(this._totalInf > 0)
            this.isEnablePowerDown = true
			}

    }

    private getCurrentKeyboardKey(key:string): void{
        switch(key){
            case ACTIONS.ARROWUP:
                this.isEnablePowerUp ? this.addPower() : null
                break
            case ACTIONS.ARROWDOWN:
                this.isEnablePowerDown ? this.removePower() : null
                break;
            case ACTIONS.ARROWRIGHT:
                this.pointNodeAction(true,false)
                break
            case ACTIONS.ARROWLEFT:
                this.pointNodeAction(false,true)
                break
        }
    }

		public viewNodes() {
			this.toolsService.changeVisibility({ nodes: true, quizzes: false });
			this.padsUtils.vibratePad();
	}

	public viewQuizzes() {
			this.toolsService.changeVisibility({ nodes: false, quizzes: true });
			this.padsUtils.vibratePad()
	}
}
