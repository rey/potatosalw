import { selectedProfile } from './../../../store/selectors/profiles.selector';
import { MastersService } from 'src/app/core/services/masters';
import { Component, DoCheck, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Profiles } from 'src/app/core/utils/profiles.enum';
import { TranslateService } from '@ngx-translate/core';
import { ModalListadoEstudiantesComponent } from '../grupos/modal-listado-estudiantes/modal-listado-estudiantes.component';
import { MODAL_DIALOG_TYPES } from 'src/app/core/utils/modal-dialog-types';
import { GruposService, SOCKETMESSAGES } from 'src/app/core/services/groups/grupos.service';
import { finalize, take, takeUntil } from 'rxjs/operators';
import { ModalAceptarCancelarComponent } from '../modal/modal-aceptar-cancelar/modal-aceptar-cancelar.component';
import { ModalListadoCursosComponent } from '../grupos/modal-listado-cursos/modal-listado-cursos.component';
import { ReportingComponent } from '../reporting/reporting.component';
import { SocketService } from 'src/app/core/services/socket/socket-service.service';
import { ModalSocketCoursesComponent } from '../grupos/modal-socket-courses/modal-socket-courses.component';
import { Subject, Subscription } from 'rxjs';
import { User } from 'src/app/core/models/users/user.models';
import { LoginService } from 'src/app/core/services/login';
import { UsersService } from 'src/app/core/services/users';
import { ModalAlertChallengesComponent } from './modal-alert-challenges/modal-alert-challenges.component';
import { SocketQuizPreviewComponent } from '../socket-quiz-preview/socket-quiz-preview.component';
import { ModalAjustesUsuarioComponent } from '../modal-ajustes-usuario/modal-ajustes-usuario.component';
import { TableInformeComponent } from 'src/app/pages/table-informe/table-informe.component';
import { InfoEstudianteComponent } from '../info-estudiante/info-estudiante.component';
import { TableInformeIndividualComponent } from 'src/app/pages/table-informe-individual/table-informe-individual.component';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { loggedInUser } from 'src/app/store/models/user.model';
import { State } from 'src/app/store/models/store.model';
import { QuizzesService } from 'src/app/core/services/quizzes';
import { RecordarQuizPlayComponent } from '../quiz-open/quiz-play/quiz-play.component';
import { RecordarQuizPlayMultipleComponent } from '../quiz-open/quiz-play-multiple/quiz-play-multiple.component';
import { ModalAlertFlashComponent } from './modal-alert-flash/modal-alert-flash.component';

@Component({
	selector: 'app-modal-profile-actions',
	templateUrl: './modal-profile-actions.component.html',
	styleUrls: ['./modal-profile-actions.component.scss']
})
export class ModalProfileActionsComponent implements OnInit, DoCheck {

	profile: any;
	user: User;
	profileSelected: Profiles;
	profileTitle: string;

	numberOfInvited = 0;
	invitacionesList: any;
	cargando = true;
	selectedGroupInfo: any;
	pendingSelectGroup: boolean;
	private subscriptions: Subscription[] = [];

	idGrupo: number;
	isEditor: boolean = false;
	showActInst = false;
	ModoAuto: any;

	private destroy$ = new Subject();

	profileTitles = {
		ESTUDIANTE: 'Estudiante',
		AUTOR: 'Editor',
		PROFESOR: 'Profesor',
		PADRE: 'Padre',
		ADMIN: 'Administrador'
	}
	disableInstantActivityButton: boolean = false;

	constructor(
		public activeModal: NgbActiveModal,
		private MastersService: MastersService,
		public translateService: TranslateService,
		private modalService: NgbModal,
		private groupService: GruposService,
		private toaster: ToasterService,
		private socketService: SocketService,
		public loginService: LoginService,
		public userService: UsersService,
		private store: Store<State>,
		private quizService: QuizzesService,
	) {

	}

	ngOnInit() {
		this.user = this.loginService.getUser()
		this.store.select(store => store.selectedProfile).pipe(takeUntil(this.destroy$)).subscribe((selectedProfile) => {
			this.profile = selectedProfile['selectedProfile']
			this.profileTitle = this.profileTitles[this.profile]
		})
		this._isEditor();
		this.setSelectedGroupInfo();
		this.idGrupo = this.selectedGroupInfo ? this.selectedGroupInfo.idGroup : null;

	}

	ngOnDestroy(): void {
		this.destroy$.next(true);
	}

	ngDoCheck(): void {
		//Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
		//Add 'implements DoCheck' to the class.
		this.setSelectedGroupInfo();
	}

	_isEditor() {
		this.userService.getListUserProfile().subscribe(res => {
			let profiles = res.data;
			for (let i = 0; i < profiles.length; i++) {
				if (profiles[i].idProfile === 1) {
					this.isEditor = true;
					break;
				};
			};
		});
	}

	checkSelectedGroup(moduleToOpen?: string) {
		this.setSelectedGroupInfo();
		if (this.selectedGroupInfo) {
			this.pendingSelectGroup = false;
		} else {
			this.MastersService.openGroups(this.profile);
			const modalRef = this.modalService.open(ModalAceptarCancelarComponent,
				{
					scrollable: true,
					windowClass: MODAL_DIALOG_TYPES.W45
				});
			modalRef.componentInstance.disableCancel = true;
			modalRef.componentInstance.optionalTitle = this.translateService.instant('ALERTCHALLENGENODE.SELECTGROUPTITLE');

			if (this.profile === 'ESTUDIANTE')
			modalRef.componentInstance.mensaje =
				this.profile === 'ESTUDIANTE' ? this.translateService.instant('ALERTCHALLENGENODE.SELECTGROUPMESSAGESTUDENTE') :
				 this.translateService.instant('ALERTCHALLENGENODE.SELECTGROUPMESSAGE');

			modalRef.result.then((result: boolean) => {
				this.disableInstantActivityButton = false;
			});
			this.pendingSelectGroup = true;
		}
	}

	setSelectedGroupInfo() {
		let storedGroupInfo = JSON.parse(localStorage.getItem('selectedGroupInfo'));
		if (storedGroupInfo) {
			if (storedGroupInfo.idUser != this.user.idUser) {
				localStorage.removeItem('selectedGroupInfo');
			}
		}
		this.selectedGroupInfo = JSON.parse(localStorage.getItem('selectedGroupInfo'));
	}

	closeModal(sendData) {
		this.destroy$.next(true);
		this.activeModal.close(sendData);
	}

	goModule(module: string) {
		switch (module) {
			case 'groups':
				this.MastersService.openGroups(this.profile);
				break;
			case 'projects':
				if (this.profile === 'ESTUDIANTE') {
					this.MastersService.verListadoCursos(null, this.profile, 0);
				} else {
					this.MastersService.verListadoCursos(null, this.profile, this.idGrupo);
				}
				break;
			case 'settings':
				this.MastersService.openSettings(this.profile);
				break;
			case 'feedbacks':
				//this.MastersService.informeEstudiante(this.profile);
				// const modalRef = this.modalService.open(InfoEstudianteComponent, { scrollable: false, windowClass: MODAL_DIALOG_TYPES.W100 });
				// modalRef.componentInstance.idGrupo = this.selectedGroupInfo.idGroup;
				// modalRef.componentInstance.group = this.selectedGroupInfo.group;
				break;
			case 'edit':
				//localStorage.setItem('dontLoadMenu', 'true'); //Este provoca que no se muestre el menú al cambiar de perfil --> REVISAR REY
				this.MastersService.verListadoCursos('editar', Profiles.Author);
				this.closeModal(true);
				break;
			case 'new':
				this.MastersService.nuevoCurso(Profiles.Author)
				this.closeModal(true);
			case 'actividadesFlash':
				this.MastersService.verListadoCursosActividadesFlash(null, this.profile);
				break;
		}
	}

	listadoAlumnos() {
		this.checkSelectedGroup('listadoAlumnos');
		if (this.pendingSelectGroup) {
			return;
		} else {
			this.setSelectedGroupInfo();
			const modalRef = this.modalService.open(ModalListadoEstudiantesComponent, { scrollable: false, windowClass: MODAL_DIALOG_TYPES.W80 });
			modalRef.componentInstance.id = this.selectedGroupInfo.idGroup;
			modalRef.componentInstance.group = this.selectedGroupInfo.group;
		}
	}

	listadoCursos() {
		this.checkSelectedGroup();
		if (this.pendingSelectGroup) {
			return;
		} else {
			this.setSelectedGroupInfo();
			const modalRef = this.modalService.open(ModalListadoCursosComponent, { scrollable: false, windowClass: `${MODAL_DIALOG_TYPES.W90} h-100` });
			modalRef.componentInstance.id = this.selectedGroupInfo.idGroup;
			modalRef.componentInstance.group = this.selectedGroupInfo.group;

			modalRef.result.then((result) => {
				if (result) {
					this.listadoAlumnos();
				}
			});
		}
	}

	informesStudentGrupo() {
		this.checkSelectedGroup();
			if (this.pendingSelectGroup) {
				return;
			} else {
				this.setSelectedGroupInfo();
				const modalRef = this.modalService.open(InfoEstudianteComponent, { scrollable: false, windowClass: MODAL_DIALOG_TYPES.W100 });
				modalRef.componentInstance.idGrupo = this.selectedGroupInfo.idGroup;
				modalRef.componentInstance.group = this.selectedGroupInfo.group;
			}
	}

	informesGrupo() {
		this.checkSelectedGroup();
		if (this.pendingSelectGroup) {
			return;
		}
		if (this.selectedGroupInfo.assignedProjects === 0 || this.selectedGroupInfo.assignedStudents === 0) {
			let modal: NgbModalRef
			modal = this.modalService.open(ModalAlertChallengesComponent, { scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W45}` });
			modal.componentInstance.assignedProjects = this.selectedGroupInfo.assignedProjects;
			modal.componentInstance.assignedStudents = this.selectedGroupInfo.assignedStudents;
			modal.componentInstance.action = "report";
			modal.componentInstance.group = this.selectedGroupInfo.group;
			modal.result.then((result) => {
				switch (result) {
					case 1:
						this.listadoCursos();
						break;
					case 2:
						this.listadoCursos();
						break;
					case 3:
						this.listadoAlumnos();
						break;
				}
			}, (reason) => {
			});
		} else {
			this.checkSelectedGroup();
			if (this.pendingSelectGroup) {
				return;
			} else {
				this.setSelectedGroupInfo();
				const modalRef = this.modalService.open(ReportingComponent, { scrollable: false, windowClass: MODAL_DIALOG_TYPES.W100 });
				modalRef.componentInstance.idGrupo = this.selectedGroupInfo.idGroup;
				modalRef.componentInstance.group = this.selectedGroupInfo.group;
			}
		}
	}

	ajustes(){
		this.modalService.open(ModalAjustesUsuarioComponent, { scrollable: true,  windowClass: `${MODAL_DIALOG_TYPES.W95}` });
	}

	instantActivityStudent(): void {
		this.checkSelectedGroup();
		if (this.pendingSelectGroup) {
			return;
		}
		if (this.selectedGroupInfo.assignedProjects === 0 && !this.loginService.esEstudiante) {
			this.toaster.error(this.translateService.instant('INFORMES.WITHOUTPROJECTS'));
		} else{
					//Opción para crear actividades instantáneas. Si es un profesor, se debe crear; si es un estudiante, tiene que poder entrar para conectarse con websocket
		//Si es estudiante, tengo que sacar el modal para esperar los movimientos del websocket
		let modal:NgbModalRef
			const idGroup = this.selectedGroupInfo.idGroup;
			const msgSubs = this.socketService.msgFromServer.pipe(takeUntil(this.destroy$)).subscribe(data => { //Cuando llega un mensaje del socket, lo estamos escuchando aqui y procedemos
			const dataArray = data.split(":")
			const type = dataArray[0]
			const onlineUsers:number = parseInt(dataArray[1])
			switch(type){
				case SOCKETMESSAGES.USERSCOUNTER:
					if(this.loginService.esEstudiante()){ //Si es estudiante, debo abrir el modal de espera para que se vayan abriendo los quizzes según decida el profesor
						this.groupService.getIfSessionExists(this.selectedGroupInfo.idGroup).pipe(takeUntil(this.destroy$), take(1)).subscribe(result => {
							this.cargando = false
							this.showActInst = result.data;
							if (this.showActInst) {
								modal = this.modalService.open(SocketQuizPreviewComponent, {scrollable: false, windowClass: `${MODAL_DIALOG_TYPES.W100} h-100`})
								modal.componentInstance.idGroup = this.selectedGroupInfo.idGroup
								modal.componentInstance.onlineUsers = onlineUsers
								modal.result.then(() => {
									this.destroy$.next(true);
									this._removeSocket(idGroup)
								}, err => {}).finally(() => {})
							} else{
								this.toaster.error(this.translateService.instant('INSTANTACTIVITIES.NOHAYDESAFIOS'));
							}
						})
					}
					else{
						modal = this.modalService.open(ModalSocketCoursesComponent, { scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W90} h-100` }); // Modal con el listado de cursos, para que el profesor pueda elegir los quizzes a realizar
						modal.componentInstance.group = this.selectedGroupInfo;
						modal.componentInstance.onlineUsers = onlineUsers
						modal.result.then(() => {
							this.destroy$.next(true);
							this._removeSocket(idGroup)
						 }, err => {}).finally(() => {})

						msgSubs.unsubscribe()
					}
					break
			}
		})
		this.subscriptions.push(msgSubs)
		this.socketService.createSocket(idGroup)	//Creo el socket
		const openSocketSub = this.socketService.openSocket.subscribe(ev => { //Cuando se cree el socket, procedemos a guardar la sesion en el servidor
			this.groupService.createGroupSession(idGroup).subscribe(result => { //Si se guarda correctamente la sesion, tenemos que abrir el modal con los cursos en modo lista
				this.socketService.idSession = result.data.idSession
				openSocketSub.unsubscribe()
			}, err => {
				//modal.close()
			})
		})
		this.subscriptions.push(openSocketSub)
		}
	}

	instantActivity(): void {
		this.disableInstantActivityButton = true
			//ver si tiene grupo seleccionado si no abrir modal para seleccionar grupo
			this.checkSelectedGroup();
			if (this.pendingSelectGroup) {
				return;
			}
			if (this.selectedGroupInfo.assignedProjects === 0 || this.selectedGroupInfo.assignedStudents === 0) {
				let modal: NgbModalRef
				modal = this.modalService.open(ModalAlertChallengesComponent, { scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W45}` });
				modal.componentInstance.assignedProjects = this.selectedGroupInfo.assignedProjects;
				modal.componentInstance.assignedStudents = this.selectedGroupInfo.assignedStudents;
				modal.componentInstance.action = "challenge";
				modal.componentInstance.group = this.selectedGroupInfo.group;
				modal.result.then((result) => {
					switch (result) {
						case 1:
							this.listadoCursos();
							break;
						case 2:
							this.listadoCursos();
							break;
						case 3:
							this.listadoAlumnos();
							break;
					}
				}, (reason) => {
				});
			} else {
				this.checkSelectedGroup();
				if (this.pendingSelectGroup) {
					return;
				} else {
					this.setSelectedGroupInfo();
					let modal: NgbModalRef
					const idGroup = this.selectedGroupInfo.idGroup
					const msgSubs = this.socketService.msgFromServer.subscribe(data => { //Cuando llega un mensaje del socket, lo estamos escuchando aqui y procedemos
						const dataArray = data.split(":")
						const type = dataArray[0]
						const onlineUsers: number = parseInt(dataArray[1])
						switch (type) {
							case SOCKETMESSAGES.USERSCOUNTER:
								modal = this.modalService.open(ModalSocketCoursesComponent, { scrollable: true, windowClass: `${MODAL_DIALOG_TYPES.W90} h-100` }); // Modal con el listado de cursos, para que el profesor pueda elegir los quizzes a realizar
								modal.componentInstance.group = this.selectedGroupInfo.group;
								modal.componentInstance.onlineUsers = onlineUsers
								modal.componentInstance.fromNode = false;
								modal.result.then(() => { }, err => { }).finally(() => {
									this.disableInstantActivityButton = false;
									this._removeSocket(idGroup)
								})
								msgSubs.unsubscribe()
								break
						}
					})
					this.subscriptions.push(msgSubs)

					this.socketService.createSocket(idGroup)	//Creo el socket

					const openSocketSub = this.socketService.openSocket.subscribe(ev => { //Cuando se cree el socket, procedemos a guardar la sesion en el servidor
						this.groupService.createGroupSession(idGroup).subscribe(result => { //Si se guarda correctamente la sesion, tenemos que abrir el modal con los cursos en modo lista
							this.socketService.idSession = result.data.idSession
							openSocketSub.unsubscribe()
						}, err => {
							modal.close()
						})
					})
					this.subscriptions.push(openSocketSub)
				}
			}
			let modal:NgbModalRef
			const idGroup = this.selectedGroupInfo.idGroup
			const msgSubs = this.socketService.msgFromServer.subscribe( data => { //Cuando llega un mensaje del socket, lo estamos escuchando aqui y procedemos
				const dataArray = data.split(":")
				const type = dataArray[0]
				const onlineUsers:number = parseInt(dataArray[1])
				switch(type){
					case SOCKETMESSAGES.USERSCOUNTER:
						if(this.loginService.esEstudiante()){
							//Si es estudiante, debo abrir el modal de espera para que se vayan abriendo los quizzes según decida el profesor
							this.groupService.getIfSessionExists(idGroup).pipe(finalize( () => this.cargando = false )).subscribe(result =>{
								this.showActInst = result.data
							})
							if(this.showActInst){
								modal = this.modalService.open(SocketQuizPreviewComponent, {scrollable: false, windowClass: `${MODAL_DIALOG_TYPES.W100} h-100`})
								modal.componentInstance.idGroup = idGroup
								modal.componentInstance.onlineUsers = onlineUsers
								modal.result.then(() => { }, err => {}).finally(() => {
									this._removeSocket(idGroup)
								})
								msgSubs.unsubscribe()
								break
							} else{
								this.toaster.error(this.translateService.instant('INSTANTACTIVITIES.NOHAYDESAFIOS'));
							}
							modal.componentInstance.onlineUsers = onlineUsers
							modal.result.then(() => { }, err => {}).finally(() => {
								this._removeSocket(idGroup)
							})
							msgSubs.unsubscribe()
						}
						break
				}
			})
			//COMENTO ESTO POR QUE NO SE QUE ESTABA HACIENDO AQUI PERO SE SOLUCIONA EL ERROR DE DOBLE SESSION EN LA TABLA GROUPSESSIONS
			// this.subscriptions.push(msgSubs)
			// this.socketService.createSocket(idGroup)	//Creo el socket
			// const openSocketSub = this.socketService.openSocket.subscribe(ev => { //Cuando se cree el socket, procedemos a guardar la sesion en el servidor
			// 	this.groupService.createGroupSession(idGroup).subscribe(result => { //Si se guarda correctamente la sesion, tenemos que abrir el modal con los cursos en modo lista
			// 		this.socketService.idSession = result.data.idSession
			// 		openSocketSub.unsubscribe()
			// 	}, err => {
			// 		//modal.close()
			// 	})
			// })
			// this.subscriptions.push(openSocketSub)
	}

	private _removeSocket(idGroup) {
		this.socketService.removeSocket() //Si ha habido error al guardar la sesión, tenemos que destruir el socket y cerrar el modal
		this.groupService.deleteGroupSession(idGroup).subscribe() //Elimino la sesión de la bd
		this.subscriptions.forEach(s => s.unsubscribe()) //Elimino las suscripciones a eventos
	}

	openTable(){
		this.modalService.open(TableInformeComponent, { scrollable: false, windowClass: MODAL_DIALOG_TYPES.W95 });
	}

	openTableIndividual(){
		this.modalService.open(TableInformeIndividualComponent, { scrollable: false, windowClass: MODAL_DIALOG_TYPES.W95 });
	}

}
