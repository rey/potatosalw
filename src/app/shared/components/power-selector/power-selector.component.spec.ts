import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PowerSelectorComponent } from './power-selector.component';

describe('PowerSelectorComponent', () => {
  let component: PowerSelectorComponent;
  let fixture: ComponentFixture<PowerSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
