import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalTutorialGrafoComponent } from './modal-tutorial-grafo.component';

describe('ModalTutorialGrafoComponent', () => {
  let component: ModalTutorialGrafoComponent;
  let fixture: ComponentFixture<ModalTutorialGrafoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTutorialGrafoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTutorialGrafoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
