import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MessageMultiTabsComponent } from './message-multi-tabs.component';

describe('MessageMultiTabsComponent', () => {
  let component: MessageMultiTabsComponent;
  let fixture: ComponentFixture<MessageMultiTabsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageMultiTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageMultiTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
