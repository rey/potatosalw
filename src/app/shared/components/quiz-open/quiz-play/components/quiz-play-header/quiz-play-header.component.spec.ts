import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayHeaderComponent } from './quiz-play-header.component';

describe('QuizPlayHeaderComponent', () => {
  let component: QuizPlayHeaderComponent;
  let fixture: ComponentFixture<QuizPlayHeaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
