import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayBodyAnswerComponent } from './quiz-play-body-answer.component';

describe('QuizPlayBodyAnswerComponent', () => {
  let component: QuizPlayBodyAnswerComponent;
  let fixture: ComponentFixture<QuizPlayBodyAnswerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayBodyAnswerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayBodyAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
