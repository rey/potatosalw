import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayBodyComponent } from './quiz-play-body.component';

describe('QuizPlayBodyComponent', () => {
  let component: QuizPlayBodyComponent;
  let fixture: ComponentFixture<QuizPlayBodyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayBodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
