import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuizPlayBodyOptionComponent } from './quiz-play-body-option.component';

describe('QuizPlayBodyOptionComponent', () => {
  let component: QuizPlayBodyOptionComponent;
  let fixture: ComponentFixture<QuizPlayBodyOptionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPlayBodyOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizPlayBodyOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
