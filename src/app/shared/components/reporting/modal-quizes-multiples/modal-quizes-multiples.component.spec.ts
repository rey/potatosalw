import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalQuizesMultiplesComponent } from './modal-quizes-multiples.component';

describe('ModalQuizesMultiplesComponent', () => {
  let component: ModalQuizesMultiplesComponent;
  let fixture: ComponentFixture<ModalQuizesMultiplesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalQuizesMultiplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalQuizesMultiplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
