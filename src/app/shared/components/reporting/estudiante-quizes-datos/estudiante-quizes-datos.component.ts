import { Component, Input, OnInit } from '@angular/core';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { finalize } from 'rxjs/operators';
import { QuizzesService } from 'src/app/core/services/quizzes';
import { MODAL_DIALOG_TYPES } from 'src/app/core/utils/modal-dialog-types';
import { ImagenPipe } from 'src/app/shared/pipes/imagen.pipe';
import { RecordarQuizPlayComponent } from '../../quiz-open/quiz-play/quiz-play.component';
import { ModalQuizesMultiplesComponent } from '../modal-quizes-multiples/modal-quizes-multiples.component';
import { ReportingService } from '../service/reporting.service';
import { TranslateService } from '@ngx-translate/core';
import { ToasterService } from 'src/app/core/services/shared/toaster.service';

export interface EstudentReportingTable{
  name: string;
  image: string;
}

const NOIMAGE = "../../../../../assets/images/icons/account_circle.svg"

@Component({
  selector: 'app-estudiante-quizes-datos',
  templateUrl: './estudiante-quizes-datos.component.html',
  styleUrls: ['./estudiante-quizes-datos.component.scss'],
})
export class EstudianteQuizesDatosComponent implements OnInit {
  displayedColumns: string[] = [];
  // dataSource : []=  [];
  dataSource: MatTableDataSource<any>;

  public headTable: [] = [];
  public bodyTable: [] = [];
  bodyTable2: any;
  isLoading: boolean;
  @Input() idGrafo?: any;
  @Input() idCourse?: any;
  @Input() idNode?: any;
  @Input() idGrupo: any;

  loading: boolean;
	quizType: number;


	constructor(private reportringService: ReportingService,
		private imagePipe: ImagenPipe,
		private quizService: QuizzesService,
		private modalService: NgbModal,
		private reportingService: ReportingService,
		private toaster: ToasterService,
		private translateService: TranslateService
	) { }

  ngOnInit() {
    this.suscribeReportingService();
  }

  suscribeReportingService() {
    this.reportringService.reportingSusbcribe.subscribe( (res:any) => {
      this.headTable = res.headTable;
      this.bodyTable = res.bodyTable;
    })
  }

  getUserAvatar(imagen):string{
    let image:string = ''
    imagen!== null && imagen !== ""? image = this.imagePipe.transform(imagen, 'userAvatar') : image = NOIMAGE
    return `url('${image}')`
  }


  viewQuiz(node: any){
		if (node.isMultiplex === 1) {
			let storedGroupInfo = JSON.parse(localStorage.getItem('selectedGroupInfo'));
		    const idGrupo = storedGroupInfo ? storedGroupInfo.idGroup : 0;
			this.quizService.getQuizInformMultipleHijos(node.idQuiz, node.idUser, idGrupo).pipe(finalize(() => this.isLoading = false)).subscribe((res:any) => {

				const modalRef = this.modalService.open(ModalQuizesMultiplesComponent, {scrollable: true, windowClass: MODAL_DIALOG_TYPES.W35, backdrop: 'static' })
				modalRef.componentInstance.quizesMultiplesList = res;
				modalRef.componentInstance.idUser = node.idUser;

				modalRef.result.then(res => {
						// Falta enviar la data para cambiar los valores y actualizar el backend
						// console.log(res)
						this.suscribeReportingService();
						this.reportingService.getDataTableQuizesStudeng(this.idGrupo, this.idCourse, this.idGrafo, this.idNode).subscribe(  (res:any) => {
							this.reportingService.reportingSusbcribe.next({ ...res.data})
							this.loading = false;
						})
			})
			});
		} else {
			const curso = node.idCourse === 0 ? this.idCourse : node.idCourse;
			const grafo = node.idTarget === 0 ? this.idGrafo : node.idTarget;
			this.quizService.getQuiz(node.idQuiz, curso, grafo).subscribe(res => {

				if(res.quiz.quizType === 1){
					this.quizService.getQuizInforme(node.idQuiz, node.idUser).pipe(finalize(() => this.isLoading = false)).subscribe((res:any) => {
						const modalRef = this.modalService.open(RecordarQuizPlayComponent, {scrollable: true, windowClass: MODAL_DIALOG_TYPES.W100, backdrop: 'static' })
						modalRef.componentInstance.quiz = { ...res.quiz, user: res.quiz.user, idOriginal: res.quiz.idOriginal, id: res.quiz.idQuiz, originalX: res.quiz.originalX, originalY: res.quiz.originalY, size: res.quiz.size, sizeQuiz: res.quiz.sizeQuiz, x: res.quiz.x, y: res.quiz.y };;
						modalRef.componentInstance.elements = res.elementsQuestion;
						modalRef.componentInstance.respuestaEstudiante = res.elementsAnswer;
						modalRef.componentInstance.courseId = node.idCurso;
						modalRef.componentInstance.graphId = node.target;
						modalRef.componentInstance.isEvaluation = true;

						modalRef.result.then(res => {
								// Falta enviar la data para cambiar los valores y actualizar el backend
								console.log(res)
								this.suscribeReportingService();
								this.reportingService.getDataTableQuizesStudeng(this.idGrupo, this.idCourse, this.idGrafo, this.idNode).subscribe(  (res:any) => {
									this.reportingService.reportingSusbcribe.next({ ...res.data})
									this.loading = false;
								})
					})
					});
				} else{
					this.toaster.error(this.translateService.instant("INFORMES.ONLYOPENANSWER")
					)
				}
			})
		}
  }


}
