import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EstudianteQuizesDatosComponent } from './estudiante-quizes-datos.component';

describe('EstudianteQuizesDatosComponent', () => {
  let component: EstudianteQuizesDatosComponent;
  let fixture: ComponentFixture<EstudianteQuizesDatosComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EstudianteQuizesDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstudianteQuizesDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
