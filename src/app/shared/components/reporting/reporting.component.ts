import { UsersService } from "./../../../core/services/users/users.service";
import { FlatTreeControl } from "@angular/cdk/tree";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material/tree";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { IReporting, TypeChlidrenReporting } from "./interfaces/reporting";
import { ReportingService } from "./service/reporting.service";
import { debounceTime, finalize, switchMap } from "rxjs/operators";
import { GruposService } from "src/app/core/services/groups/grupos.service";
import { ChallengeListModel } from "src/app/core/models/groups/groups-model";
import { Subject } from "rxjs";
import { TranslateService } from "@ngx-translate/core";
import { ToasterService } from "src/app/core/services/shared/toaster.service";
import moment from "moment-timezone";
interface ExampleFlatNode {
	expandable: boolean;
	name: string;
	id: string;
	icon: string;
	level: number;
}

enum FILTER_MODE {
	LIST_MODE = "list_mode",
	CHALLENGE_MODE = "challenge_mode",
}

enum SELECTED_TYPE {
	GRAPH = "graph",
	CHALLENGE = "challenge",
}

@Component({
	selector: "app-reporting",
	templateUrl: "./reporting.component.html",
	styleUrls: ["./reporting.component.scss"],
	encapsulation: ViewEncapsulation.None,
})
export class ReportingComponent implements OnInit {
	public idCourse: any;
	public idGrafo: any;
	public idNode: any;
	public idGrupo: any;
	public group: any;
	isLoading: boolean = true;
	challenges: ChallengeListModel[] = [];
	selectedFilterMode: string = FILTER_MODE.LIST_MODE;
	listChallengeSelected: number[] = [];
	listGraphSelected: number[] = [];
	filterModeList = FILTER_MODE;
	selectedType = SELECTED_TYPE;
	$toogle = new Subject<void>();
	showReportButton = false;
	usersInscritos: any;
	editMode = false
	editValue:number = null
	nameSession:string;

	private _transformer = (node: IReporting, level: number) => {
		return {
			expandable: !!node.children && node.children.length > 0,
			name: node.name,
			id: node.id,
			icon: node.icon,
			level: level,
			type: node.type,
			idCourse: node.idCourse,
			idTarget: node.idTarget,
			pendingQuizzes: node.pendingQuizzes,
		};
	};

	treeControl = new FlatTreeControl<ExampleFlatNode>(
		(node) => node.level,
		(node) => node.expandable
	);

	treeFlattener = new MatTreeFlattener(
		this._transformer,
		(node) => node.level,
		(node) => node.expandable,
		(node) => node.children
	);

	dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
	loading: boolean = false;
	downloadIcon: boolean = false;
	selectedNode: any = null;
	reportGenerated: boolean = false;
	reportTextButtonRefresh: string = this.translateService.instant("INFORMES.REFRESH");
	reportTextButton: string = this.translateService.instant("INFORMES.SHOWREPORT");
	constructor(
		private reportingService: ReportingService,
		private activeModal: NgbActiveModal,
		public userService: UsersService,
		private groupService: GruposService,
		private toaster: ToasterService,
		private translateService: TranslateService,
	) {}

	ngOnInit(): void {
		this.isLoading = true;
		this.getMasterDataCourses();
		this.getListadoEstudiantes();
		this.$toogle
			.pipe(
				debounceTime(1000),
				switchMap(() =>
					this.reportingService.getDataTableFromChallenges(
						this.idGrupo,
						this.listChallengeSelected
					)
				)
			)
			.subscribe(
				(result) => {
					this.reportingService.reportingSusbcribe.next({ ...result.data });
				},
				(err) =>
					this.toaster.error(
						this.translateService.instant("INFORMES.ERRORQUIZZES")
					)
			);
	}

	getMasterDataCourses() {
		this.selectedFilterMode = FILTER_MODE.LIST_MODE;
		this.resetData();
		this.reportingService
			.getReportingByGroupId(this.idGrupo)
			.pipe(finalize(() => (this.isLoading = false)))
			.subscribe(
				(res: any) => {
					this.dataSource.data = res.data.courses;
				},
				(err) => {
					this.toaster.error(
						this.translateService.instant("INFORMES.ERRORCOURSES")
					);
					this.dataSource.data = [];
				}
			);
	}

	getListadoEstudiantes() {
    this.groupService.getListEstudiantes(this.group.idGroup).subscribe((res:any) => {
      // console.log(res)
      // if (condition) {

      // }
      this.usersInscritos = res.data;
    });
  }

	resourse = (node: IReporting) => {
		if (node.type === TypeChlidrenReporting.NODE) {
			this.selectedNode = node;
			this.loading = true;
			this.idNode = node.id;
			this.idCourse = node.idCourse;
			this.idGrafo = node.idTarget;
			this.downloadIcon = true;
			this.reportingService
				.getDataTableQuizesStudeng(
					this.idGrupo,
					this.idCourse,
					this.idGrafo,
					this.idNode
				)
				.subscribe(
					(res: any) => {
						this.reportingService.reportingSusbcribe.next({ ...res.data });
						this.loading = false;
					},
					(err) => {
						this.toaster.error(
							this.translateService.instant("INFORMES.ERRORQUIZZES")
						);
					}
				);
		}
	};

	hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

	closeModal() {
		this.activeModal.close();
	}

	getChallenges() {
		this.selectedFilterMode = FILTER_MODE.CHALLENGE_MODE;
		this.resetData();
		this.groupService
			.getChallenges(this.idGrupo)
			.pipe(finalize(() => (this.isLoading = false)))
			.subscribe(
				(result) => {
					result.data.forEach(element => {
						moment.locale('es');
						let serverDate = moment.tz(element.nameSession, "UTC");
						let localDate = serverDate.local()
						element.nameSession = localDate.format('LLLL')
					});
					this.challenges = result.data as ChallengeListModel[];
				},
				(err) => {
					this.toaster.error(
						this.translateService.instant("INFORMES.ERRORCHALLENGES")
					);
					this.challenges = [];
				}
			);
	}

	downloadExcel() {
		this.reportingService
			.downloadExcel(this.idGrupo, this.idCourse, this.idGrafo, this.idNode)
			.subscribe(
				(blob) => {
					const url = URL.createObjectURL(blob);
					const a = document.createElement("a");
					a.href = url;
					const fecha = new Date();
					const nombre = `infome_${this.idNode}_${fecha}.xlsx`;
					a.download = nombre;
					document.body.appendChild(a);
					a.click();
					document.body.removeChild(a);
				},
				(err) => {
					this.toaster.error(
						this.translateService.instant("INFORMES.ERROREXCEL")
					);
				}
			);
	}

	toggleChallenge(ev: boolean, data: any, type: string) {
		if (type === SELECTED_TYPE.CHALLENGE) {
			const obj = data as ChallengeListModel;
			if (ev) this.listChallengeSelected.push(obj.idSession);
			else
				this.listChallengeSelected = this.listChallengeSelected.filter(
					(element) => element !== obj.idSession
				);
			this.$toogle.next();
		} else {
			const obj = data as IReporting;
			const id = Number(obj.id);

			if (ev) this.listGraphSelected.push(id);
			else
				this.listGraphSelected = this.listGraphSelected.filter(
					(element) => element !== id
				);
			if (this.listGraphSelected.length) this.showReportButton = true;
			else {
				this.showReportButton = false;
				this.resetData();
			}
		}
	}

	generateGraphsReport() {
		this.reportingService
			.getDataTableFromGraphs(this.idGrupo, this.listGraphSelected)
			.subscribe(
				(result) => {
					this.reportingService.reportingSusbcribe.next({ ...result.data });
					this.downloadIcon = false;
					this.reportGenerated = true;
				},
				(err) =>
					this.toaster.error(
						this.translateService.instant("INFORMES.ERRORQUIZZES")
					)
			);
	}
	private resetData() {
		this.reportingService.reportingSusbcribe.next({ data: [] });
		this.showReportButton = false;
		this.listChallengeSelected = this.listGraphSelected = [];
	}

	editSaveSession(challenge: ChallengeListModel){
		this.editMode = !this.editMode
		this.editMode ? this.editValue = challenge.idSession : this.editValue = null

		if(!this.editMode && !this.editValue && this.nameSession){
			this.groupService.updateNameSession(challenge.idSession, this.nameSession, challenge.idGroup).subscribe(result => {
				this.toaster.success(this.translateService.instant('INFORMES.NAMESESSIONOK'))
				this.nameSession = ''
			}, err => {
				this.toaster.error(this.translateService.instant('INFORMES.NAMESESSIONKO'))
			})
		}
	}

	deleteChallenge(challenge: ChallengeListModel){
		//Eliminar el reto
		this.groupService.deleteChallenge(challenge.idGroup, challenge.idSession).subscribe(result => {
			this.toaster.success(this.translateService.instant('INFORMES.DELETECHALLENGEOK'))
			this.getChallenges()
		}, err => {
			this.toaster.error(this.translateService.instant('INFORMES.DELETECHALLENGEKO'))
		})
	}

	nameSessionChanged(name:string){
		this.nameSession = name
	}
}
