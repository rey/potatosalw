import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportingService {

  public reportingSusbcribe = new  Subject<({data})>();

  constructor(private http: HttpClient) { }

  getReportingByGroupId(idGroup: number){
    return this.http.get<any>(`group/list-courses-groups/${idGroup}`);
  }

  getDataTableQuizesStudeng(idGrupo: any, idCourse: any, idGrafo: any, idNode: any) {
    return this.http.get<any>(`group/gettable-quizzes/${idGrupo}/${idCourse}/${idGrafo}/${idNode}`);
  }

	downloadExcel(idGrupo: any, idCourse: any, idGrafo: any, idNode: any): Observable<Blob> {
    return this.http.get(`group/excelInforme/${idGrupo}/${idCourse}/${idGrafo}/${idNode}`, {responseType: 'blob'});
  }

	getDataTableFromChallenges(idGroup:number, idSessionList: number[]): Observable<any>{
		const url = 'group/gettable-quizzes-challenge'
		const body = {
			idGroup: idGroup,
			idSessionList : idSessionList,
			profile: ""
		}
		return this.http.post(url, body)
	}

	getDataTableFromChallengesStudent(idGroup:number, idSessionList: number[]): Observable<any>{
		const url = 'group/gettable-quizzes-challenge'
		const body = {
			idGroup: idGroup,
			idSessionList : idSessionList,
			profile: "Student"
		}
		return this.http.post(url, body)
	}

	getDataTableFromGraphs(idGroup:number, idTargetList: number[]): Observable<any>{
		const url = 'group/gettable-quizzes-targets'
		const body = {
			idGroup: idGroup,
			idTargetList: idTargetList
		}
		return this.http.post(url,body)
	}
}
