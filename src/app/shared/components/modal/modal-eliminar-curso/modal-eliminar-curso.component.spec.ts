import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalEliminarCursoComponent } from './modal-eliminar-curso.component';

describe('ModalEliminarCursoComponent', () => {
  let component: ModalEliminarCursoComponent;
  let fixture: ComponentFixture<ModalEliminarCursoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEliminarCursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEliminarCursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
