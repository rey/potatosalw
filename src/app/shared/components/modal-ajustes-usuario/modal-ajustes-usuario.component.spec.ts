import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalAjustesUsuarioComponent } from './modal-ajustes-usuario.component';

describe('ModalAjustesUsuarioComponent', () => {
  let component: ModalAjustesUsuarioComponent;
  let fixture: ComponentFixture<ModalAjustesUsuarioComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAjustesUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAjustesUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
