import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalGruposEditarComponent } from './modal-grupos-editar.component';

describe('ModalGruposEditarComponent', () => {
  let component: ModalGruposEditarComponent;
  let fixture: ComponentFixture<ModalGruposEditarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalGruposEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGruposEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
