import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalCompetitionComponent } from './modal-competition.component';

describe('ModalCompetitionComponent', () => {
  let component: ModalCompetitionComponent;
  let fixture: ComponentFixture<ModalCompetitionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCompetitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCompetitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
