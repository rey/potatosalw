export class IInfoEstudiante {
    id: string;
    name: string;
    icon: string;
    children?: IInfoEstudiante[];
    type: string; 
    idCourse: number = 0;
    idTarget: number = 0;
}