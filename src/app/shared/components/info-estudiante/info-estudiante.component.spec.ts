import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoEstudianteComponent } from './info-estudiante.component';

describe('InfoEstudianteComponent', () => {
  let component: InfoEstudianteComponent;
  let fixture: ComponentFixture<InfoEstudianteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoEstudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoEstudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
