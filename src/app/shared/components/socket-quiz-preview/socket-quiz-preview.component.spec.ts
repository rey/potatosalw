import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SocketQuizPreviewComponent } from './socket-quiz-preview.component';

describe('SocketQuizPreviewComponent', () => {
  let component: SocketQuizPreviewComponent;
  let fixture: ComponentFixture<SocketQuizPreviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SocketQuizPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketQuizPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
