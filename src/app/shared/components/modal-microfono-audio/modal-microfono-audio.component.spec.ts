import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalMicrofonoAudioComponent } from './modal-microfono-audio.component';

describe('ModalMicrofonoAudioComponent', () => {
  let component: ModalMicrofonoAudioComponent;
  let fixture: ComponentFixture<ModalMicrofonoAudioComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMicrofonoAudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMicrofonoAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
