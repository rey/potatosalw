import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-tasks-home',
  templateUrl: './modal-tasks-home.component.html',
  styleUrls: ['./modal-tasks-home.component.scss']
})
export class ModalTasksHomeComponent implements OnInit {

  images: Array<string> = []

  constructor(    public activeModal: NgbActiveModal) { 
    this.images = [
      '../../../../assets/images/demo_images/tasks.png',
    ]
  }

  ngOnInit() {
  }

  closeModal(sendData) {
    this.activeModal.close(sendData);
  }

}
