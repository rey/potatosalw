import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalTasksHomeComponent } from './modal-tasks-home.component';

describe('ModalTasksHomeComponent', () => {
  let component: ModalTasksHomeComponent;
  let fixture: ComponentFixture<ModalTasksHomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTasksHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTasksHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
