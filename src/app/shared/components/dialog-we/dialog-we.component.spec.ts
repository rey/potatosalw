import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogWeComponent } from './dialog-we.component';

describe('DialogWeComponent', () => {
  let component: DialogWeComponent;
  let fixture: ComponentFixture<DialogWeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogWeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogWeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
