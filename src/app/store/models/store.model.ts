import { loggedInUser } from './user.model';
import { availableProfiles } from "../models/profiles.model"
import { selectProfile } from '../actions/selected-profile.action';

export interface State {
	readonly user: loggedInUser;
	readonly profiles: availableProfiles;
	readonly selectedProfile: selectProfile;
}
