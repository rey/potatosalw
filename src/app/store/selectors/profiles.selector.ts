import { createFeatureSelector, createSelector } from "@ngrx/store";
import { State } from "../models/store.model";

export const selectedProfile = createFeatureSelector<State>("selectedProfile");

export const selectAvailableProfiles = createSelector(
	selectedProfile,
	(state) => state.selectedProfile
);
