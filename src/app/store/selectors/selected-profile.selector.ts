import { createFeatureSelector, createSelector } from "@ngrx/store";
import { State } from "../models/store.model";

export const selectProfiles = createFeatureSelector<State>("availableProfiles");

export const selectAvailableProfiles = createSelector(
	selectProfiles,
	(state) => state.profiles
);
