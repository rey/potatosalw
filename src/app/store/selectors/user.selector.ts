import { createFeatureSelector, createSelector } from "@ngrx/store";
import { State } from "../models/store.model";

export const selectUser = createFeatureSelector<State>("user");

export const selectUserDetails = createSelector(
  selectUser,
	(state) => state.user
);
